## Note: This repo is deprecated!!! 

New development moved to [here](https://bitbucket.org/stoneridgetechnology/cloud_portal)


## Overview

![arch view](./docs/echelon.png)

* source file is [echelon.xml](docs/echelon.xml) with [draw.io](draw.io)

## Developer Note



### 1. Setup ASW credentials (Do It Once)

* AWS crednetials
* Parallel Cluster credentials


### 2. Development Setup

Environment variables:

* `SRT_STRIPE_PUBLISHABLE_KEY`

* `ECHELON_ADMIN_EMAIL_PASSWORD`
* `SRT_STRIPE_SECRET_KEY`
* `SRT_STRIPE_SECRET_WEBHOOK_KEY`


1. start the react app:

```bash
$ npm install  # only run once
$ npm run start
```

2. run the python backend (Python 3.6) in development mode:

```bash
python3 -m venv echelon #  only once
source echelon/bin/activate
pip3 install -r ./requirements.txt # only once
python3 ./api.py
```

## Release

1. Build the folder for static serving:

```bash
$ npm run build
```

2. Serving flask (TODO)

## Notes for aws cluster

* SSH to the header node

```bash
pcluster ssh hydra -i ~/.ssh/pcluster.pem
```