"""
all the main api routines in flask
"""
import secrets
import logging
from functools import wraps
import json
import requests
from absl import flags
from absl import app as absl_app
from flask import Flask, Response, request, jsonify, make_response, session, current_app, abort
# pylint:disable = no-name-in-module
from api.config import CONFIG
from api.services.welcome_template import build_welcome_message
from api.services.resend_code_template import build_resend_message
from api.user import init_database, random_with_n_digits, get_hashed_password, check_password
from api.services.email import Email
from api.services.stripe_webhook import webhook_received
from api.token import issue, verify
from api.services.services import Service
from api.aws_pcluster import list_aws
from api.on_prem import list_local



FLAGS = flags.FLAGS
flags.DEFINE_string('proxy', 'http://localhost:3000/',
                    "reverse proxy address for development")
flags.DEFINE_string('echelon_db', 'sqlite:echelon.db',
                    'user database configuration')
flags.DEFINE_string('job_callback', '',
                    'callback url, set the public url for production')

app = Flask(__name__)


@app.route('/api/auth/login', methods=['GET'])
def login_api():
    """
    handles login request, if not verified, do not issue token, jsut return 200
    """
    auth = request.authorization
    user = Service['db'].get_user(auth.username)
    if user is None:
        return Response('Unknown email or password.', 401, {})
    user_id, hashed_password, verified = user
    if verified == 0:
        if check_password(auth.password, hashed_password):
            # do not issue token for non-verified user
            return {}
        return Response('Unknown email or password.', 401, {})
    if check_password(auth.password, hashed_password):
        return {"token": issue(user_id, auth.username)}


@app.route('/api/auth/register', methods=['GET'])
def register_api():
    """register API, also create verification code """
    auth = request.authorization
    hashed_password = get_hashed_password(auth.password)
    verify_code = random_with_n_digits(8)
    status, msg = Service['db'].add_user(
        auth.username, hashed_password, verify_code)
    if status:
        Service['email'].send(build_welcome_message(
            auth.username, str(verify_code)))
        return Response('', 200)
    return make_response(jsonify({"error": msg}), 400)


@app.route('/api/auth/resend', methods=['GET'])
def resend_api():
    """resend the verification code via email """
    try:
        email = request.args.get("user")
        verify_code = random_with_n_digits(8)
        Service['email'].send(build_resend_message(
            email, str(verify_code)))
        ok_flag, msg = Service['db'].update_user_verify_code(
            email, verify_code)
        if ok_flag:
            logging.info("updated user %s verify code per request", email)
            return Response('', 200)
        else:
            logging.warning("failed to fulfill user %s's request to resend code: %s", email, msg)
            return make_response(jsonify({"error": msg}), 400)
    except KeyError:
        return make_response("invalid parameter for user", 400)


@app.route('/api/auth/verify', methods=['GET'])
def verify_api():
    """check the verification code to verify the user's email address"""
    try:
        email = request.args.get("user")
        code = request.args.get("code")
        verified = Service['db'].verify_user(email, code)
        if verified is None:
            return Response('', 400)
        return {"token": issue(verified[2], email)}

    except KeyError:
        return make_response("invalid parameter for verify request", 400)


def token_required(func):
    """Wrap this for all APIs that need token authentication"""
    @wraps(func)
    def decorated_function(*args, **kwargs):
        """ """
        bearer_token = request.headers.get('Authorization').split(" ")
        if len(bearer_token) == 2:
            token = bearer_token[1]
            user_id = verify(token)
            session['id'] = user_id
            if user_id is not None:
                return func(*args, **kwargs)
        logging.warning("illegal token received %s",
                        request.headers.get('Authorization'))
        return Response('', 401, {})
    return decorated_function


@app.route('/api/balance', methods=['GET'])
@token_required
def get_balance_api():
    """check the balance of the given user"""
    try:
        email = session.get("id")
        balance= Service['db'].get_balance(email)
        if balance is None:
            return make_response("", 500)
        balance_json = {"balance": balance[0], "reserved": balance[1]}
        return make_response(jsonify(balance_json), 200)
    except KeyError:
        return make_response("invalid parameter for balance request", 400)


@app.route('/api/cluster', methods=['GET'])
@token_required
def list_clusters_api():
    """list available clusters for this user"""
    aws = list_aws()
    local = list_local()
    clusters = []
    for l_cluster in local:
        clusters.append(l_cluster)
    for a_cluster in aws:
        clusters.append(a_cluster)
    response = make_response(jsonify(clusters), 200)
    response.headers["Content-Type"] = "application/json"
    return response


@app.route('/api/cluster', methods=['POST'])
@token_required
def list_cluster_api():
    """list cluster info"""
    # user_id = session.get('id')
    try:
        cluster_name = request.json['cluster']
    except KeyError:
        return make_response("Need to specify cluster name", 400)
    logging.info("getting cluster info for %s", cluster_name)
    # TODO check user_id has permission to view cluster
    cluster_info, msg = CONFIG.get_cluster(cluster_name)
    if msg != "":
        return make_response(msg, 500)
    logging.info("return cluster info for %s with %d queues",
                 cluster_name, len(cluster_info))
    response = make_response(jsonify(cluster_info), 200)
    response.headers["Content-Type"] = "application/json"
    return response


@app.route('/api/jobs', methods=['POST'])
@token_required
def list_job_api():
    """list all jobs"""
    # user_id = session.get('id')
    try:
        cluster_name = request.json['cluster']
        # TODO check user
    except KeyError:
        return make_response("Need to specify cluster name", 400)
    logging.info("getting job list for %s", cluster_name)
    # TODO check if user_id has permission to this cluster_name
    job_info, msg = CONFIG.list_jobs(cluster_name)
    if msg != "":
        return make_response(msg, 500)
    logging.info("return cluster info for %s with %d jobs",
                 cluster_name, len(job_info))
    response = make_response(jsonify(job_info), 200)
    response.headers["Content-Type"] = "application/json"
    return response


@app.route('/api/model', methods=['POST'])
@token_required
def model_api():
    """list all models for a user in a cluster"""
    user_id = session.get('id')
    try:
        cluster_name = request.json['cluster']
        action = request.json['action']
        if action == "list":
            models, msg = CONFIG.list_models(cluster_name, user_id)
        elif action == "delete":
            models, msg = CONFIG.delete_model(
                cluster_name, user_id, request.json['model'])
        else:
            return make_response("not implemented", 501)
        if msg != "":
            print("msg is", msg)
            return make_response(msg, 500)
        response = make_response(jsonify(models), 200)
        response.headers["Content-Type"] = "application/json"
        return response
    except KeyError:
        logging.warning("invalid user input for /api/model")
        return make_response("Need to specify cluster name or action", 400)


@app.route('/api/model/upload', methods=['POST'])
@token_required
def upload_model_api():
    """
    There are several steps involving the upload of a model:
    1. Client request a possible upload, server sends out a pre-signed URL
    (from S3 bucket e.g.) back
    2. Client puts the zipped file to the bucket
    3. Client notifies the server that the bucket key is ready
    4. Server in the background copies the key to the backend cluster, and
    unzip the fold
    """
    user_id = session.get('id')
    try:
        cluster_name = request.json['cluster']
        model = request.json['model']
    except KeyError:
        return make_response("Need to specify cluster name or model", 400)
    model, msg = CONFIG.upload_model(cluster_name, user_id, model)
    if msg != "":
        logging.warning("error upload a model %s", msg)
        return make_response(msg, 500)
    return make_response(jsonify(model), 200)


@app.route('/api/model/job', methods=['POST'])
@token_required
def list_model_jobs_api():
    """list all existing jobs for a model"""
    user_id = session.get('id')
    try:
        if request.json['user'] != user_id:
            return make_response("wrong user name", 400)
        cluster_name = request.json['cluster']
        jobs, msg = CONFIG.list_model_jobs(
            cluster_name, user_id, request.json['model'])
        if msg != "":
            return make_response(msg, 500)
        response = make_response(jsonify(jobs), 200)
        response.headers["Content-Type"] = "application/json"
        return response
    except KeyError:
        return make_response("Need to specify cluster name", 400)


@app.route('/api/job', methods=['POST'])
@token_required
def run_job_api():
    """submit a job to a cluster"""
    user_id = session.get('id')
    _, msg = CONFIG.run_job(request.json, user_id)
    if msg != "":
        return make_response(msg, 500)
    return make_response("", 200)


@app.route('/api/job/output', methods=['POST'])
@token_required
def get_job_output_api():
    """fetch the job PRT file output"""
    user_id = session.get('id')
    prt_content, msg = CONFIG.get_job_output(request.json, user_id)
    if msg != "":
        return make_response(msg, 500)
    return make_response(jsonify(prt_content), 200)


@app.route('/api/job/summary', methods=['POST'])
@token_required
def get_job_summary_api():
    """ get job summary for 2d plotting """
    user_id = session.get('id')
    summary_content, msg = CONFIG.get_job_summary(request.json, user_id)
    if msg != "":
        return make_response(msg, 500)
    return make_response(jsonify(summary_content), 200)



@app.route('/api/transactions', methods=['POST'])
@token_required
def get_transactions_api():
    """ get job summary for 2d plotting """
    user_id = session.get('id')
    try:
        group_name = request.json['group']
    except KeyError:
        group_name = ""
    trans = Service['db'].get_transactions_by_email_and_group(user_id, group_name)
    return make_response(jsonify(trans), 200)


@app.route('/api/payment_received', methods=['POST'])
def stripe_webhook_api():
    """the stripe webhook"""
    rtn = webhook_received(request)
    if rtn is None:
        return make_response("", 500)
    return make_response(jsonify(rtn), 200)


@app.route('/api/job_status', methods=['POST'])
def job_done_webhook_api():
    """the webhook when job is done TODO"""
    logging.info("Received job done webhook!")
    try:
        reserve_info = Service['db'].get_reserve(request.json['reserve_id'])
        if reserve_info is None:
            return make_response("invalid reserve", 500)
        if reserve_info[3] != request.json['code']:
            return make_response("invalid code", 500)
        msg = CONFIG.job_done_update(request.json, reserve_info)
        if msg != "":
            return make_response(msg, 500)
        return make_response("", 200)
    except KeyError:
        return make_response("invalid user input", 500)


def debug_only(func):
    """only for development mode"""
    @wraps(func)
    def wrapped(**kwargs):
        if not current_app.debug:
            abort(404)
        return func(**kwargs)
    return wrapped

@app.route('/static/js/<path:path>', methods=['GET'])
@debug_only
def special(path):
    """hack to support js bundle in development mode"""
    return requests.get(f'{FLAGS.proxy}static/js/{path}').content


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>', methods=['GET'])
@debug_only
def proxy(path):
    """development proxy the npm run port """
    return requests.get(f'{FLAGS.proxy}{path}').content


def main(argv):
    """the main routine"""
    del argv
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch = logging.StreamHandler()
    # ch.setFormatter(formatter)
    # logging.getLogger().addHandler(ch)
    Service["db"] = init_database(FLAGS.echelon_db)
    Service["email"] = Email()
    CONFIG.set_call_back(FLAGS.job_callback + "/api/job_status")
    app.config["SECRET_KEY"] = secrets.token_urlsafe(16)

    for cluster_name, cluster in CONFIG.clusters.items():
        logging.info("adding/updating cluster %s to database", cluster_name)
        Service["db"].add_or_update_cluster(cluster)
    # Service["db"].add_or_update_cluster(cluster, group_id=-1):
    app.run(host='0.0.0.0', port=5000)
    # app = Flask(__name__, static_folder='./build', static_url_path='/')  # production run
    # or use nginx

# https://github.com/stripe-samples/accept-a-payment/blob/main/custom-payment-flow/server/python/server.py

if __name__ == "__main__":
    absl_app.run(main)
