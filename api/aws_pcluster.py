from subprocess import PIPE, run, CalledProcessError, CompletedProcess
import os
import logging
import uuid
import json
import re
import datetime
import pcluster
import boto3
from botocore.exceptions import ClientError
from pcluster.utils import paginate_boto3, get_head_node_ip_and_username
from pcluster.constants import PCLUSTER_STACK_PREFIX

VENDOR = "aws"
REGION = "us-east-1"
CLUSTER_TYPE = "parallel-cluster"
CLUSTER_INPUT_PREFIX = "input_stage/"
CLUSTER_OUTPUT_PREFIX = "output_stage/"

CLUSTER_USER_ROOT = "/efs/users/"
CLUSTER_USER_PRIVATE_MODEL = "/models/private"
CLUSTER_USER_PUBLIC_MODEL = "/models/public"
CLUSTER_PUBLIC_MODEL_PATH = "/efs/shared/models"

VERSION_PATH_MAP = {
    "2021.1": "/efs/bin/echelon"
}


def get_pcluster_version(stack):
    try:
        for tag in stack.get("Tags"):
            if tag['Key'] == "Version":
                return tag['Value']
    except KeyError:
        return "unknown"
    finally:
        return "unknown"


def gen_private_model_path(user_name):
    return CLUSTER_USER_ROOT + user_name + CLUSTER_USER_PRIVATE_MODEL


def gen_public_model_path(user_name):
    return CLUSTER_USER_ROOT + user_name + CLUSTER_USER_PUBLIC_MODEL


def gen_job_run_path(user_name):
    return CLUSTER_USER_ROOT + user_name + CLUSTER_USER_PUBLIC_MODEL


def create_input_object_name(user_name):
    return CLUSTER_INPUT_PREFIX + \
        str(datetime.date.today()) + "/" + user_name + "/" + str(uuid.uuid1())


def create_output_object_name(user_name):
    return CLUSTER_INPUT_PREFIX + "/" + \
        str(datetime.date.today()) + "/" + user_name + "/output.zip"


def create_presigned_post(bucket_name, user_name,
                          fields=None, conditions=None, expiration=3600):
    """Generate a presigned URL S3 POST request to upload a file

    :param bucket_name: string
    :param object_name: string
    :param fields: Dictionary of prefilled form fields
    :param conditions: List of conditions to include in the policy
    :param expiration: Time in seconds for the presigned URL to remain valid
    :return: Dictionary with the following keys:
        url: URL to post to
        fields: Dictionary of form fields and values to submit with the POST
    :return: None if error.
    """

    # Generate a presigned S3 POST URL
    s3_client = boto3.client('s3')
    object_name = create_input_object_name(user_name)
    try:
        response = s3_client.generate_presigned_post(bucket_name,
                                                     object_name,
                                                     #Fields={"acl": "public-read"},
                                                     Fields=fields,
                                                     Conditions=conditions,
                                                     ExpiresIn=expiration)
        return object_name, response
        # fields = s3_client.generate_presigned_url('put_object',
        #                                      Params={'Bucket': bucket_name,
        # 'Key': object_name,
        #                                              'ACL': 'public-read'
        #                                              },
        #                                      ExpiresIn=3600)
        # return object_name, fields
    except ClientError as e:
        logging.error(e)
        return None, str(e)


# ModelInfo = collections.namedtuple('ModelInfo', 'permission file path')

def data_file_to_model(ssh_stdout, user_name):
    """ convert data file to model names """
    models = ssh_stdout.strip('\n').split('\n')

    def gen_model_info(model_path, user_name):
        file = os.path.basename(model_path)
        directory = os.path.dirname(model_path)
        permission = "private"
        if model_path.startswith("/efs/public/models/"):
            permission = "shared"
            directory = directory[len("/efs/shared/models/"):]
        if model_path.startswith(
                "/efs/users/{USER}/models/public/".format(USER=user_name)):
            permission = "public"
            directory = directory[len(
                "/efs/users/{USER}/models/public/".format(USER=user_name)):]
        if model_path.startswith(
                "/efs/users/{USER}/models/private/".format(USER=user_name)):
            permission = "private"
            directory = directory[len(
                "/efs/users/{USER}/models/private/".format(USER=user_name)):]
        return {"permission": permission, "path": directory, "file": file}
    return [gen_model_info(model, user_name) for model in models]


def construct_path_from_model(user_name, file_name, path, model_permission):
    """The reverse of data_file_to_model"""
    if model_permission == "private":
        return os.path.join(
            "/efs/users/{USER}/models/private/".format(USER=user_name), path, file_name)
    if model_permission == "public":
        return os.path.join(
            "/efs/users/{USER}/models/public/".format(USER=user_name), path, file_name)
    if model_permission == "shared":
        return os.path.join("/efs/shared/models/", path, file_name)
    return ""


class PCluster:
    """AWS ParalleCluster class"""
    SSH_TEMPLATE = "ssh {USER}@{MASTER_IP} -i {PEM_FILE} {ARGS}"

    def __init__(self, cluster_name, pem_file, bucket_name, shared):
        """constructor"""
        self.is_shared = shared
        self.cluster_name = cluster_name
        self.bucket_name = bucket_name
        self.region = REGION
        self.head_node_ip, self.username = get_head_node_ip_and_username(
            cluster_name)
        logging.info(
            "AWS Parallel Cluster %s, header IP is %s, username is %s, pem file %s",
            cluster_name,
            self.head_node_ip,
            self.username,
            pem_file)
        if os.path.isfile(pem_file):
            logging.error("Could not find the pem file %s", pem_file)
        self.pem_file = pem_file
        self.call_back_endpoint = ""

    def set_call_back(self, call_back_ep):
        """set the call back url for jobs"""
        self.call_back_endpoint = call_back_ep

    def info(self):
        """get cluster info"""
        return {
            "name": self.cluster_name,
            "vendor": "aws",
            "region": self.region,
            "type": "parallelcluster",
            "shared": self.is_shared
        }

    def add_new_user(self, user_name):
        # TODO
        # ssh mkdir -p gen_private_model_path(user_name):
        # ssh mkdir -p gen_private_model_path(user_name)
        # or sh /home/centos/scripts/add_user user_name
        return True, ""

    def list_models(self, user_name):
        ssh_cmd = self.gen_ssh_command(
            "find /efs/public/models /efs/users/{USER}/models -name *.DATA -type f".format(USER=user_name))
        result = run(
            ssh_cmd,
            stdout=PIPE,
            stderr=PIPE,
            universal_newlines=True,
            shell=True)
        if result.stderr != "":
            logging.info("list models for pcluster failed: %s", result.stderr)
            return [], result.stderr
        models = data_file_to_model(result.stdout, user_name)
        return models, ""

    def delete_model(self, user_name, model):
        try:
            dst = construct_path_from_model(
                user_name, model["file"], model["path"], model["permission"])
            if model['remove_dir']:
                dst = os.path.dirname(dst)
            # remove the directory and list_models
            logging.info("Deleting model %s", dst)
            ssh_cmd = self.gen_ssh_command(
                "\"rm -rf {DST}; find /efs/public/models /efs/users/{USER}/models -name *.DATA -type f\"".format(USER=user_name, DST=dst))
            logging.info("ssh command is %s", ssh_cmd)
            result = run(
                ssh_cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True,
                shell=True)
            if result.stderr != "":
                logging.warning("Error running remote ssh: %s", result.stderr)
                return [], "Internal error"
            models = data_file_to_model(result.stdout, user_name)
            return models, ""
        except KeyError:
            return [], "invalid user input"

    def list_model_jobs(self, user_name, model):
        """ list a model's previous jobs"""
        try:
            path = construct_path_from_model(
                user_name, model["file"], model["path"], model["permission"])
            file_name = os.path.splitext(os.path.basename(path))[0]
            job_root_path = os.path.join(
                os.path.dirname(path), ".run", file_name, user_name)
            logging.info("job root path %s", job_root_path)
            ssh_cmd = self.gen_ssh_command("ls {path}".format(
                path=job_root_path))
            result = run(
                ssh_cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True,
                shell=True)
            if result.stderr != "":
                return [], ""
            jobs = result.stdout.split()
            return jobs, ""
        except KeyError:
            logging.warning("failure to list model jobs with key error")
            return "", "invalid user input"
        except CalledProcessError:
            logging.warning("failure to list model jobs %s", result.stderr)
            return "", result.stderr

    def upload_model(self, user_name, model):
        """upload a model"""
        try:
            if "upload" not in model:  # generate presign URLs
                object_name, model = create_presigned_post(
                    self.bucket_name, user_name)
                return {"model": model, "key": object_name}, ""
            else:
                logging.info("getting upload request, model %s", str(model))
                # unzip_model(self.bucket_name, user_name, model["upload"])
                return self.unzip_model_from_bucket(
                    self.bucket_name, user_name, model)
        except KeyError:
            logging.warning("failure to upload a model with key error")
            return None, "invalid user input"

    def gen_ssh_command(self, args):
        return self.SSH_TEMPLATE.format(
            USER=self.username,
            MASTER_IP=self.head_node_ip,
            PEM_FILE=self.pem_file,
            ARGS=args)

    def get_cluster(self):
        ssh_cmd = self.gen_ssh_command(
            "sinfo -s")
        result = CompletedProcess([], "")

        def post_process_sinfo(output):
            """
            convert streams like this into structures:
            PARTITION AVAIL  TIMELIMIT   NODES(A/I/O/T) NODELIST
            ondemand     up   infinite        0/12/0/12 ondemand-dy-p3dn24xlarge-[1-4],ondemand-dy-p32xlarge-[1-8]
            spot*        up   infinite        0/20/0/20 spot-dy-p3dn24xlarge-[1-4],spot-dy-p32xlarge-[1-16]
            """
            lines = output.split("\n")
            rtn = []
            if len(lines) <= 1:
                return rtn
            for lineno, partition in enumerate(lines):
                if lineno == 0 or partition == "":
                    continue
                name, avail, time_limit, nodes, node_list, _ = re.split(
                    "\s+", partition)
                available, idle, other, total = [
                    int(i) for i in nodes.split('/')]
                rtn.append({"name": name,
                            "avail": avail,
                            "time_limit": time_limit,
                            "stat": {
                                "available": available,
                                "idle": idle,
                                "other": other,
                                "total": total
                            },
                            "echelon": {
                                "versions": [
                                    "2021.1"
                                ],
                            },
                            "node_list": node_list.split(",")
                            })
            return rtn
        try:
            result = run(
                ssh_cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True,
                shell=True,
                check=True)
            logging.info("successfully run get cluster info")
            return post_process_sinfo(result.stdout), ""

        except CalledProcessError:
            logging.warning(
                "failure to run get cluster info %s", result.stderr)
            return [], result.stderr

    def run_echelon_job(self, user_name, job):
        ssh_cmd = self.gen_ssh_command(
            "python3 /efs/public/scripts/run_echelon_job.py \'\"" +
            json.dumps(job).replace(
                " ",
                "").replace(
                "\"",
                "\\\"") +
            "\"\'")
        result = CompletedProcess([], "")
        try:
            result = run(
                ssh_cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True,
                shell=True,
                check=True)
        except CalledProcessError:
            logging.warning("failure to run echelon job %s", result.stderr)
            return "", result.stderr
        logging.info("successfully run echelon job")
        assert False  # TODO parse output for json
        return {}, ""

    def list_jobs(self):
        """list jobs for the user in this cluster"""
        ssh_cmd = self.gen_ssh_command("squeue")

        def post_process_squeue(output):
            """
            convert streams like this into structures:
            JOBID PARTITION     NAME     USER  ST       TIME  NODES NODELIST(REASON)
            92311     debug     test      cdc   R       0:08      2 d09n29s02,d16n02
            88915 general-c GPU_test      cdc  PD       0:00      1 (Priority)
            """
            lines = output.split("\n")
            rtn = []
            if len(lines) <= 1:
                return rtn
            for line in lines:
                if len(line) == 0 or not line[0].isdigit():
                    continue
                items = re.split(r"\s+", line)
                iid = items[0]
                user_name = items[3]
                queue = items[2]
                job_name = items[2]
                status = items[4]
                time = items[5]
                rtn.append({
                    "id": iid,
                    "user_name": user_name,
                    "queue": queue,
                    "job_name": job_name,
                    "status": status,
                    "time": time
                })
            return rtn
        result = run(
            ssh_cmd,
            stdout=PIPE,
            stderr=PIPE,
            universal_newlines=True,
            shell=True)
        if result.stderr != "":
            return [], "Internal error"
        return post_process_squeue(result.stdout), ""

    def unzip_model_from_bucket(self, bucket_name, user_name, model):
        """unzip a model from a bucket
        """
        model['upload']['bucket'] = self.bucket_name
        model['user_name'] = user_name
        ssh_cmd = self.gen_ssh_command(
            "python3 /efs/shared/scripts/unzip_model.py \'\"" +
            json.dumps(model).replace(
                " ",
                "").replace(
                "\"",
                "\\\"") +
            "\"\'")
        logging.info("command is %s", ssh_cmd)
        result = CompletedProcess([], "")
        try:
            result = run(
                ssh_cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True,
                shell=True,
                check=True)
            return result.stdout, ""
        except CalledProcessError:
            logging.warning("failure to run unzip model %s", result.stderr)
            return "", result.stderr


def list_aws():
    """list all clusters in aws"""
    try:
        result = []
        for stack in paginate_boto3(
                boto3.client("cloudformation").describe_stacks):
            if stack.get("ParentId") is None and stack.get(
                    "StackName").startswith(PCLUSTER_STACK_PREFIX):
                logging.info(
                    "Found a cluster %s",
                    stack.get("StackName")[
                        len(PCLUSTER_STACK_PREFIX):])
                # print("stack is %s", str(stack))
                # pcluster_version = _get_pcluster_version_from_stack(stack)
                result.append(
                    {
                        "vendor": VENDOR,
                        "region": REGION,
                        "type": CLUSTER_TYPE,
                        "name": stack.get("StackName")[len(PCLUSTER_STACK_PREFIX):],  # noqa: E203
                        "versions": ["2021.1"],   #
                        # TODO remove this hardcoded queues
                        "queues": [
                            "ondemand",
                            "spot"
                        ],
                        "version": get_pcluster_version(stack),
                    }
                )
        # LOGGER.info(tabulate(result, tablefmt="plain"))
    except ClientError as e:
        # LOGGER.critical(e.response.get("Error").get("Message"))
        logging.error("client error listing aws clusters %s", str(e))
        return []
    except KeyboardInterrupt:
        # LOGGER.info("Exiting...")
        return []
    return result
