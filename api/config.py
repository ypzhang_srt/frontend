"""
entry place for all cluster operations
"""
import math
import logging
from api.aws_pcluster import PCluster, list_aws
from api.on_prem import OnPremiseCluster, list_local
from api.remote_path import elapsed_to_minutes
from api.services.services import Service

AWS_PEM_FILE = "~/.ssh/pcluster.pem"
CLUSTER_BUCKET_NAME = "pcluster-stage-us-east-1"


def convert_to_hour_unit(asked_minutes, round_up=3):
    """convert minutes in float to hours: eg:
    28 min to 50 (round_up is 3), 51 min to 100 (round_up is 12)
    """
    assert (60 % round_up) == 0
    assert (100 % (60 // round_up)) == 0   # make sure
    return (math.ceil(asked_minutes) + round_up - 1) // round_up * round_up * 100 // 60


class Config:
    """Config is a container class for all available clusters"""

    def __init__(self):
        """constructor"""
        aws_clusters = list_aws()
        logging.info("Found %d aws clusters.", len(aws_clusters))
        self.clusters = {}
        for cluster_info in aws_clusters:
            self.clusters[cluster_info['name']] = PCluster(
                cluster_info['name'],
                AWS_PEM_FILE,
                CLUSTER_BUCKET_NAME, True)

        local_clusters = list_local()
        logging.info("Found %d local clusters.", len(local_clusters))
        for cluster_info in local_clusters:
            self.clusters[cluster_info['name']] = OnPremiseCluster(
                cluster_info['name'], "localhost", "PBS", "/data/user/cloud")
        logging.info("Config %d clusters", len(self.clusters))

    def set_call_back(self, call_back_ep):
        """set the call back url for jobs"""
        _ = [cluster.set_call_back(call_back_ep)
             for _, cluster in self.clusters.items()]

    def list_models(self, cluster_name, user_id):
        """delete all models"""
        try:
            cluster = self.clusters[cluster_name]
            models, msg = cluster.list_models(user_id)
            return models, msg
        except KeyError:
            return [], "Specified cluster " + cluster_name + " does not exist."

    def delete_model(self, cluster_name, user_id, model):
        """delete a model"""
        try:
            cluster = self.clusters[cluster_name]
            models, msg = cluster.delete_model(user_id, model)
            return models, msg
        except KeyError:
            return models, "Specified cluster " + cluster_name + " does not exist."

    def list_jobs(self, cluster_name):
        """list all jobs"""
        try:
            cluster = self.clusters[cluster_name]
            jobs, msg = cluster.list_jobs()
            return jobs, msg
        except KeyError:
            return [], "Specified cluster " + cluster_name + " does not exist."

    def get_cluster(self, cluster_name):
        """get cluster info"""
        try:
            cluster = self.clusters[cluster_name]
            info, msg = cluster.get_cluster()
            return info, msg
        except KeyError:
            return [], "Specified cluster " + cluster_name + " does not exist."

    def run_job(self, job_json, user_id):
        """run a job"""
        reserve_id = 0
        try:
            cluster_name = job_json['cluster']
            user = job_json['user']
            if user != user_id:
                return None, "Permission error for user " + user
            # model = job_json['model']
            job = job_json['job']
            # logging.info("model file is %s", model["file"])
            cluster_info = Service['db'].get_cluster_by_name(cluster_name)
            user_info = Service['db'].get_user(user)
            if not Service['db'].check_user_cluster(user_info, cluster_info):
                logging.warning("check user cluster failed")
                return None, "cluster " + cluster_name + " not accessible"
            reserve_id = Service['db'].reserve_hours(cluster_name, convert_to_hour_unit(
                job['timeout_min']), user, job_json['group'])
            if reserve_id is None:
                logging.warning("not enough credit for user %s", user)
                return None, "not enough credit for the new job"
            cluster = self.clusters[cluster_name]
            job_record, msg = cluster.run_echelon_job(user, job, reserve_id)
            if job_record is not None:
                job_id = Service['db'].add_job(
                    reserve_id, cluster_info[0], job_record["id"], job_record['ngpus'])
                if job_id is None:
                    logging.warning("error inserting the job id")
                else:
                    update_ok = Service['db'].update_jobid_in_reserve(
                        job_id, job_record['job']['run_time']['call_back_code'], reserve_id)
                    if not update_ok:
                        logging.warning("error updating jobid in reserve")
            return job_record, msg
        except KeyError as err:
            logging.warning("key error in run job %s", err)
            if reserve_id > 0:
                logging.warning("recover the reserved balance")
                Service['db'].cancel_reserve(reserve_id, False)
            return None, str(err)
        except Exception as err:
            logging.warning(
                "exception error in run job %s, type %s", err, type(err))
            if reserve_id > 0:
                logging.warning("recover the reserved balance")
                Service['db'].cancel_reserve(reserve_id, False)
            return None, str(err)

    def get_job_output(self, job_json, user_id):
        """get job PRT output"""
        try:
            cluster_name = job_json['cluster']
            user = job_json['user']
            if user != user_id:
                return None, "Permission error for user " + user
            job = job_json['job']
            print("job is", job)
            cluster = self.clusters[cluster_name]
            return cluster.get_job_output(user, job)
        except KeyError as exp:
            logging.warning("error getting job output: %s", exp)
            return "", "user input error"

    def get_job_summary(self, job_json, user_id):
        """get job summary data for 2d plot"""
        try:
            cluster_name = job_json['cluster']
            user = job_json['user']
            if user != user_id:
                return None, "Permission error for user " + user
            job = job_json['job']
            cluster = self.clusters[cluster_name]
            return cluster.get_job_summary(user, job)
        except KeyError as err:
            logging.warning("error getting job summary: %s", err)
            return "", "user input error"

    def list_model_jobs(self, cluster_name, user_id, model):
        """list jobs for a model"""
        try:
            cluster = self.clusters[cluster_name]
            jobs, msg = cluster.list_model_jobs(user_id, model)
            return jobs, msg
        except KeyError:
            return None, "invalid user input"

    def upload_model(self, cluster_name, user_id, model):
        """upload a model"""
        try:
            cluster = self.clusters[cluster_name]
            model, msg = cluster.upload_model(user_id, model)
            return model, msg
        except KeyError:
            return None, "invalid user input"

    def job_done_update(self, job_done, reserve_info):
        """job done call back, update the database, calculate the balance """
        try:
            job_info = Service['db'].get_job_by_id(reserve_info[2])
            if job_info is None:
                logging.warning("invalid job id %s", reserve_info[2])
                return "invalid job id"
            cluster_name = job_done['cluster']
            job_status, msg = self.clusters[cluster_name].list_jobs([
                                                                    job_info[0]])
            if msg != "":
                return msg
            if not job_status:
                logging.warning(
                    "invalid jobs status from job id %s", job_info[0])
                return "invalid job status"
            elapsed_min = elapsed_to_minutes(job_status[0]['time'])
            Service['db'].update_job_status(
                job_status[0]["status"], elapsed_min)
            hour_charge = convert_to_hour_unit(
                elapsed_min * int(job_info[5]))   # ngpus
            is_ok = Service['db'].remove_reserve(
                job_done['reserve_id'], reserve_info, hour_charge, job_info[0])
            if is_ok:
                logging.info(
                    "successfully removed reserve and updated balance for job %s", job_info[0])
                return ""
            else:
                return "error removing the reserve"
        except KeyError as err:
            logging.warning("error in job done update %s", err)
            return "invalid user input"


CONFIG = Config()
