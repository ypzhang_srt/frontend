"""
A lock class that allows you to do:

with LockW(mutex):
    # critical section
    ...

instead of :
mutex.aquire()
# critical section
...
mutex.release()

"""


class LockW:
    """helper locker"""

    def __init__(self, mutex):
        self.mutex = mutex

    def __enter__(self):
        self.mutex.acquire()
        return self

    def __exit__(self, type_, value, traceback):
        self.mutex.release()
