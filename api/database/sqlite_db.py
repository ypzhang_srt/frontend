"""business logic in sqlite database"""
import sqlite3
import time
import logging
import datetime
from threading import Lock
from api.database.lockw import LockW


CREATE_USER_TABLE = """ CREATE TABLE IF NOT EXISTS users (
                                        UserId integer PRIMARY KEY,
                                        Email text NOT NULL UNIQUE,
                                        UserName text,
                                        CreatedDate text,
                                        InvitedBy integer DEFAULT -1,
                                        Salt text NOT NULL,
                                        Verified integer DEFAULT 0,
                                        VerifyCode integer,
                                        VerifiedDate text,
                                        FOREIGN KEY (InvitedBy) REFERENCES "users" (UserId)
                                    );
                        """

CREATE_GROUP_TABLE = """ CREATE TABLE IF NOT EXISTS groups (
                                        GroupId integer PRIMARY KEY,
                                        GroupName text NOT NULL UNIQUE,
                                        CreatedDate text,
                                        CreatedBy integer,
                                        FOREIGN KEY (CreatedBy) REFERENCES "users" (UserId)
                                        );
                    """

CREATE_GROUP_USER_TABLE = """ CREATE TABLE IF NOT EXISTS group_user (
                                        GroupUserId integer PRIMARY KEY,
                                        Role text NOT NULL,
                                        GroupId integer,
                                        UserId integer,
                                        FOREIGN KEY (GroupId) REFERENCES "groups" (GroupId),
                                        FOREIGN KEY (UserId) REFERENCES "users" (UserId)
                                        );
                    """

CREATE_ACCOUNT_TABLE = """CREATE TABLE IF NOT EXISTS accounts (
                                        AccountId integer PRIMARY KEY,
                                        Balance DECIMAL NOT NULL DEFAULT 0,
                                        Reserved DECIMAL NOT NULL DEFAULT 0,
                                        UserId integer,
                                        GroupId integer,
                                        FOREIGN KEY (UserId) REFERENCES "users" (UserId),
                                        FOREIGN KEY (GroupId) REFERENCES "groups" (GroupId)
                                    );
                       """
CREATE_TRANSACTION_TABLE = """CREATE TABLE IF NOT EXISTS transactions (
                                        TransId integer PRIMARY KEY,
                                        Amount DECIMAL NOT NULL DEFAULT 0,
                                        AmountAfter DECIMAL NOT NULL DEFAULT 0,
                                        Flag text,
                                        TimeStamp text,
                                        AccountId integer,
                                        JobId integer,
                                        FOREIGN KEY (AccountId) REFERENCES "accounts" (AccountId),
                                        FOREIGN KEY (JobId) REFERENCES "jobs" (JobId)
                                    );
                        """

CREATE_RESERVE_TABLE = """CREATE TABLE IF NOT EXISTS reserves (
                                        ReserveId integer PRIMARY KEY,
                                        Amount DECIMAL NOT NULL DEFAULT 0,
                                        AccountId integer,
                                        TimeStamp text,
                                        JobId integer DEFAULT -1,
                                        ClusterName text,
                                        CallbackCode integer,
                                        FOREIGN KEY (AccountId) REFERENCES "accounts" (AccountId),
                                        FOREIGN KEY (JobId) REFERENCES "jobs" (JobId)
                                    );
                        """
CREATE_JOB_TABLE = """ CREATE TABLE IF NOT EXISTS jobs (
                                        JobId integer PRIMARY KEY,
                                        UniqueId text,
                                        SubmitedAt text,
                                        Status text,
                                        ElapsedSeconds integer,
                                        NGPUs integer,
                                        ClusterId integer,
                                        ReserveId integer,
                                        FOREIGN KEY (ClusterId) REFERENCES "clusters" (ClusterId),
                                        FOREIGN KEY (ReserveId) REFERENCES "reserves" (ReserveId)
                                    );
                        """

CREATE_CLUSTER_TABLE = """ CREATE TABLE IF NOT EXISTS clusters (
                                        ClusterId integer PRIMARY KEY,
                                        ClusterName text NOT NULL UNIQUE,
                                        Vendor text,
                                        Region text,
                                        Type text,
                                        IsPublic integer,
                                        GroupId integer DEFAULT -1,
                                        FOREIGN KEY (GroupId) REFERENCES "groups" (GroupId)
                                    );
                        """

# INSERT INTO users VALUES ()
# c.execute("SELECT * FROM users where email = '%s'" % user):


def get_now_string():
    """Return a timestamp for database column"""
    return datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")


def get_current_gmt_second():
    """return a string of the current GMT unix timestamp"""
    return str(int(time.time()))


def get_current_gmt_mil_second():
    """return a string of the current GMT unix timestamp"""
    return str(int(time.time()*1000))


NEW_USER_INITIAL_BALANCE = 150

# TODO we may need to use inter-process locking in production
# https://stackoverflow.com/a/49177943
"""
from ilock import ILock

with ILock('Unique lock name'):
    # The code should be run as a system-wide single instance
    ...
"""


class SqliteDB:
    """
    implement business logic using SQlite db
    """

    def __init__(self, file_name):
        logging.info('SQLITE connecting to %s', file_name)
        self.mutex = Lock()
        self.conn = sqlite3.connect(file_name, check_same_thread=False)
        self.conn.execute("PRAGMA journal_mode=WAL")
        self.conn.execute(CREATE_USER_TABLE)
        self.conn.execute(CREATE_GROUP_TABLE)
        self.conn.execute(CREATE_ACCOUNT_TABLE)
        self.conn.execute(CREATE_GROUP_USER_TABLE)
        self.conn.execute(CREATE_CLUSTER_TABLE)
        self.conn.execute(CREATE_JOB_TABLE)
        self.conn.execute(CREATE_TRANSACTION_TABLE)
        self.conn.execute(CREATE_RESERVE_TABLE)
        self.conn.commit()

    def add_user(self, email, hashed_password, verify_code, group_id=-1):
        """add a new user, returns True if successful """
        try:
            with LockW(self.mutex):
                now = get_now_string()
                new_user = (email, now, hashed_password, verify_code, 0)
                new_user_sql = ''' INSERT INTO users(Email, CreatedDate, Salt, VerifyCode, Verified)
                VALUES(?,?,?,?,?) '''
                cur = self.conn.cursor()
                cur.execute(new_user_sql, new_user)
                new_user_id = cur.lastrowid

                new_account = (0, 0, new_user_id, group_id)
                new_account_sql = ''' INSERT INTO accounts(Balance, Reserved, UserId, GroupId)
                VALUES(?,?,?,?) '''
                cur.execute(new_account_sql, new_account)
                self.conn.commit()
                new_account_id = cur.lastrowid
            self.add_transaction(NEW_USER_INITIAL_BALANCE, new_account_id, "+")
            return True, ""
        except sqlite3.IntegrityError:
            logging.warning("user email has already been registered.")
            return False, "user email has already been registered"
        except sqlite3.Error as err:
            logging.warning("error insert a new user: %s", err)
            return False, "internal error"

    def add_transaction(self, amount, account_id, payment_flag, job_id=-1, reserved_amount=0):
        """add a transaction to the transactions table, also update the balance
        "payment_flag" is either "+" or "-"
        """
        try:
            with LockW(self.mutex):
                now = get_current_gmt_mil_second()

                cur = self.conn.cursor()

                if payment_flag == "+":
                    update_balance_sql = '''UPDATE accounts
                        SET Balance = Balance + (?),
                        Reserved = Reserved - (?)
                        WHERE AccountId = (?)'''
                elif payment_flag == "-":
                    update_balance_sql = '''UPDATE accounts
                        SET Balance = Balance - (?),
                        Reserved = Reserved - (?)
                        WHERE AccountId = (?)'''
                else:
                    logging.warning(
                        "payment flag %s not recognized", payment_flag)
                    self.conn.commit()
                    return False, "unrecognied payment flag"
                cur.execute(update_balance_sql,
                            (amount, reserved_amount, account_id))
                cur.execute(
                    "SELECT Balance from accounts WHERE AccountId = (?)", str(account_id))
                amount_after = cur.fetchall()[0][0]
                new_transaction = (amount, amount_after, payment_flag,
                                   now, int(account_id), job_id)
                new_trans_sql = '''INSERT INTO transactions(Amount, AmountAfter, Flag, TimeStamp, AccountId, JobId)
                VALUES(?,?,?,?,?,?)'''
                cur.execute(new_trans_sql, new_transaction)
                self.conn.commit()
                logging.info("%s transactions amount %d, amount_after is %d",
                             payment_flag, amount, amount_after)
            return True, ""
        except sqlite3.IntegrityError:
            logging.warning("user email has already been registered.")
            return False, "user email has already been registered"
        except sqlite3.Error as err:
            logging.warning("error insert a new user: %s", err)
            return False, "internal error"

    def add_transaction_by_email(self, amount, email, payment_flag, job_id=-1):
        """Add transaction by the email"""
        the_user = self.get_user(email)
        user_id = the_user[0]
        account_info = self.get_account(user_id)
        account_id = account_info[2]
        self.add_transaction(amount, account_id, payment_flag, job_id)

    def get_id(self, email):
        """get the user id or group id given the email"""
        try:
            get_user_ids = "SELECT UserId, Verified from users WHERE Email = \"{EMAIL}\""
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(get_user_ids.format(EMAIL=email))
                users = cur.fetchall()
                if len(users) == 0:
                    logging.warning(
                        "failed get id attempt for email %s", email)
                    return None
                if len(users) > 1:
                    logging.error(
                        "found duplicated users, please check users database for %s", email)
                    return None
                if users[0][1] == 0:
                    logging.warning(
                        "failed get id attempt for %s due to email not verified", email)
                    return None
                return users[0][0]
        except sqlite3.Error as err:
            logging.warning("error get ids for user: %s", err)
        return None

    def get_account(self, user_id, group_id=-1):
        """get the balance info from the user id or group id"""
        if user_id is None or user_id < 0:
            return None
        if group_id < 0:
            get_balance_sql = "SELECT Balance, Reserved, AccountId from accounts WHERE UserId = {USER_ID}".format(
                USER_ID=str(user_id))
        else:
            get_balance_sql = "SELECT Balance, Reserved, AccountId from accounts WHERE GroupId = {USER_ID}".format(
                USER_ID=str(group_id))
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(get_balance_sql)
                balances = cur.fetchall()
                if len(balances) == 0:
                    logging.warning(
                        "failed get id attempt for user %d, group %d", user_id, group_id)
                    return None
                if len(balances) > 1:
                    logging.error(
                        "found duplicated users, please check accounts database for user %d group %d", user_id, group_id)
                    return None
                return balances[0]
        except sqlite3.Error as err:
            logging.warning(
                "error get balance for user %d group %d: %s", user_id, group_id, err)
        return None

    def get_transactions_by_email_and_group(self, email, group_name=""):
        """get transactions from a user email or group name"""
        user_info = self.get_user(email)
        group_info = self.get_group(group_name, -1)
        sel_account_sql = "SELECT AccountId from accounts WHERE UserId = (?) and GroupId = (?)"
        sel_trans_sql = """SELECT Amount, AmountAfter, Flag, TimeStamp, JobId from
            transactions WHERE AccountId = {ACCOUNT_ID} ORDER BY TimeStamp DESC"""
        try:
            cur = self.conn.cursor()
            cur.execute(
                sel_account_sql, (user_info[0], group_info[0]))
            account_info = cur.fetchall()
            if len(account_info) == 0:
                logging.warning(
                    "failed to find transactions for email: %s group name: %s", email, group_name)
                return []
            if len(account_info) > 1:
                logging.error(
                    "found duplicated account_info, please check accounts database for user %s group %s", email, group_name)
                return []
            cur.execute(sel_trans_sql.format(ACCOUNT_ID=account_info[0][0]))
            trans = cur.fetchall()
            trans_dict = [{"amount": item[0],
                           "amount_after": item[1],
                           "ts": item[3], "flag": item[2],
                           "job_id": item[4]} for item in trans]
            return trans_dict
        except sqlite3.Error as err:
            logging.warning(
                "error get transaction for user %s group %s: %s", email, group_name, str(err))
        except Exception as exp:
            logging.warning(
                "unknown error (type %s): %s getting transactions", type(exp), str(exp))
        return []

    def get_balance(self, email, group_name=""):
        """get the balance info from the user"""
        user_id = self.get_id(email)
        group_info = self.get_group(group_name, -1)
        return self.get_account(user_id, group_info[0])

    def update_user(self):
        """update user properties"""
        with LockW(self.mutex):
            return

    def get_user(self, email):
        """
        Returns a tuple of columns for this email, return None if error
        """
        get_user_for_auth = "SELECT UserId, Salt, Verified from users WHERE Email = \"{EMAIL}\""
        with LockW(self.mutex):
            cur = self.conn.cursor()
            cur.execute(get_user_for_auth.format(EMAIL=email))
            users = cur.fetchall()
            if len(users) == 0:
                logging.warning("failed login attempt for email %s", email)
                return None
            if len(users) > 1:
                logging.error(
                    "found duplicated users, please check users database for %s", email)
                return None
            return users[0]

    def get_group(self, group_name, default_rtn=None):
        """
        Returns a tuple of group columns for this group_name, None if error
        """
        if group_name == "":
            return (default_rtn, )
        get_group_sql = "SELECT GroupId from groups WHERE GroupName = \"{GROUP_NAME}\""
        with LockW(self.mutex):
            cur = self.conn.cursor()
            cur.execute(get_group_sql.format(GROUP_NAME=group_name))
            groups = cur.fetchall()
            if len(groups) == 0:
                logging.warning("group name %s does not exist", group_name)
                return (default_rtn, )
            if len(groups) > 1:
                logging.error(
                    "found duplicated groups, please check groups database for %s", group_name)
                return (default_rtn, )
            return groups[0]

    def verify_user(self, email, verify_code):
        """
        Returns a tuple of error code and error message
        error code: 0 - not verified
                    1 - verified
                    -1 - other errors, see error message for details
        """
        get_user_for_auth = "SELECT Verified, VerifyCode, UserId from users WHERE Email = \"{EMAIL}\""
        update_verify = "UPDATE users SET Verified = 1, VerifiedDate = \"{DATETIME}\" WHERE Email =\"{EMAIL}\""
        with LockW(self.mutex):
            cur = self.conn.cursor()
            cur.execute(get_user_for_auth.format(EMAIL=email))
            users = cur.fetchall()
            if len(users) == 0:
                logging.warning("failed  attempt for email %s", email)
                return None
            if len(users) > 1:
                logging.error(
                    "found duplicated users, please check users database for %s", email)
                return None
            if users[0][0] == 1:
                logging.warning("user %s has already been verified", email)
            if str(users[0][1]) == verify_code:
                cur = self.conn.cursor()
                cur.execute(update_verify.format(
                    EMAIL=email, DATETIME=get_now_string()), ())
                self.conn.commit()
                return users[0]
            return None

    def get_cluster_by_name(self, cluster_name):
        """get the clusterId by cluster name"""
        try:
            sel_cluster_sql = '''SELECT ClusterId, Vendor, Region, Type, IsPublic, GroupId
                        FROM clusters
                        WHERE ClusterName = \"{CLUSTER_NAME}\"'''
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(sel_cluster_sql.format(CLUSTER_NAME=cluster_name))
                clusters = cur.fetchall()
                if len(clusters) == 0 or len(clusters) > 1:
                    return None
                return clusters[0]
        except sqlite3.Error as err:
            logging.warning("error getting cluster: %s", err)
        return None

    def get_clusters(self):
        """get the clusterId by cluster name"""
        try:
            sel_cluster_sql = '''SELECT ClusterId, ClusterName, Vendor, Region, Type, IsPublic, GroupId
                        from clusters'''
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(sel_cluster_sql)
                clusters = cur.fetchall()
                if len(clusters) == 0 or len(clusters) > 1:
                    return None
        except sqlite3.Error as err:
            logging.warning("error getting cluster: %s", err)
        return None

    def get_job_by_id(self, job_id):
        """get the job by job id """
        try:
            sel_job_sql = '''SELECT UniqueId, SubmitedAt, Status, ElapsedSeconds, ReserveId, NGPUs
                        FROM jobs
                        WHERE JobId = \"{JOB_ID}\"'''
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(sel_job_sql.format(JOB_ID=job_id))
                jobs = cur.fetchall()
                if len(jobs) == 0 or len(jobs) > 1:
                    return None
                return jobs[0]
        except sqlite3.Error as err:
            logging.warning("error getting cluster: %s", err)
        return None

    def add_or_update_cluster(self, cluster, group_id=-1):
        """add or update cluster database"""
        info = cluster.info()
        cluster = self.get_cluster_by_name(info["name"])
        if cluster is None:
            return self.add_cluster(info, group_id)
        return self.update_cluster(info, cluster, group_id)

    def add_job(self, reserve_id, cluster_id, unique_id, ngpus):
        """add a new job"""
        new_job_sql = '''INSERT INTO jobs(UniqueId, SubmitedAt, Status, ElapsedSeconds, ClusterId, ReserveId, NGPUs)
                VALUES(?,?,?,?,?,?,?)'''
        new_job = (unique_id, get_now_string(),
                   "submitted", -1, cluster_id, reserve_id, ngpus)
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(new_job_sql, new_job)
                self.conn.commit()
                new_job_id = cur.lastrowid
                return new_job_id
        except sqlite3.Error as err:
            logging.warning("error updating verify code %s", err)
        return False

    def add_cluster(self, cluster_info, group_id=-1):
        """add cluster to database"""
        new_cluster_sql = '''INSERT INTO clusters(ClusterName, Vendor, Region, IsPublic, Type, GroupId)
                VALUES(?,?,?,?,?, ?)'''
        new_cluster = (cluster_info['name'], cluster_info['vendor'], cluster_info['region'],
                       1 if cluster_info['shared'] else 0, cluster_info['type'], group_id)
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(new_cluster_sql, new_cluster)
                self.conn.commit()
                return True
        except sqlite3.Error as err:
            logging.warning("error updating verify code %s", err)
        return False

    def update_cluster(self, cluster_info, old_cluster, group_id):
        """update cluster to database if different"""
        update_cluster_sql = '''UPDATE clusters SET ClusterName = (?), Vendor = (?), Region = (?), IsPublic = (?), Type = (?), GroupId = (?)
                WHERE ClusterId = (?)'''
        update_cluster = (cluster_info['name'], cluster_info['vendor'], cluster_info['region'],
                          1 if cluster_info['shared'] else 0, cluster_info['type'], group_id, old_cluster[0])
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(update_cluster_sql, update_cluster)
                self.conn.commit()
                return True
        except sqlite3.Error as err:
            logging.warning("error updating cluster %s", err)
        return False

    def reserve_hours(self, cluster_name, hours, email, group_name=""):
        """reserve the hours needed to run a job, returns false if fails (balance not enough)"""
        user_id = self.get_id(email)
        group_info = self.get_group(group_name, -1)
        cur_balance = self.get_account(user_id, group_info[0])
        if cur_balance is None:
            return None
        balance, reserved, account_id = cur_balance
        after_balance = balance - reserved - hours
        if after_balance < 0:
            logging.info(
                "after balance less than 0(%d), reject.", after_balance)
            return None
        try:
            update_reserved_sql = '''UPDATE accounts
                        SET Reserved = Reserved + (?)
                        WHERE AccountId = (?)'''
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(update_reserved_sql, (hours, account_id))
                new_reserve = (hours, get_now_string(),
                               int(account_id), cluster_name)
                new_reserve_sql = '''INSERT INTO reserves(Amount, TimeStamp, AccountId, ClusterName)
                VALUES(?,?,?,?)'''
                cur.execute(new_reserve_sql, new_reserve)
                self.conn.commit()
                new_reserve_id = cur.lastrowid
                return new_reserve_id
        except sqlite3.Error as err:
            logging.warning("error updating verify code %s", err)
        return None

    def get_reserve(self, reserve_id):
        """get the reservation"""
        try:
            sel_reserve_sql = '''SELECT Amount, AccountId, JobId, CallbackCode
                        FROM reserves
                        WHERE ReserveId = \"{RESERVE_ID}\"'''
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(sel_reserve_sql.format(RESERVE_ID=reserve_id))
                reserves = cur.fetchall()
                if len(reserves) == 0 or len(reserves) > 1:
                    logging.warning(
                        "error getting reserve from id %s", reserve_id)
                    return None
                return reserves[0]
        except sqlite3.Error as err:
            logging.warning("error getting reserve: %s", err)
        return None

    def cancel_reserve(self, reserve_id, update_balance=True):
        """cancel the reservation"""
        reserve_info = self.get_reserve(reserve_id)
        if reserve_info is None:
            return False
        try:
            if update_balance:
                update_balance_sql = '''UPDATE accounts
                        SET Reserved = Reserved - (?), Balance = Balance - (?)
                        WHERE AccountId = (?)'''
                update_balance = (
                    reserve_info[0], reserve_info[0], reserve_info[1])
            else:
                update_balance_sql = '''UPDATE accounts
                        SET Reserved = Reserved - (?)
                        WHERE AccountId = (?)'''
                update_balance = (reserve_info[0], reserve_info[1])
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(update_balance_sql, update_balance)
                # also delete the reserve id
                cur.execute("DELETE FROM reserves WHERE ReserveId = \"{RESERVE_ID}\"".format(
                    RESERVE_ID=reserve_id))
                self.conn.commit()
                return True
        except sqlite3.Error as err:
            logging.warning("error updating balance %s", err)
        return False

    def remove_reserve(self, reserve_id, reserve_info, actual_hour, job_uid):
        """remove the reservation, update balance with actual usage"""
        self.add_transaction(
            actual_hour, reserve_info[1], "-", job_uid, reserve_info[0])
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                # also delete the reserve id
                cur.execute("DELETE FROM reserves WHERE ReserveId = \"{RESERVE_ID}\"".format(
                    RESERVE_ID=reserve_id))
                self.conn.commit()
                return True
        except sqlite3.Error as err:
            logging.warning("error updating balance %s", err)
        return False

    def update_jobid_in_reserve(self, job_id, code, reserve_id):
        """update the JobId in reserved table"""
        update_jobid_sql = "UPDATE reserves SET JobId = (?), CallbackCode = (?) WHERE ReserveId = (?)"
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(update_jobid_sql, (job_id, code, reserve_id))
                self.conn.commit()
                return True
        except sqlite3.Error as err:
            logging.warning("error updating jobid in reserved table %s", err)
        return False

    def update_job_status(self, job_id, status, elapsed="--"):
        """update the JobId in reserved table"""
        update_jobid_sql = "UPDATE jobs SET Status = (?), ElapsedSeconds = (?) WHERE JobId = (?)"
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(update_jobid_sql, (status, elapsed, job_id))
                self.conn.commit()
                return True
        except sqlite3.Error as err:
            logging.warning("error updating jobid in reserved table %s", err)
        return False

    def update_user_verify_code(self, email, verify_code):
        """
        returns True if updated user's verify code
        """
        update_verify = "UPDATE users SET VerifyCode = {VCODE} WHERE Email =\"{EMAIL}\""
        try:
            with LockW(self.mutex):
                cur = self.conn.cursor()
                cur.execute(update_verify.format(
                    VCODE=verify_code,
                    EMAIL=email))
                self.conn.commit()
                return True, ""
        except sqlite3.Error as err:
            logging.warning("error updating verify code %s", err)
        return False, str(err)

    def check_user_cluster(self, user_info, cluster_info):
        """check if the user has the permission to use a cluster
        usage:
        cluster_info = Service['db'].get_cluster_by_name(cluster_name)
        user_info = Service['db'].get_user(user)
        ok = Service['db'].check_user_cluster(user_info, cluster_info)
        """
        # ClusterId, Vendor, Region, Type, IsPublic, GroupId
        if cluster_info[4] == 1:  # IsPublic
            return True
        assert cluster_info[5] > 0  # GroupId must be valid
        # TODO select user_info[0] and cluster_info[5] in user_group table
        assert False
