"""test sqlite db"""
import os
import tempfile
import pytest
from api.database.sqlite_db import SqliteDB, NEW_USER_INITIAL_BALANCE
from api.config import Config


@pytest.fixture
def temp_db():
    """return a temp datafile for database testing
    """
    # get the next temp file name
    # pylint:disable = protected-access
    temp_name = next(tempfile._get_candidate_names())
    # print(temp_name)
    yield temp_name
    os.remove(temp_name)

# pylint:disable=redefined-outer-name


def test_new_user(temp_db):
    """test new user workflow"""
    db_obj = SqliteDB(temp_db)
    email = "test@example.com"
    db_obj.add_user(email, "hashed_password", "123456")
    # wrong email should return None
    assert db_obj.get_user("not-exist@example.com") is None

    # correct email returns valid entry
    test_user = db_obj.get_user(email)
    assert test_user[1] == "hashed_password"
    assert test_user[2] == 0

    # test transaction
    user_id = test_user[0]
    account_info = db_obj.get_account(user_id)
    assert account_info[0] == NEW_USER_INITIAL_BALANCE  # balance
    assert account_info[1] == 0  # reserved
    account_id = account_info[2]

    db_obj.add_transaction(100, account_id, "+")

    new_account_info = db_obj.get_account(user_id)
    assert new_account_info[0] == 100 + NEW_USER_INITIAL_BALANCE

    db_obj.add_transaction_by_email(120, email, "+")
    new_account_info = db_obj.get_account(user_id)
    assert new_account_info[0] == 220 + NEW_USER_INITIAL_BALANCE

    db_obj.add_transaction_by_email(30, email, "-")
    new_account_info = db_obj.get_account(user_id)
    assert new_account_info[0] == 190 + NEW_USER_INITIAL_BALANCE

    trans = db_obj.get_transactions_by_email_and_group(email)
    assert len(trans) == 4

    db_obj.verify_user(email, "123456")
    reserve_id = db_obj.reserve_hours("cluster", 50, email)
    assert reserve_id is not None

    config = Config()
    new_cluster = db_obj.add_or_update_cluster(config.clusters['hydra'])
    assert new_cluster is True

    cluster_info = db_obj.get_cluster_by_name('hydra')
    assert cluster_info is not None
    assert cluster_info[0] == new_cluster

    assert db_obj.check_user_cluster(test_user, cluster_info)

    reserve_id = db_obj.reserve_hours('hydra', 50, email)
    assert reserve_id is not None

    job_id = db_obj.add_job(reserve_id, new_cluster, "job_uid", 1)
    assert job_id is not None

    assert db_obj.update_jobid_in_reserve(job_id, 123456, reserve_id) is True

    job_info = db_obj.get_job_by_id(job_id)
    assert job_info[0] == "job_uid"


def test_new_cluster(temp_db):
    """test cluster in db"""
    config = Config()
    db_obj = SqliteDB(temp_db)
    rtn = db_obj.add_or_update_cluster(config.clusters['hydra'])
    assert rtn is True
    db_obj.get_clusters()
    assert db_obj.add_or_update_cluster(config.clusters['hydra']) is True


if __name__ == "__main__":
    pytest.main()
