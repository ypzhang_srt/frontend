"""common utilities to read eclipse output"""
import os
import numpy as np


TYPES = {
    b"INTE": np.dtype('>i4'),
    b"REAL": np.dtype('>f4'),
    b"DOUB": np.dtype('>f8'),
    b"MESS": np.dtype('>V1'),
    b"LOGI": np.dtype('>i4'),
    b"C008": np.dtype('a8'),
    b"C004": np.dtype('a4'),
    b"CHAR": np.dtype('a8')
}


def make_slice(expr, data):
    """Make a slice object from string like 0, 0:2 or 0:3:1"""
    if expr == ':':
        return slice(len(data))
    pieces = [int(p) for p in expr.split(':')]
    if len(pieces) == 1:
        return slice(pieces[0], pieces[0] + 1)
    return slice(*pieces)


def open_eclipse_output(abs_path):
    """open"""
    # binary mode for seeking
    return open(abs_path, mode="rb")


def read_int(file):
    """Read an int"""
    raw = np.fromfile(file, TYPES[b'INTE'], 1)
    return raw[0] if len(raw) == 1 else None


def read_label(file, n_bytes):
    """Read a string with eclipse encoding"""
    return file.read(n_bytes).strip()


def read_keyword(file, enable_print=True):
    """Consume a keyword, returns keyword and data if valid,
    returns None if reached to the end of the file """
    if enable_print:
        print("**************************************")
    keyword_mark = read_int(file)
    if keyword_mark is None:
        return None, None
    assert keyword_mark == 16
    label = read_label(file, 8)
    data_size = read_int(file)
    the_type = read_label(file, 4)
    keyword_mark = read_int(file)
    # assert keyword_mark == 16
    size_read = 0
    np_type = TYPES[the_type]
    if enable_print:
        print(label, the_type, np_type, data_size)
    data = np.zeros(data_size, dtype=np_type)
    items_read = 0
    while size_read < data_size * np_type.itemsize:
        data_len = read_int(file)
        item_count = data_len // np_type.itemsize
        data[items_read:items_read +
             item_count] = np.fromfile(file, np_type, item_count)
        items_read += item_count
        size_read += data_len
        if size_read < data_size * np_type.itemsize:
            data_len_2 = read_int(file)
            assert data_len == data_len_2
    if data_size != 0:
        read_int(file)

    if np_type in (np.dtype('a8'), np.dtype('a4')):
        data = np.fromiter((s.strip() for s in data), data.dtype)
    if enable_print:
        print(data)
    return label, data


def search_keyword(file, keyword, slice_exp):
    """return the record indicated by the keyword"""
    while True:
        label, data = read_keyword(file, False)
        if label is None:
            break
        if label.decode('ascii') == keyword:
            the_slice = make_slice(slice_exp, data)
            return data[the_slice]
    return None


def gen_abs_file(file_name):
    """generate absolute file path"""
    return file_name if os.path.isabs(file_name) else os.path.join(os.getcwd(), file_name)
