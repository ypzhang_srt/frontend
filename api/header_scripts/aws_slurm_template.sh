#!/bin/bash
#
#SBATCH --job-name={{user_name}}-{{ job_name }}
#SBATCH --output={{ output_dir }}/{{job_name}}_output.txt
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --gres=gpu:{{ngpus}}
#SBATCH --time={{ time_out }}
##  60:00

module load openmpi
/efs/bin/echelon --output={{output_dir}} --ngpus={{ngpus}} {{data_path}}
