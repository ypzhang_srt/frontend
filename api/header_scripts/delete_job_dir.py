"""the remote script to delete a job"""
#!/usr/bin/env python3
import os
import sys
import logging
import json


def extract_jobid(std_output):
    """extract the job id from the stdout"""
    lines = std_output.split("\n")
    return lines[-2]


def main(argv):
    """main"""
    logging.basicConfig(level=logging.INFO)
    if len(argv) == 1:
        logging.critical("Missing argument for job info")
        return 1
    json_argv = argv[1]
    try:
        job = json.loads(json_argv)
        os.rmdir(job['dir'])
    except KeyError as err:
        logging.info("key error %s", err)
    return 0


if __name__ == "__main__":
    main(sys.argv)
