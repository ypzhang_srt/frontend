#!/usr/bin/env python3
# python 3.5 or above
import time
import os
import logging
import sys
import toml
import json
from pathlib import Path


def main(argv):
    """main"""
    logging.basicConfig(level=logging.INFO)
    if len(argv) == 1:
        logging.critical("Missing argument for job info")
        return 1
    json_argv = argv[1]
    metas = []
    try:
        model_run = json.loads(json_argv)
        for meta_abs_path in Path(model_run['root']).rglob(model_run['meta_file']):
            meta = toml.load(meta_abs_path)
            # no need to send all the information:
            try:
                meta_payload = {
                    "echelon_version": meta["job"]["job"]["version"],
                    "elapsed": meta["elapsed"],
                    "id": meta["id"],
                    "last_status": meta["last_status"],
                    "ngpus": meta["job"]["job"]["ngpus"],
                    "queue": meta["job"]["job"]["queue"],
                    "submitted": meta["submitted"],
                    "output_dir": meta["job"]["run_time"]["uid"]
                }
                metas.append(meta_payload)
            except KeyError:
                metas.append(meta)   # compatible with old meta, REMOVEME later
        print(json.dumps(metas, separators=(',', ':')))
        # print(path.name)
    except KeyError as miss_key:
        sys.stderr.write("invalid user input, missing %s", miss_key)


if __name__ == "__main__":
    main(sys.argv)
