#!/bin/bash
#PBS -N {{user_name}}-{{ job_name}}
#PBS -l select=1:ncpus=8:ngpus={{ ngpus }}:mpiprocs=8:mem=20Gb
#PBS -l Walltime={{ time_out }}
#PBS -o {{ output_dir }}/{{ job_name }}_stdout.txt
#PBS -e {{ output_dir }}/{{ job_name }}_stderr.txt
#PBS -q {{ queue }}
#PBS -V

cd \$PBS_O_WORKDIR

############# Runtime variables ################
export WTHREADS=8
############ Runtime command ###################

cat \$PBS_NODEFILE > {{output_dir}}/nodes.txt

cd {{ output_dir }}
{{binary}} --output={{output_dir}} --ngpus={{ngpus}} {{data_path}}
status=$?
cd ..
{{ zip_command }}

curl -X POST {{ call_back_url }} -H "Content-Type: application/json" -d "{\"code\":{{ call_back_code }},\"reserve_id\":{{ reserve_id }},\"exit_code\":${status},\"job_id\":\"{{ job_name }}\",\"cluster\":\"{{ cluster }}\",\"output_dir\":\"{{ output_dir }}\"}"

exit $status

