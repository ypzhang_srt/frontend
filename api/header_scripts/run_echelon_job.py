"""the remote script to run echelon job"""
#!/usr/bin/env python3
from subprocess import run, PIPE
import os
import logging
import sys
import json
import toml
from jinja2 import Template


def extract_jobid(std_output):
    """extract the job id from the stdout"""
    lines = std_output.split("\n")
    return lines[-2]


def main(argv):
    """main"""
    logging.basicConfig(level=logging.INFO)
    if len(argv) == 1:
        logging.critical("Missing argument for job info")
        return 1
    json_argv = argv[1]
    job = json.loads(json_argv)
    run_time = job['run_time']
    try:
        run_dir = run_time['run_dir']
        os.makedirs(run_dir, exist_ok=True)
        template_file = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), run_time['template'])
        uid = run_time['uid']
        templated_file = os.path.join(run_dir, run_time['template'])
        zip_command = " ".join(run_time['zip_command_list'])
        with open(template_file) as file_:
            template = Template(file_.read())
            templated = template.render(job_name=uid, output_dir=run_dir, time_out=run_time['timeout'], ngpus=str(
                run_time['ngpus']), data_path=run_time['data_file'], user_name=run_time['user'], queue=run_time['queue'], binary=run_time['binary'],
                call_back_url=run_time['callback'], call_back_code=run_time['call_back_code'], reserve_id=run_time['reserve_id'],
                zip_command=zip_command, cluster=run_time['cluster'])
            with open(templated_file, 'w') as file_out:
                file_out.write(templated)
            cmd = run_time['submit_command'] + " " + templated_file
            result = run(
                cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True,
                check=False,
                shell=True)
    except KeyError as err:
        logging.info("key error %s", err)
    job_record = {"job": job}
    job_record["id"] = extract_jobid(result.stdout)
    job_record["last_status"] = "submitted"
    job_record["ngpus"] = run_time['ngpus']
    job_record["submitted"] = uid
    job_record["elapsed"] = "--"
    meta_file = os.path.join(run_dir, run_time['meta_file'])
    with open(meta_file, 'w') as sfile:
        toml.dump(job_record, sfile)
    # it is important to print out output for ssh client to parse
    print(json.dumps(job_record))
    return 0


if __name__ == "__main__":
    main(sys.argv)
