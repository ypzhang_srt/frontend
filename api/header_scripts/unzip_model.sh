#!/usr/bin/env python3
from jinja2 import Template
import time
import boto3
import os
import zipfile
import logging
import shutil
import sys
import json
import stat
import subprocess


def construct_path_from_model(user_name,  path, model_permission):
    """The reverse of data_file_to_model"""
    if model_permission == "private":
        return os.path.join("/efs/users/{USER}/models/private/".format(USER=user_name), path)
    if model_permission == "public":
        return os.path.join("/efs/users/{USER}/models/public/".format(USER=user_name), path)
    if model_permission == "shared":
        return os.path.join("/efs/shared/models/", path)
    return ""


def move_directory(root_src_dir, root_dst_dir):
    """move directories even dst dir exists before
    https://stackoverflow.com/a/7420617
    """
    for src_dir, dirs, files in os.walk(root_src_dir):
        dst_dir = src_dir.replace(root_src_dir, root_dst_dir, 1)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        for file_ in files:
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                # in case of the src and dst are the same file
                if os.path.samefile(src_file, dst_file):
                    continue
                os.remove(dst_file)
            shutil.move(src_file, dst_dir)


def construct_temp_path_from_model(key):
    """The reverse of data_file_to_model"""
    return os.path.join("/efs/stage/input", key)


def main(argv):
    """main"""
    logging.basicConfig(level=logging.INFO)
    if len(argv) == 1:
        logging.critical("Missing argument for model info")
        return 1
    json_argv = argv[1]

    model = json.loads(json_argv)
    bucket = model["upload"]["bucket"]
    key = model["upload"]["key"]
    user_name = model["user_name"]
    temp_file = construct_temp_path_from_model(key)
    temp_dir = temp_file + "_unzipped"
    folder_name = os.path.dirname(temp_file)
    os.makedirs(folder_name, exist_ok=True)
    # make another temp dir using the filename
    os.makedirs(temp_dir, exist_ok=True)
    s3 = boto3.client('s3')
    # TODO use the presign url to get the object.
    # Current downloading need to set up the aws credential on the header node
    with open(temp_file, 'wb') as f:
        logging.info("saving bucket %s key %s to file %s",
                     bucket, key, temp_file)
        s3.download_fileobj(bucket, key, f)

    # unzip the zipped file to the temporary dir
    logging.info("unzip %s to %s", temp_file, temp_dir)
    with zipfile.ZipFile(temp_file, 'r') as zip_ref:
        zip_ref.extractall(temp_dir)

    # rename the temp dir to the target dir
    only_files = [f for f in os.listdir(
        temp_dir) if os.path.isfile(os.path.join(temp_dir, f))]
    has_files_under_root = len(only_files) > 0

    # if user uploaded files under the root, the model path can't be "./" or ""
    model_rel_path = model["path"]
    if has_files_under_root and (model_rel_path is "" or model_rel_path == "./"):
        logging.info("add model to rel path")
        model_rel_path = "model"

    target_dir = construct_path_from_model(
        user_name, model_rel_path, model["permission"])
    file_names = os.listdir(temp_dir)

    logging.info("copying contents in %s to %s", temp_dir, target_dir)
    os.makedirs(target_dir, exist_ok=True)
    move_directory(temp_dir, target_dir)
    # for file_name in file_names:
    #    if os.path.isdir(os.path.join(temp_dir, file_name)):
    #       file_name += "/"
    #    logging.info("copying %s to %s", file_name, target_dir)
    #    shutil.move(os.path.join(temp_dir, file_name), target_dir, copy_function = shutil.copytree)

    # all_job_dirs = [os.path.join(run_dir, o) for o in os.listdir(run_dir) if os.path.isdir(os.path.join(run_dir,o))]
    # all_job_dirs = [o for o in os.listdir(run_dir) if os.path.isdir(os.path.join(run_dir,o))]
    # for job in all_job_dirs:
    #     print(job)


if __name__ == "__main__":
    main(sys.argv)
