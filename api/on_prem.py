"""
on prem cluster. It assumes the host has ssh access to the cluster header
"""
import os
import numpy as np
import logging
import re
import json
import time
import toml
from subprocess import run, CalledProcessError, CompletedProcess
from api.eclipse import read_keyword, open_eclipse_output, search_keyword
from api.pbs_client import PBSClient
from api.remote_path import (RemotePath, gen_list_model_cmd, construct_job_json_for_cluster,
                             gen_run_job_cmd, gen_list_model_jobs_cmd)
from api.ssh_cmd import SSHCmd


VENDOR = "srt"
REGION = "maryland"
CLUSTER_TYPE = "on-premise"
CLUSTER_INPUT_PREFIX = "input_stage/"
CLUSTER_OUTPUT_PREFIX = "output_stage/"

CLUSTER_USER_ROOT = "/data/user/cloud/"
CLUSTER_USER_PRIVATE_MODEL = "/models/private"
CLUSTER_USER_PUBLIC_MODEL = "/models/public"
CLUSTER_PUBLIC_MODEL_PATH = "/data/user/cloud/shared/models"
JOB_META_FILE = ".echelon_job.toml"

VERSION_PATH_MAP = {
    "nightly": "/srt/echelon/nightly/current/echelon",
    "2021.1.1": "/srt/echelon/releases/2021.1.1/linux/echelon"
}


class OnPremiseCluster:
    """The mega cluster class that relay APIs into SSH command to the cluster's header node"""

    def __init__(self, cluster_name, host_name, queue_type, nfs_root, user_name="", pem_file=""):
        """constructor"""
        self.cluster_name = cluster_name
        self.is_shared = True
        self.path = RemotePath(nfs_root)
        self.ssh_client = SSHCmd(host_name, user=user_name, pem_file=pem_file)
        # watch for the prompt in case the remote host is not in the known_host list
        self.ssh_client.dry_run()
        if queue_type == "PBS":
            self.queue = PBSClient()
        else:
            assert False  # TODO
        self.call_back_endpoint = ""
        # self.job_queue = PBSJobQueue()

    def info(self):
        """get cluster info"""
        return {
            "name": self.cluster_name,
            "vendor": "SRT",
            "region": "Maryland",
            "type": "on-premise",
            "shared": self.is_shared
        }

    def set_call_back(self, call_back_ep):
        """set the call back url for jobs"""
        self.call_back_endpoint = call_back_ep

    def list_models(self, user_name):
        """list models for a user"""
        cmd = gen_list_model_cmd(self.path, user_name)
        result = self.ssh_client.run(cmd)
        if result.stdout != "":
            models = self.path.data_file_to_model(result.stdout, user_name)
            if models:
                return models, ""
        if result.stderr != "":
            logging.warning("list model returns stderr %s", result.stderr)
            return [], "Internal error"

    def delete_model(self, user_name, model):
        """delete a model"""
        dst = self.path.construct_path_from_model(
            user_name, model["file"], model["path"], model["permission"])
        if model['remove_dir']:
            dst = os.path.dirname(dst)
        # remove the directory and list_models
        logging.info("deleting model %s", dst)
        cmd = "rm -rf {DST}; find /data/user/cloud/public/models /data/user/cloud/{USER}/models -name *.DATA -type f".format(
            USER=user_name, DST=dst)
        result = self.ssh_client.run(cmd)
        if result.stderr != "":
            return [], "Internal error"
        models = self.path.data_file_to_model(result.stdout, user_name)
        return models, ""

    def list_model_jobs(self, user_name, model):
        """list jobs for a model"""
        try:
            run_root_path = self.path.gen_model_run_root(
                user_name, model["file"], model["path"], model["permission"])
            if not run_root_path:
                return [], "invalid model"
            cmd = gen_list_model_jobs_cmd(self.path, run_root_path)
            result = self.ssh_client.run(cmd)
            if result.stderr != "":
                return [], "internal error"
            return json.loads(result.stdout), ""
        except CalledProcessError:
            logging.warning("failure to list model jobs %s", result.stderr)
            return None, result.stderr
        except KeyError as err:
            logging.warning("invalid user input %s", err)
            return None, "Invalid user input"

    def list_jobs(self, job_ids=None):
        """list (qstat) job status"""
        try:
            if job_ids is None:
                cmd = self.queue.list_job_cmd
            else:
                cmd = self.queue.list_jobs_cmd(job_ids)
            result = self.ssh_client.run(cmd)
            if result.stderr != "":
                logging.warning("error listing job status: %s", result.stderr)
                return [], result.stderr
            job_status_list = self.queue.post_process_qstat(result.stdout)
            return job_status_list, ""
        except Exception as err:
            logging.warning(
                "caught excepation in list jobs %s: %s", type(err), str(err))
            return [], "internal error"

    def get_cluster(self):
        """TODO"""
        rtn = []
        rtn.append({"name": "v100",
                    "avail": 4,
                    "time_limit": "",
                    "stat": {
                        "available": 4,
                        "idle": 0,
                        "other": 0,
                        "total": 4
                    },
                    "echelon": {
                        "versions": [
                            "nightly",
                            "2021.1.1"
                        ],
                    },
                    "queues": [
                        "v100"
                    ],
                    "node_list": [],
                    })
        return rtn, ""

    def get_job_output(self, user_name, job):
        """get the echelon job .PRT"""
        try:
            print("job is", job)
            file_no_suffix = os.path.splitext(
                os.path.basename(job['model']['file']))[0]
            uid = job["output_dir"]

            run_dir, _ = self.path.gen_job_run_dir(
                user_name, job['model']['file'], job['model']['path'],
                job['model']['permission'], gen_uid=False)
            run_dir = os.path.join(run_dir, uid)

            prt_file = os.path.join(run_dir, file_no_suffix+".PRT")
            if not os.path.isfile(prt_file):
                logging.warning("PRT file %s does not exist", prt_file)
                return {
                    "total_lines": -1,
                    "size": -1,
                    "head": ["file does not exist"],
                    "tail": []}, ""
            # get file size quickly
            file_size = os.path.getsize(prt_file)

            if file_size > 1e6:
                logging.warning("PRT file too large")
                return {"size": file_size,
                        "total_lines": -1,
                        "head": ["file too large, please download the zip file"],
                        "tail": []}, ""

            with open(prt_file) as prt:
                contents = prt.readlines()
                logging.info("read prt file %d lines", len(contents))
                return {
                    "size": file_size,
                    "total_lines": len(contents),
                    "head": contents[0:50],
                    "tail": contents[max(len(contents)-50, 50):]
                }, ""

            logging.warning("can't open PRT file %s", prt_file)
            return {
                "total_lines": -1,
                "size": -1,
                "head": ["file does not exist"],
                "tail": []
            }, ""
        except KeyError as err:
            logging.warning("invalid user input %s", err)
            return {}, "invalid user input"

    def get_job_summary(self, user_name, job):
        try:
            data_file = self.path.construct_path_from_model(
                user_name, job['model']['file'], job['model']['path'], job['model']['permission'])
            file_no_suffix = os.path.splitext(
                os.path.basename(job['model']['file']))[0]
            uid = job["output_dir"]
            run_dir = os.path.join(os.path.dirname(
                data_file), ".run", file_no_suffix, user_name, uid)
            sumspec_file = os.path.join(run_dir, file_no_suffix+".SMSPEC")
            key = "FOPT"
            if not os.path.isfile(sumspec_file):
                logging.warning("SUMSPEC file %s does not exist", sumspec_file)
                return {
                    "all_keywords": [],
                    "keywords": [],
                    "labels": [],
                    "time": [],
                    "data": []}, ""
            keywords = search_keyword(
                open_eclipse_output(sumspec_file), 'KEYWORDS', ":")
            if not isinstance(keywords, np.ndarray):
                return {
                    "all_keywords": [],
                    "keywords": [],
                    "labels": [],
                    "time": [],
                    "data": []}, ""
            indexes = []
            time_index = -1
            for kid, keyword in enumerate(keywords):
                k = keyword.decode('ascii')
                if k == 'TIME':
                    time_index = kid
                if k == key:
                    indexes.append(kid)
            with open_eclipse_output(sumspec_file) as sumspec:
                names = search_keyword(sumspec, 'NAMES', ":")
                if names is None:
                    names = search_keyword(
                        open_eclipse_output(sumspec_file), 'WGNAMES', ":")
            labels = [names[i].decode('ascii') for i in indexes]
            output = []
            times = []
            unsmry_file = os.path.join(run_dir, file_no_suffix+".UNSMRY")
            unsmry_parser = open_eclipse_output(unsmry_file)
            while True:
                label, data = read_keyword(unsmry_parser, False)
                if label is None:
                    break
                if label.decode('ascii') == 'PARAMS':
                    times.append("%0.2f" % data[time_index])
                    sel_data = ["%.2f" % data[i] for i in indexes]
                    output.append(sel_data)
            return {
                "all_keywords": [keywords[i].decode('ascii') for i in range(0, len(keywords))],
                "keyword": key,
                "labels": labels,
                "time": times,
                "data": output
            }, ""

        except KeyError as e:
            logging.warning("invalid user input %s", e)
            return {}, "invalid user input"

    def run_echelon_job(self, user_name, job, reserve_id):
        """
        run echelon job on this cluster
        """
        # TODO replace pbs_tempalte with queue class
        job_json = construct_job_json_for_cluster(self.cluster_name,
                                                  self.path, user_name, job, self.call_back_endpoint, reserve_id,
                                                  "pbs_template.sh", "qsub", VERSION_PATH_MAP[job["version"]])

        ssh_cmd = gen_run_job_cmd(self.path, job_json)
        print("ssh_cmd is: ", ssh_cmd)
        result = CompletedProcess([], "")
        try:
            result = self.ssh_client.run(ssh_cmd)
            return json.loads(result.stdout), ""
        except CalledProcessError:
            logging.warning("failure to run echelon job %s", result.stderr)
            return None, result.stderr
        except KeyError as err:
            logging.warning("invalid user input %s", err)
            return None, "Invalid user input"

    def job_done_update(self, job_done):
        """implement the job done call back"""


def list_local():
    result = []
    result.append(
        {
            "vendor": VENDOR,
            "region": REGION,
            "type": CLUSTER_TYPE,
            "name": "SRT",
            "versions": ["nightly", "2021.1.1"],
            "queues": [
                "v100"
            ]
            # _colorize(stack.get("StackStatus"), args),
            # pcluster_version,
        }
    )
    return result
