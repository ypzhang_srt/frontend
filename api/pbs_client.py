"""PBS Queue utilities"""
import re


class PBSClient:
    """PBS client"""
    QSTAT_STATUS_MAP = {
        "F": "finished",
        "W": "waiting",
        "R": "running",
        "C": "completed",
        "E": "exiting",
        "Q": "queued"
    }
    QSUB = "qsub"
    TEMPLATE = "pbs_template.sh"

    def __init__(self):
        """constructor"""
        self.list_job_cmd = "qstat -aw"
        self.list_job_list_cmd = "qstat -xaw"

    def list_jobs_cmd(self, job_ids: list):
        """return the list job command for specified job ids"""
        return self.list_job_list_cmd + " " + " ".join(job_ids)

    def post_process_qstat(self, output):
        """
        convert streams like this into structures:
        brooklyn:
                                                                                                    Req'd  Req'd   Elap
        Job ID                         Username        Queue           Jobname         SessID   NDS  TSK   Memory Time  S Time
        ------------------------------ --------------- --------------- --------------- -------- ---- ----- ------ ----- - -----
        713849.brooklyn                username        v100            RG-V100            55922    1     4  300gb   --  R 00:54:32
        """
        lines = output.split("\n")
        rtn = []
        if len(lines) <= 1:
            return rtn
        for line in lines:
            if len(line) == 0 or not line[0].isdigit():
                continue
            items = re.split(r"\s+", line)
            job_id = items[0]
            user_name = items[1]
            queue = items[2]
            job_name = items[3]
            if len(items) == 11:
                status = items[-2]
                job_time = items[-1]
            else:
                status = items[-3]
                job_time = items[-2]
            rtn.append({
                "id": job_id,
                "user_name": user_name,
                "queue": queue,
                "job_name": job_name,
                "status": self.QSTAT_STATUS_MAP[status],
                "time": job_time
            })
        return rtn
