"""
class to manage path in different clusters
all arbitrary rules happen in this class.

model path, two types of models:
1. public: all users in this cluster can access
2. private: only this user can have access

job path:
job output dir is created under the same directory with a special name '.run'

"""
import os
import time
import json
import logging
from api.user import random_with_n_digits


CLUSTER_USER_PRIVATE_MODEL = "models/private"
JOB_META_FILE = ".echelon_job.toml"


def gen_list_model_cmd(model_path, user):
    """generate list model command"""
    model_roots = [model_path.gen_private_model_root(
        user), model_path.gen_public_model_root()]
    if not model_roots:
        return ""
    cmd = "mkdir -p"
    for model_dir in model_roots:
        cmd += " " + model_dir
    cmd += "; find "
    for model_dir in model_roots:
        cmd += " " + model_dir
    cmd += " -name *.DATA -type f"
    return cmd


def gen_run_job_cmd(model_path, job_json):
    """generate run job command"""
    return "python3 " + model_path.script_root_dir + "/run_echelon_job.py \'\"" + \
        json.dumps(job_json, separators=(',', ':')).replace(
            "\"", "\\\"") + "\"\'"


def gen_list_model_jobs_cmd(model_path, run_root_path):
    """generate command to list model jobs"""
    payload = {"root": run_root_path, "meta_file": JOB_META_FILE}
    # + "\""
    return "python3 " + model_path.script_root_dir + "/list_job_dirs.py \'\"" + \
        json.dumps(payload, separators=(',', ':')
                   ).replace("\"", "\\\"") + "\"\'"


def convert_timeout(minutes):
    """convert minutes to HH::MM::00"""
    if minutes > 60:
        return str(minutes // 60) + ":" + "{:02d}".format(minutes % 60) + ":00"
    return str(minutes) + ":00"


def elapsed_to_minutes(elapsed):
    """convert HH::MM::SS to minutes (round-up"""
    if elapsed.startswith("-"):
        return 0
    splits = elapsed.split(":")
    hours, minutes, seconds = int(splits[0]), int(splits[1]), int(splits[2])
    minutes += 60 * hours
    if seconds != 0:
        minutes += 1
    return minutes


def construct_job_json_for_cluster(cluster_name, model_path, user_name, job,
                                   call_back_endpoint, reserve_id, template_file, command, echelon_path):
    """construct the job structure to pass to the cluster"""
    run_time = {"user": user_name}
    model = job['model']
    run_time["run_dir"], run_time["uid"] = model_path.gen_job_run_dir(
        user_name, model['file'], model['path'], model['permission'])
    run_time['data_file'] = model_path.construct_path_from_model(
        user_name, model['file'], model['path'], model['permission'])
    run_time["meta_file"] = os.path.join(run_time["run_dir"], JOB_META_FILE)
    run_time['callback'] = call_back_endpoint
    run_time['call_back_code'] = random_with_n_digits(8)
    run_time['reserve_id'] = reserve_id
    run_time['template'] = template_file
    run_time['queue'] = job['queue']
    run_time['cluster'] = cluster_name
    run_time['ngpus'] = job['ngpus']
    run_time['binary'] = echelon_path
    run_time['zip_command_list'] = ['zip',  '-r', run_time["uid"] + "/" + run_time["uid"] + '.zip',
                                    run_time["uid"], '-x', 'node.txt', '-x', '*.sh', '-x', '*.toml', '-x',
                                    '*.INIT', '-x', '*.UNRST', '-x', '*.EGRID', '-x', 'nodes.txt']
    run_time['timeout'] = convert_timeout(job['timeout_min'])
    run_time['submit_command'] = command
    return {"run_time": run_time, "job": job}


class RemotePath:
    """Construct path given a model or recover a model from the path"""

    def __init__(self, cluster_root_dir):
        """
        cluster_root_dir is the network file system root for the clour portal: e.g. /efs or /data/user/cloud
        """
        self.cluster_root_dir = cluster_root_dir
        self.users_root_dir = os.path.join(cluster_root_dir, 'users')
        self.script_root_dir = os.path.join(cluster_root_dir, 'scripts')

    def gen_private_model_root(self, user_name):
        """
        generate the private model path
        """
        return os.path.join(self.get_user_root(user_name), CLUSTER_USER_PRIVATE_MODEL)

    def get_user_root(self, user_name):
        """get the root dir of the this user"""
        return os.path.join(self.users_root_dir, user_name)

    def gen_public_model_root(self):
        """
        generate the public model path
        """
        return os.path.join(self.cluster_root_dir, "public/models")

    def gen_model_run_root(self, user_name, file_name, path, model_permission):
        """generate the root of the run directory for a particular model"""
        data_path = self.construct_path_from_model(
            user_name, file_name, path, model_permission)
        if data_path is None:
            return None
        file_no_extension = os.path.splitext(file_name)[0]
        return os.path.join(os.path.dirname(data_path), ".run", file_no_extension, user_name)

    def gen_job_run_dir(self, user_name, file_name, path, model_permission, gen_uid=True):
        """generate the job run dir"""
        model_run_root = self.gen_model_run_root(
            user_name, file_name, path, model_permission)
        if model_run_root is None:
            return None
        uid = str(int(time.time())) if gen_uid else ""
        return os.path.join(model_run_root, uid), uid

    def data_file_to_model(self, ssh_stdout, user_name):
        """convert list of data file names to model names
        """
        models = ssh_stdout.strip('\n').split('\n')
        models = [self._gen_model_info(model, user_name) for model in models]
        return [model for model in models if model]  # remove the None's

    def _gen_model_info(self, model_path, user_name):
        """Tell the model permission depending on the path"""
        data_file = os.path.basename(model_path)
        dirname = os.path.dirname(model_path)
        if model_path.startswith(self.gen_public_model_root() + os.sep):
            dirname = dirname[len(self.gen_public_model_root() + os.sep):]
            return {"permission": 'public', "path": dirname, "file": data_file}
        if model_path.startswith(self.gen_private_model_root(user_name + os.sep)):
            dirname = dirname[len(
                self.gen_private_model_root(user_name) + os.sep):]
            return {"permission": 'private', "path": dirname, "file": data_file}
        logging.warning("unknown model path: %s for user %s",
                        model_path, user_name)
        return None

    def construct_path_from_model(self, user_name, file_name, path, model_permission):
        """The reverse of data_file_to_model"""
        if model_permission == "private":
            return os.path.join(self.gen_private_model_root(user_name), path, file_name)
        if model_permission == "public":
            return os.path.join(self.gen_public_model_root(), path, file_name)
        logging.warning("unknown model permission type %s", model_permission)
        return None
