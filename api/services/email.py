"""email service"""
import os
import smtplib
import ssl
import logging

SMTP_SERVER = "smtp.gmail.com"
PORT = 587  # For starttls


class Email:
    """Email client for """

    def __init__(self):
        """constructor"""
        self.admin_email = "echelon@stoneridgetechnology.com"
        self.password = os.environ.get('ECHELON_ADMIN_EMAIL_PASSWORD')
        if self.password is None:
            logging.warning(
                "please set the ECHELON_ADMIN_EMAIL_PASSWORD env for email service")

        # Try to log in to server
        try:
            context = ssl.create_default_context()
            server = smtplib.SMTP(SMTP_SERVER, PORT)
            server.ehlo()  # Can be omitted
            server.starttls(context=context)  # Secure the connection
            server.ehlo()  # Can be omitted
            server.login(self.admin_email, self.password)
            logging.info("successfully log into smtp service")
        except Exception as err:
            # Print any error messages to stdout
            logging.warning("error log into email service %s", err)
        finally:
            server.quit()

    def send(self, message):
        """send an email """
        try:
            context = ssl.create_default_context()
            message["From"] = self.admin_email
            server = smtplib.SMTP(SMTP_SERVER, PORT)
            server.ehlo()  # Can be omitted
            server.starttls(context=context)  # Secure the connection
            server.ehlo()  # Can be omitted
            server.login(self.admin_email, self.password)
            server.sendmail(self.admin_email,
                            message["To"], message.as_string())
        except Exception as exp:
            # Print any error messages to stdout
            logging.warning(
                "error login the email smtp service, email service is disabled: %s", exp)
        finally:
            server.quit()
