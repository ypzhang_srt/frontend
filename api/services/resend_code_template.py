"""email template for resending verification code"""
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


RESEND_CODE_TEMPLATE = {
    "text": """
Hi,

Your newest verification code is {0}, please login and enter this code to verify your email address.
""",
    "html": """
<html>
  <body>
    <p>Hi,<br>
       Your newest verification code is {0}, please login and enter this code to verify your email address. <br>
    </p>
  </body>
</html>
"""
}


def build_resend_message(receiver, vcode):
    """build message for resending verification code"""
    message = MIMEMultipart("alternative")
    message["Subject"] = "Your verification code to ECHELON cloud computing"
    message["To"] = receiver

    part1 = MIMEText(RESEND_CODE_TEMPLATE["text"].format(vcode), "plain")
    part2 = MIMEText(RESEND_CODE_TEMPLATE["html"].format(vcode), "html")

    message.attach(part1)
    message.attach(part2)
    return message
