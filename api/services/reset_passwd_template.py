"""email template for reset password """

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


RESET_PASSWD_TEMPLATE = {
    "text": """
Hi,

A password reset is received. Please use this link {0} to reset your password.
""",
    "html": """
<html>
  <body>
    <p>Hi,<br>
      A password reset is received. Please use this link {0} to reset your password.
    </p>
  </body>
</html>
"""
}


def build_reset_message(receiver, random_link):
    """build the reset password email"""
    message = MIMEMultipart("alternative")
    message["Subject"] = "Welcome to ECHELON cloud computing!"
    message["To"] = receiver

    part1 = MIMEText(
        RESET_PASSWD_TEMPLATE["text"].format(random_link), "plain")
    part2 = MIMEText(RESET_PASSWD_TEMPLATE["html"].format(random_link), "html")

    message.attach(part1)
    message.attach(part2)
    return message
