"""stripe related routines"""
import os
import logging
import json
import stripe
from api.services.services import Service

stripe.api_key = os.getenv('SRT_STRIPE_SECRET_KEY')
if stripe.api_key == "" or stripe.api_key is None:
    logging.error("please setup SRT_STRIPE_SECRET_KEY")
    exit(1)
elif stripe.api_key.startswith("sk_test"):
    logging.warning(
        "stripe in test mode, make sure this is not production run")


def webhook_received(request):
    """handle stripe webhook"""
    # You can use webhooks to receive information about asynchronous payment events.
    # For more about our webhook events check out https://stripe.com/docs/webhooks.
    webhook_secret = os.getenv('SRT_STRIPE_SECRET_WEBHOOK_KEY')
    request_data = json.loads(request.data)

    if webhook_secret:
        # Retrieve the event by verifying the signature using the raw body and secret if webhook signing is configured.
        signature = request.headers.get('stripe-signature')
        try:
            event = stripe.Webhook.construct_event(
                payload=request.data, sig_header=signature, secret=webhook_secret)
            data = event['data']
        except Exception as exp:
            logging.warning(
                "error construct stripe event in the webhook %s", str(exp))
            return None
        # Get the type of webhook event sent - used to check the status of PaymentIntents.
        event_type = event['type']
    else:
        data = request_data['data']
        event_type = request_data['type']
    data_object = data['object']

    # pylint:disable=pointless-string-statement
    """
    data_object is {'id': 'cs_live_a1ab3bKd5XtrTJL8uPqVJ7lROMifPw4LHNJqsQgtqrwFO55mS7kk27noL3', 'object': 'checkout.session',
    'allow_promotion_codes': None, 'amount_subtotal': 100, 'amount_total': 100, 'billing_address_collection': None, 'cancel_ur
    l': 'https://02b81f12ee0b.ngrok.io/c/dashboard', 'client_reference_id': 'ypzhang@stoneridgetechnology.com', 'currency': 'u
    sd', 'customer': 'cus_JNhbIovq8bahs5', 'customer_details': {'email': 'zhang.yongpeng@gmail.com', 'tax_exempt': 'none', 'ta
    x_ids': []}, 'customer_email': None, 'livemode': True, 'locale': None, 'metadata': {}, 'mode': 'payment', 'payment_intent'
    : 'pi_1IkwCFLBiyM2KNwGahiZIPDy', 'payment_method_options': {}, 'payment_method_types': ['card'], 'payment_status': 'paid',
    'setup_intent': None, 'shipping': None, 'shipping_address_collection': None, 'submit_type': None, 'subscription': None, '
    success_url': 'https://02b81f12ee0b.ngrok.io/c/dashboard', 'total_details': {'amount_discount': 0, 'amount_shipping': 0, '
    amount_tax': 0}}
    """

    # print("data_object is", data_object)
    # print("event type is", event_type)

    if event_type == 'checkout.session.completed':
        logging.info("payment %d received from user %s",
                     data_object['amount_total'], data_object['client_reference_id'])

        session = stripe.checkout.Session.retrieve(
            data_object["id"],
            expand=['line_items']
        )
        try:
            quantity = session["line_items"]["data"][0]["quantity"]
        except KeyError:
            logging.warning(
                "error getting the quantity for the checkout session")
            return None
        Service['db'].add_transaction_by_email(
            quantity * 100, data_object['client_reference_id'], "+")
        # TODO send email receipt
    else:
        logging.info("received stripe webhook event %s, ignored.", event_type)
    return {'status': 'success'}
    # return jsonify({'status': 'success'})
