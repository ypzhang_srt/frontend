"""email template for resetting password"""

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


WELCOME_TEMPLATE = {
    "text": """
Hi,

Welcome to echelon cloud computing!
Your verification code is {0}, please use it for the first login.
""",
    "html": """
<html>
  <body>
    <p>Hi,<br>
       Welcome to echelon cloud computing!<br>
       Your verification code is {0}, please use it for the first login. <br>
    </p>
  </body>
</html>
"""
}


def build_welcome_message(receiver, verify_code):
    message = MIMEMultipart("alternative")
    message["Subject"] = "Welcome to ECHELON cloud computing!"
    message["To"] = receiver

    part1 = MIMEText(WELCOME_TEMPLATE["text"].format(verify_code), "plain")
    part2 = MIMEText(WELCOME_TEMPLATE["html"].format(verify_code), "html")

    message.attach(part1)
    message.attach(part2)
    return message
