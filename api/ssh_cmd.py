"""ssh cmd"""
import logging
from subprocess import PIPE, run, CalledProcessError, CompletedProcess


class SSHCmd:
    """the ssh command class that handles the run"""

    def __init__(self, master_host, user="", pem_file=""):
        """constructoring the ssh command from the settings"""
        if user != "":
            self.ssh_prefix = "ssh " + user + "@" + master_host
        else:
            self.ssh_prefix = "ssh " + master_host

        if pem_file != "":
            self.ssh_prefix += " -i " + pem_file

    def run(self, *args):
        """run ssh command"""
        cmd = self.ssh_prefix
        for arg in args:
            cmd += " " + arg
        try:
            result = CompletedProcess([], "")
            result = run(
                cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True,
                check=False,
                shell=True)
            # if result.stderr != "":
            # logging.warning("ssh command %s returns stderr %s", cmd, result.stderr)
            return result
        except CalledProcessError as err:
            logging.warning("ssh command %s returns stderr %s, error: %s",
                            cmd, result.stderr, err)
            return result

    def dry_run(self, cmd="ls /"):
        """dry run for the first time"""
        return self.run(cmd)
