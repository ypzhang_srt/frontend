import pytest

from api.config import Config


def test_pcluster_info():
    config = Config()
    output, _ = config.clusters['hydra'].get_cluster()
    assert len(output) > 0
