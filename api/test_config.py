"""test config"""
import sys
import pytest

from api.config import Config, convert_to_hour_unit


def test_config():
    """simple test"""
    config = Config()
    models, msg = config.list_models('hydra', "test@web.com")
    assert msg == ""
    print(models)
    assert len(models) > 0


def test_reserve_hour():
    """simple test"""
    assert convert_to_hour_unit(28.1) == 50
    assert convert_to_hour_unit(27) == 45
    assert convert_to_hour_unit(21, 12) == 40
    print("here")


if __name__ == "__main__":
    pytest.main()
