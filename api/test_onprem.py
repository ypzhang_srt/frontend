"""
test on prem config
"""
import pytest

from api.config import Config


def test_onprem_info():
    """simple test"""
    config = Config()
    output, _ = config.clusters['SRT'].list_jobs()
    print(output)
    # assert len(output) > 0


if __name__ == "__main__":
    pytest.main()
