"""test cases for parallel cluster class"""
import pytest
import boto3
from botocore.exceptions import ClientError
from pcluster.utils import get_installed_version, paginate_boto3
from pcluster.constants import PCLUSTER_STACK_PREFIX


def test_pcluster_version():
    """get version"""
    print(get_installed_version())


def test_list():
    """test the list function"""
    try:
        result = []
        for stack in paginate_boto3(
                boto3.client("cloudformation").describe_stacks):
            if stack.get("ParentId") is None and stack.get(
                    "StackName").startswith(PCLUSTER_STACK_PREFIX):
                print(stack.get("StackName")[len(PCLUSTER_STACK_PREFIX):])
                # pcluster_version = _get_pcluster_version_from_stack(stack)
                result.append(
                    [
                        stack.get("StackName")[len(PCLUSTER_STACK_PREFIX):],  # noqa: E203
                        # _colorize(stack.get("StackStatus"), args),
                        # pcluster_version,
                    ]
                )
        # LOGGER.info(tabulate(result, tablefmt="plain"))
    except ClientError:
        # LOGGER.critical(e.response.get("Error").get("Message"))
        assert False


if __name__ == "__main__":
    pytest.main()
