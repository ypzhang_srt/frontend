"""testing model class"""
import pytest

from api.remote_path import (RemotePath, gen_list_model_cmd,
                             gen_run_job_cmd, convert_timeout, elapsed_to_minutes)


def test_model_path():
    """simple test"""
    model = RemotePath("/efs")
    assert model.get_user_root("user1") == "/efs/users/user1"
    assert model.users_root_dir == "/efs/users"
    assert model.gen_private_model_root(
        "user2") == "/efs/users/user2/models/private"
    assert model.gen_public_model_root() == "/efs/public/models"
    # pylint:disable = protected-access
    model1 = model._gen_model_info(
        "/efs/users/user2/models/private/model1/test.data", "user2")
    assert model1 == {"permission": "private",
                      "path": "model1", "file": "test.data"}

    assert model._gen_model_info(
        "/efs/users/user2/models/private/model1/test.data", "wrong_user") is None

    pub_model = model._gen_model_info(
        model.gen_public_model_root() + "/model_public/1/test2.data", "anyuser")
    assert pub_model == {'permission': 'public',
                         'path': 'model_public/1', 'file': 'test2.data'}

    pub_path = model.construct_path_from_model(
        "user_name", "test2.data", "tag", "public")
    assert pub_path == "/efs/public/models/tag/test2.data"

    priv_path = model.construct_path_from_model(
        "user_name", "test2.data", "tag/subtag", "private")
    assert priv_path == "/efs/users/user_name/models/private/tag/subtag/test2.data"


def test_gen_list_model_cmd():
    """test gen list model cmd"""
    model = RemotePath("/data/users")
    cmd = gen_list_model_cmd(model, "user1")
    assert cmd == "mkdir -p /data/users/users/user1/models/private /data/users/public/models; find  /data/users/users/user1/models/private /data/users/public/models -name *.DATA -type f"


def test_convert_timeout():
    """get convert timeout"""
    assert convert_timeout(30) == "30:00"
    assert convert_timeout(60) == "60:00"
    assert convert_timeout(61) == "1:01:00"


def test_gen_run_job_cmd():
    """test gen run job cmd"""
    model = RemotePath("/data/users")
    cmd = gen_run_job_cmd(model, {"user_name": "user1"})
    print(cmd)
    # assert cmd == 'python3 /data/users/scripts/run_echelon_job.py '"{\"user_name\":\"user1\"}"''


def test_elapsed_to_minutes():
    """test elapsed to minutes"""
    assert elapsed_to_minutes("00:30:00") == 30
    assert elapsed_to_minutes("00:30:45") == 31
    assert elapsed_to_minutes("02:30:45") == 151


if __name__ == "__main__":
    pytest.main()
