"""testing model class"""
import pytest

from api.ssh_cmd import SSHCmd


def test_ssh_cmd():
    """simple test"""
    localhost = SSHCmd("localhost")
    result = localhost.dry_run()
    assert result.stderr == ""
    assert result.stdout != ""


if __name__ == "__main__":
    pytest.main()
