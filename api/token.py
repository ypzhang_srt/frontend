"""
token module for checking and issuing JWT tokens
"""
from datetime import datetime, timedelta
import logging
import jwt


JWT_SECRET = "secret"
JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_SECONDS = 60 * 40  # 40 minuetes?


def issue(user_id, user_name):
    """
    issue a token with an expiry
    """
    payload = {
        'iss': "echelon",
        'id': user_id,
        'sub': user_name,
        'exp': datetime.utcnow() + timedelta(seconds=JWT_EXP_DELTA_SECONDS)
    }
    return jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM).decode()


def verify(token):
    """
    verify the token, returns None if token is invalid
    """
    try:
        payload = jwt.decode(token, JWT_SECRET)
        return payload['sub']
    except jwt.ExpiredSignatureError:
        logging.warning("received expired signature")
        return None
    except jwt.InvalidTokenError:
        logging.warning("received invalid token")
        return None

    return None
