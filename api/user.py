"""
authentication related
"""

from random import randint
import bcrypt
from api.database.sqlite_db import SqliteDB


def random_with_n_digits(num):
    range_start = 10**(num-1)
    range_end = (10**num)-1
    return randint(range_start, range_end)


def init_database(config_str):
    """
    init user table by config string, supports:
    * sqlite:file_name
    """
    db_config = config_str.split(':')
    if db_config[0] == "sqlite":
        file_name = db_config[1]
        return SqliteDB(file_name)
    return None


def get_hashed_password(plain_text_password):
    # Hash a password for the first time
    #   (Using bcrypt, the salt is saved into the hash itself)
    return bcrypt.hashpw(plain_text_password, bcrypt.gensalt())


def check_password(plain_text_password, hashed_password):
    # Check hashed password. Using bcrypt, the salt is saved into the hash itself
    return bcrypt.checkpw(plain_text_password, hashed_password)
