## Design Notes

Start with sqlite3 due to simplicity.

### Users

* Each user has a unique email address to register. 
* User needs to verify the email address (with the verification code sent in the email) after the first login. An non-verified user has no permission to list or view clusters.
* Two types of users: 
    * Individual user: the default type. 
    * Group user: when he creates or joins a group.
* An individual user can list and use our public clusters.
* The owner of a group can invite another user (by email) to join. The invited user does not need to register beforehand.
* The group owner has administration permission to manage the group:
    * create or destroy a cluster
    * invite or remove a user
    * promote or demote a user to/from admin role
* Once becoming a group user, he can only access the dedicated cluster to this group.
* A user can be associated with only one group.
* The owner of a group can create and modify a cluster

### Clusters

* A cluster is either a pre-existing public cluster or a private cluster created by a group
* An individual user can only view the public cluster
* A group user can only view the private cluster created by this group.
    * We should limit the number of dedicated clusters. Start with one? 

### Billings

* The basic unit of billing is GPU hour (two decimals)
* New user gets 1? free GPU hour upon registration? 
* Each individual user has his own account
* Group users share the same group account
    * We will track each group member's usage
* A reserved amount is temporarily calculated for each job submitted based on the expected wall time. A job is rejected if the reserved amount exceeds the (current balance - already reserved)
* The real cost is deducted when job is finished and the reserved amount is cleared.
* Should we charge monthly fee for a group? 


### Databases

#### User 

tables: users, groups

```
CREATE TABLE IF NOT EXISTS "users"
(
    [UserId] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    [Email] NVARCHAR(80) UNIQUE NOT NULL,
    [UserName] NVARCHAR(30),
    [InvitedBy] INTEGER,
    [OrganizationId] INTEGER
    [RegDate] DATETIME,
    [GroupId]  INTEGER,
    [Verified] INTEGER,
    [Salt] NVARCHAR(80)
    FOREIGN KEY ([InvitedBy]) REFERENCES "users" ([UserID])
	    ON DELETE NO ACTION ON UPDATE NO ACTION
    FOREIGN KEY ([GroupId]) REFERENCES "groups" ([GroupID])
	    ON DELETE NO ACTION ON UPDATE NO ACTION 
)
CREATE INDEX [IFK_UserInvitedBy] ON "employees" ([ReportsTo]);

CREATE TABLE IF NOT EXISTS "groups"
(
  [GroupId] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  [Name] NVARCHAR(30),
  [CreatedBy] INTEGER,
  [CreateDate] DATETIME,
  FOREIGN KEY ([CreatedBy]) REFERENCES "users" ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION
)

(
    [ReportsTo] INTEGER,
    [BirthDate] DATETIME,
    [HireDate] DATETIME,
    [Address] NVARCHAR(70),
    [City] NVARCHAR(40),
    [State] NVARCHAR(40),
    [Country] NVARCHAR(40),
    [PostalCode] NVARCHAR(10),
    [Phone] NVARCHAR(24),
    [Fax] NVARCHAR(24),
    [Email] NVARCHAR(60),
    FOREIGN KEY ([ReportsTo]) REFERENCES "employees" ([EmployeeId]) 
		ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE INDEX [IFK_EmployeeReportsTo] ON "employees" ([ReportsTo]);

```