import React, { memo, useCallback, useState, useRef, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core";
import Routing from "./Routing";
import axios from 'axios';
import NavBar from "./navigation/NavBar";
import ConsecutiveSnackbarMessages from "../../shared/components/ConsecutiveSnackbarMessages";
import smoothScrollTop from "../../shared/functions/smoothScrollTop";
// import LazyLoadAddBalanceDialog from "./subscription/LazyLoadAddBalanceDialog";
import LazyLoadAddJobDialog from "./jobs/LazyLoadAddJobDialog";
import LazyLoadAddGPUHourDialog from "./subscription/LazyLoadAddGPUHourDialog";
import getTokenHeaders from "./Token"
import LazyLoadDeleteModelDialog from "./models/LazyLoadDeleteModelDIalog";

const styles = (theme) => ({
  main: {
    marginLeft: theme.spacing(9),
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    [theme.breakpoints.down("xs")]: {
      marginLeft: 0,
    },
  },
});

function Main(props) {
  const { classes } = props;
  const isMountedRef = useRef(null);
  const [selectedTab, setSelectedTab] = useState(null);
  const [CardChart, setCardChart] = useState(null);
  const [hasFetchedCardChart, setHasFetchedCardChart] = useState(false);
  const [EmojiTextArea, setEmojiTextArea] = useState(null);
  const [hasFetchedEmojiTextArea, setHasFetchedEmojiTextArea] = useState(false);
  const [ImageCropper, setImageCropper] = useState(null);
  const [hasFetchedImageCropper, setHasFetchedImageCropper] = useState(false);
  const [Dropzone, setDropzone] = useState(null);
  const [hasFetchedDropzone, setHasFetchedDropzone] = useState(false);
  const [DateTimePicker, setDateTimePicker] = useState(null);
  const [hasFetchedDateTimePicker, setHasFetchedDateTimePicker] = useState(
    false
  );
  const [targets, setTargets] = useState([]);
  const [isAccountActivated, setIsAccountActivated] = useState(false);
  // const [isAddBalanceDialogOpen, setIsAddBalanceDialogOpen] = useState(false);
  const [isAddGPUHourDialogOpen, setIsAddGPUHourDialogOpen] = useState(false);
  const [isAddJobDialogOpen, setIsAddJobDialogOpen] = useState(false);
  const [isDeleteModelDialogOpen, setIsDeleteModelDialogOpen] = useState(false);

  const [pushMessageToSnackbar, setPushMessageToSnackbar] = useState(null);
  const [clusters, setClusters] = useState([]);
  const [curCluster, setCurCluster] = useState(null)
  // const [curClusterInfo, setCurClusterInfo] = useState({});
  const [model, setModel] = useState(null);
  const [models, setModels] = useState([]);

  const onSelectCluster = useCallback((cluster_name) => {
    for (const cluster_id in clusters) {
      if (clusters[cluster_id]["name"] === cluster_name) {
        setCurCluster(clusters[cluster_id])
      }
    }
    // setCurClusterInfo(getCurCluster(cluster, clusters))
  }, [setCurCluster, clusters]);

  const onSetModels = useCallback((models) => {
    setModels(models)
  }, [setModels]);

  const openAddJobDialog = useCallback((model) => {
    setModel(model);
    setIsAddJobDialogOpen(true);
  }, [setModel, setIsAddJobDialogOpen]);


  const closeAddJobDialog = useCallback(() => {
    setIsAddJobDialogOpen(false);
  }, [setIsAddJobDialogOpen]);

  const openDeleteModelDialog = useCallback((model) => {
    setModel(model);
    setIsDeleteModelDialogOpen(true);
  }, [setModel, setIsDeleteModelDialogOpen]);

  const closeDeleteModelDialog = useCallback(() => {
    setIsDeleteModelDialogOpen(false);
  }, [setIsDeleteModelDialogOpen]);

  const openAddGPUHourDialog = useCallback(() => {
    if (isAccountActivated) {
      setIsAddGPUHourDialogOpen(true);
    }
  }, [setIsAddGPUHourDialogOpen, isAccountActivated]);

  const closeAddGPUHourDialog = useCallback(() => {
    setIsAddGPUHourDialogOpen(false);
  }, [setIsAddGPUHourDialogOpen]);

  /*
  const openAddBalanceDialog = useCallback(() => {
    if (isAccountActivated) {
      setIsAddBalanceDialogOpen(true);
    }
  }, [setIsAddBalanceDialogOpen, isAccountActivated]);

  const closeAddBalanceDialog = useCallback(() => {
    setIsAddBalanceDialogOpen(false);
  }, [setIsAddBalanceDialogOpen]);
  */


  const onPaymentSuccess = useCallback(() => {
    pushMessageToSnackbar({
      text: "Your balance has been updated.",
    });
    setIsAddGPUHourDialogOpen(false);
  }, [pushMessageToSnackbar, setIsAddGPUHourDialogOpen]);

  const onJobSubmitSuccess = useCallback(() => {
    pushMessageToSnackbar({
      isErrorMessage: false,
      text: "Job successfully submitted.",
    });
    setIsAddJobDialogOpen(false);
  }, [pushMessageToSnackbar, setIsAddJobDialogOpen]);

  const onJobSubmitFail = useCallback((msg) => {
    pushMessageToSnackbar({
      isErrorMessage: true,
      text: "Job submission failed: " + msg,
    });
  }, [pushMessageToSnackbar]);


  const onDeleteModelSubmitSuccess = useCallback(() => {
    pushMessageToSnackbar({
      isErrorMessage: false,
      text: "Model successfully deleted",
    });
    setIsDeleteModelDialogOpen(false);
  }, [pushMessageToSnackbar, setIsDeleteModelDialogOpen]);

  const onDeleteModelSubmitFail = useCallback((msg) => {
    pushMessageToSnackbar({
      isErrorMessage: true,
      text: "Model delete failed: " + msg,
    });
    setIsDeleteModelDialogOpen(false);
  }, [pushMessageToSnackbar, setIsDeleteModelDialogOpen]);


  const fetchClusters = useCallback(() => {
    const token = getTokenHeaders()
    if (token !== null && token !== "0" ) {
      axios.get('/api/cluster', {headers: token,
      })
        .then(res => {
          setIsAccountActivated(true)
          setClusters(res.data)
          if (curCluster === null && res.data.length > 0) {
            setCurCluster(res.data[0])
            // setCurClusterInfo(getCurCluster(res.data[0].name, clusters))
          }
        })
        .catch(error => {
          setTimeout(() => {
          }, 1500);
        })
      }
  }, [setClusters, setCurCluster, curCluster]);

  const toggleAccountActivation = useCallback(() => {
    if (pushMessageToSnackbar) {
      if (isAccountActivated) {
        pushMessageToSnackbar({
          text: "Your account is now deactivated.",
        });
      } else {
        pushMessageToSnackbar({
          text: "Your account is now activated.",
        });
      }
    }
    setIsAccountActivated(!isAccountActivated);
  }, [pushMessageToSnackbar, isAccountActivated, setIsAccountActivated]);

  const selectDashboard = useCallback(() => {
    smoothScrollTop();
    document.title = "ECHELON - Dashboard";
    setSelectedTab("Dashboard");
    if (!hasFetchedCardChart) {
      setHasFetchedCardChart(true);
      import("../../shared/components/CardChart").then((Component) => {
        setCardChart(Component.default);
      });
    }
  }, [
    setSelectedTab,
    setCardChart,
    hasFetchedCardChart,
    setHasFetchedCardChart,
  ]);

  const selectSubscription = useCallback(() => {
    smoothScrollTop();
    document.title = "ECHELON - Jobs";
    setSelectedTab("Jobs");
  }, [setSelectedTab]);

  const selectJob = useCallback(() => {
    smoothScrollTop();
    document.title = "ECHELON - Jobs";
    setSelectedTab("Jobs");
  }, [setSelectedTab]);


  const selectTransaction = useCallback(() => {
    smoothScrollTop();
    document.title = "ECHELON - Transactions";
    setSelectedTab("Transactions");
  }, [setSelectedTab]);


  const selectModel = useCallback(() => {
    smoothScrollTop();
    document.title = "ECHELON - Models";
    setSelectedTab("Models");
    if (!hasFetchedEmojiTextArea) {
      setHasFetchedEmojiTextArea(true);
      import("../../shared/components/EmojiTextArea").then((Component) => {
        setEmojiTextArea(Component.default);
      });
    }
    if (!hasFetchedImageCropper) {
      setHasFetchedImageCropper(true);
      import("../../shared/components/ImageCropper").then((Component) => {
        setImageCropper(Component.default);
      });
    }
    if (!hasFetchedDropzone) {
      setHasFetchedDropzone(true);
      import("../../shared/components/Dropzone").then((Component) => {
        setDropzone(Component.default);
      });
    }
    if (!hasFetchedDateTimePicker) {
      setHasFetchedDateTimePicker(true);
      import("../../shared/components/DateTimePicker").then((Component) => {
        setDateTimePicker(Component.default);
      });
    }
  }, [setSelectedTab,
    setEmojiTextArea,
    setImageCropper,
    setDropzone,
    setDateTimePicker,
    hasFetchedEmojiTextArea,
    setHasFetchedEmojiTextArea,
    hasFetchedImageCropper,
    setHasFetchedImageCropper,
    hasFetchedDropzone,
    setHasFetchedDropzone,
    hasFetchedDateTimePicker,
    setHasFetchedDateTimePicker]);

  const getPushMessageFromChild = useCallback(
    (pushMessage) => {
      setPushMessageToSnackbar(() => pushMessage);
    },
    [setPushMessageToSnackbar]
  );

  useEffect(() => {
    isMountedRef.current = true;
    fetchClusters();


    return () => {
      isMountedRef.current = false;
    }
  }, [
    fetchClusters,
  ]);

  return (
    <Fragment>
      <LazyLoadAddGPUHourDialog
        open={isAddGPUHourDialogOpen}
        onClose={closeAddGPUHourDialog}
        onSuccess={onPaymentSuccess}
      />
      <LazyLoadAddJobDialog
        open={isAddJobDialogOpen}
        curCluster={curCluster}
        onClose={closeAddJobDialog}
        onSuccess={onJobSubmitSuccess}
        onError={onJobSubmitFail}
        model={model}
      />
      <LazyLoadDeleteModelDialog
      open={isDeleteModelDialogOpen}
      curCluster={curCluster}
      onClose={closeDeleteModelDialog}
      onSuccess={onDeleteModelSubmitSuccess}
      onError={onDeleteModelSubmitFail}
      onSetModels={onSetModels}
      model={model}
    />
      <NavBar
        selectedTab={selectedTab}
        clusters={clusters}
        messages={[]}
        curCluster={curCluster}
        onSelectCluster={onSelectCluster}
        openAddGPUHourDialog={openAddGPUHourDialog}
      />
      <ConsecutiveSnackbarMessages
        getPushMessageFromChild={getPushMessageFromChild}
      />
      <main className={classNames(classes.main)}>
        <Routing
          isAccountActivated={isAccountActivated}
          ImageCropper={ImageCropper}
          EmojiTextArea={EmojiTextArea}
          CardChart={CardChart}
          Dropzone={Dropzone}
          DateTimePicker={DateTimePicker}
          toggleAccountActivation={toggleAccountActivation}
          pushMessageToSnackbar={pushMessageToSnackbar}
          targets={targets}
          models={models}
          onSetModels={onSetModels}
          curCluster={curCluster}
          clusters={clusters}
          selectDashboard={selectDashboard}
          selectSubscription={selectSubscription}
          selectJob={selectJob}
          selectTransaction={selectTransaction}
          selectModel={selectModel}
          openAddGPUHourDialog={openAddGPUHourDialog}
          openAddJobDialog={openAddJobDialog}
          openDeleteModelDialog={openDeleteModelDialog}
          setTargets={setTargets}
        />
      </main>
    </Fragment>
  );
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(memo(Main));
