import React, { memo } from "react";
import PropTypes from "prop-types";
import { Switch } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import Dashboard from "./dashboard/Dashboard";
// import Subscription from "./subscription/Subscription";
import Job from "./jobs/Job";
import Transaction from "./transactions/Transaction";
import Model from "./models/Model";
import PropsRoute from "../../shared/components/PropsRoute";

const styles = (theme) => ({
  wrapper: {
    margin: theme.spacing(1),
    width: "auto",
    [theme.breakpoints.up("xs")]: {
      width: "95%",
      marginLeft: "auto",
      marginRight: "auto",
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(4),
    },
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      width: "90%",
      marginLeft: "auto",
      marginRight: "auto",
    },
    [theme.breakpoints.up("md")]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      width: "82.5%",
      marginLeft: "auto",
      marginRight: "auto",
    },
    [theme.breakpoints.up("lg")]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      width: "70%",
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
});

function Routing(props) {
  const {
    classes,
    EmojiTextArea,
    ImageCropper,
    Dropzone,
    DateTimePicker,
    pushMessageToSnackbar,
    models,
    onSetModels,
    toggleAccountActivation,
    clusters,
    curCluster,
    targets,
    setTargets,
    isAccountActivated,
    selectDashboard,
    selectJob,
    selectTransaction,
    selectModel,
    openAddJobDialog,
    openDeleteModelDialog,
  } = props;
  return (
    <div className={classes.wrapper}>
      <Switch>
        <PropsRoute
          path="/c/models"
          component={Model}
          EmojiTextArea={EmojiTextArea}
          ImageCropper={ImageCropper}
          Dropzone={Dropzone}
          curCluster={curCluster}
          DateTimePicker={DateTimePicker}
          pushMessageToSnackbar={pushMessageToSnackbar}
          openAddJobDialog={openAddJobDialog}
          openDeleteModelDialog={openDeleteModelDialog}
          models={models}
          onSetModels={onSetModels}
          selectModel={selectModel}
        />
        <PropsRoute
          path="/c/jobs"
          component={Job}
          pushMessageToSnackbar={pushMessageToSnackbar}
          selectJob={selectJob}
          curCluster={curCluster}
          openAddJobDialog={openAddJobDialog}
        />
        <PropsRoute
          path="/c/transactions"
          component={Transaction}
          selectTransaction={selectTransaction}
          pushMessageToSnackbar={pushMessageToSnackbar}
        />


        <PropsRoute
          path=""
          component={Dashboard}
          toggleAccountActivation={toggleAccountActivation}
          pushMessageToSnackbar={pushMessageToSnackbar}
          clusters={clusters}
          curCluster={curCluster}
          targets={targets}
          setTargets={setTargets}
          isAccountActivated={isAccountActivated}
          selectDashboard={selectDashboard}
        />
      </Switch>
    </div>
  );
}

Routing.propTypes = {
  classes: PropTypes.object.isRequired,
  EmojiTextArea: PropTypes.elementType,
  ImageCropper: PropTypes.elementType,
  Dropzone: PropTypes.elementType,
  DateTimePicker: PropTypes.elementType,
  pushMessageToSnackbar: PropTypes.func,
  setTargets: PropTypes.func.isRequired,
  onSetModels: PropTypes.func.isRequired,
  // posts: PropTypes.arrayOf(PropTypes.object).isRequired,
  toggleAccountActivation: PropTypes.func,
  // clusters: PropTypes.object.isRequired,
  targets: PropTypes.arrayOf(PropTypes.object).isRequired,
  isAccountActivated: PropTypes.bool.isRequired,
  selectDashboard: PropTypes.func.isRequired,
  selectSubscription: PropTypes.func.isRequired,
  selectJob: PropTypes.func.isRequired,
  selectTransaction: PropTypes.func.isRequired,
  selectModel: PropTypes.func.isRequired,
  // openAddBalanceDialog: PropTypes.func.isRequired,
  openAddGPUHourDialog: PropTypes.func.isRequired,
  openAddJobDialog: PropTypes.func.isRequired,
  openDeleteModelDialog: PropTypes.func.isRequired,
};

export default withStyles(styles, { withTheme: true })(memo(Routing));
