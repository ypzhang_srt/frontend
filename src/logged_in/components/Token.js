function getTokenHeaders() {
    try {
      const token = localStorage.getItem('token');
      if (token !== 'undefined' && token) {
        return {
          "Authorization": `Bearer ${token}`
        }
      }
      return null
    } catch (err) {
      return null
    }
  }

export default getTokenHeaders;