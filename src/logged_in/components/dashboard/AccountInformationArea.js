import React, {useCallback, useEffect, useState} from "react";
import PropTypes from "prop-types";
import axios from 'axios';
import {
  Button,
} from "@material-ui/core";
import TextField from '@material-ui/core/TextField';
import {
  Paper,
  Toolbar,
  ListItemText,
  Box,
  withStyles
} from "@material-ui/core";

const styles = theme => ({
  paper: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  toolbar: { justifyContent: "space-between" },
  scaleMinus: {
    transform: "scaleX(-1)"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '25ch',
  },
  "@keyframes spin": {
    from: { transform: "rotate(359deg)" },
    to: { transform: "rotate(0deg)" }
  },
  spin: { animation: "$spin 2s infinite linear" },
  listItemSecondaryAction: { paddingRight: theme.spacing(1) }
});

function getUser() {
  try {
    return localStorage.getItem('user');
  } catch (err) {
    return ""
  }
}

function AccountInformationArea(props) {
  // eslint-disable-next-line
  const { classes, toggleAccountActivation, isAccountActivated, pushMessageToSnackbar } = props;
  const [disableResend, setDisableResend] = useState(false);
  const [countDown, setCountDown] = useState(0)
  const [verifyCountDown, setVerifyCountDown] = useState(0)
  const [disableVerify, setDisableVerify] = useState(false);
  const [vcode, setVCode] = useState("")

  const handleClick = useCallback(
    (event) => {
      axios.get('/api/auth/verify',  { params: { user: getUser(), code: vcode} })
      .then(res => {
        if (res.data.token !== "undefined") {
          localStorage.setItem('token', res.data.token)
          toggleAccountActivation()
          setDisableVerify(false)
        }
      })
      .catch(error => {
        pushMessageToSnackbar({
          isErrorMessage: true,
          text: "verification code was incorrect",
        });
        setTimeout(() => {
        }, 1500);
      })
      setDisableVerify(true)
      setVerifyCountDown(15)
    },
    [setDisableVerify, setVerifyCountDown, vcode, toggleAccountActivation, pushMessageToSnackbar]
  );

  const handleResend = useCallback(
    (event) => {
      axios.get('/api/auth/resend',  { params: { user: getUser() } })
      .then(res => {
        setDisableResend(true)
        setCountDown(30)
      })
      .catch(error => {
        setTimeout(() => {
        }, 1500);
      })
    },
    [setDisableResend, setCountDown]
  );

  const resend_button = disableResend? countDown : "Resend Code"
  const verify_button = disableVerify? verifyCountDown : "Verify"

  useEffect(() => {
    const interval = setInterval(() => {
      if (countDown > 1) {
        setCountDown(countDown - 1)
      } else {
        setDisableResend(false)
      }
      if (verifyCountDown > 1) {
        setVerifyCountDown(verifyCountDown - 1)
      } else {
        setDisableVerify(false)
      }
    }, 1000);
    return () => clearInterval(interval);
  }, [countDown, setCountDown, setDisableResend, verifyCountDown, setVerifyCountDown, setDisableVerify]);


  return (
    <Paper className={classes.paper}>
      <Toolbar className={classes.toolbar}>
        <Box display="flex" alignItems="center">
          <Box mr={2}>
            <ListItemText
              primary="Status"
              secondary={isAccountActivated ? "Activated" : "Not activated"}
              className="mr-2"
            />
          </Box>
          <Button
          variant="contained"
          color="secondary"
          disabled={disableResend}
          onClick={handleResend}
          >
            { resend_button }
        </Button>
        </Box>
        <Box display="flex" alignItems="center">
          <Box mr={2}>
            <TextField
              id="outlined-margin-none"
              placeholder="enter verification code"
              className={classes.textField}
              variant="outlined"
              onChange={event => {
                setVCode(event.target.value);
              }}
            />
          </Box>
          <Button
          variant="contained"
          color="secondary"
          onClick={handleClick}
          disabled={disableVerify}
          >
            {verify_button}
        </Button>
        </Box>
      </Toolbar>
    </Paper>
  );
}

AccountInformationArea.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  toggleAccountActivation: PropTypes.func.isRequired,
  // pushMessageToSnackbar: PropTypes.func.isRequired,
  isAccountActivated: PropTypes.bool.isRequired

};

export default withStyles(styles, { withTheme: true })(AccountInformationArea);
