import React, {useCallback, useState, useEffect} from 'react';
import { PieChart, Pie, Cell } from 'recharts';
// import PropTypes from "prop-types";
import axios from "axios";
import getTokenHeaders from "../Token"


function ClusterChart(props) {
    const {
        curCluster,
    } = props;

    const [clusterInfo, setClusterInfo] = useState([]);

    const fetchClusterInfo = useCallback(() => {
      if (curCluster === null) {
        return Promise.resolve();
      }

        const token = getTokenHeaders()
        const jsonPayload =  {"cluster": curCluster.name}
        axios.post('/api/cluster', jsonPayload, {headers: token,
        })
          .then(res => {
            setClusterInfo(res.data)
            console.log("clusterInfo is ", res.data)
          })
          .catch(error => {        
            setTimeout(() => {
            }, 1500);
          })

      
    }, [setClusterInfo, curCluster]);
   
    useEffect(() => {
      fetchClusterInfo()
    }, [
      fetchClusterInfo]);

    const data = [
      { name: 'Group A', value: 400 },
      { name: 'Group B', value: 300 },
      { name: 'Group C', value: 300 },
      { name: 'Group D', value: 200 },
    ];
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

    const RADIAN = Math.PI / 180;

    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
      const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
      const x = cx + radius * Math.cos(-midAngle * RADIAN);
      const y = cy + radius * Math.sin(-midAngle * RADIAN);

      return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
          {`${(percent * 100).toFixed(0)}%`}
        </text>
      );
    };

    const width = 200 * clusterInfo.length
    return (
      <div width={width} height={300}>
        <PieChart width={width} height={200}>
         {clusterInfo.map((queue, index, array) => { 
          const half_div = (100.0) / (2 * array.length)
          const cx = parseFloat(half_div * (2 * index + 1)).toFixed(2) + "%"
           return (        
          <Pie
            data={data}
            cx={cx}
            cy="40%"
            labelLine={false}
            label={renderCustomizedLabel}
            outerRadius={80}
            fill="#8884d8"
            dataKey="value"
            key={index}
          >
            {data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            ))}
          </Pie> 
 )}
          )}
        </PieChart>
        </div>
    );
  }


  ClusterChart.propTypes = {
    // curCluster: PropTypes.object.isRequired,
  };
  
  export default ClusterChart;