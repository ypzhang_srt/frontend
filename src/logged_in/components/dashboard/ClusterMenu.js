// import React, { Fragment } from "react";
import React, { useState, useCallback} from "react";
import {
    Grid,
    // TablePagination,
    // Divider,
    // Toolbar,
    Card,
    Typography,
    // Button,
    // Paper,
    Box,
    // withStyles,
  } from "@material-ui/core";
import HighlightedInformation from "../../../shared/components/HighlightedInformation";  
// import DeleteIcon from "@material-ui/icons/Delete";
// import SelfAligningImage from "../../../shared/components/SelfAligningImage";
// import PropTypes from "prop-types";

const rowsPerPage = 5;

function ClusterMenu(props) {
  const { clusters } = props;
  const [page, setPage] = useState(0);
  // eslint-disable-next-line
  const handleChangePage = useCallback(
    (__, page) => {
      setPage(page);
    },
    [setPage]
  );
  
  if (clusters.length > 0) {
    return (
        <Box p={1}>
            <Grid container spacing={1}>
            {clusters
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((cluster) => (
                    <Card key={cluster.name}>
                        <Box pt={3} pr={3} pl={3} pb={2}>
                            <Typography variant="subtitle1">
                        <b>{cluster.name}</b>
                        </Typography>
                        <Typography variant="body1" color="textSecondary">
                            {cluster.vendor}
                        </Typography>
                        <Typography variant="body1" color="textSecondary">
                            {cluster.region}
                        </Typography>
                        <Typography variant="body1" color="textSecondary">
                            {cluster.type}
                        </Typography>

                    </Box>        
                    </Card>
                ))}
            </Grid>
        </Box>
    );
  }
  return (
  <Box m={2}>
  <HighlightedInformation>
    No clusters found.
  </HighlightedInformation>
  </Box>
  )
}

ClusterMenu.propTypes = {
  // pushMessageToSnackbar: PropTypes.func
};

export default ClusterMenu;
