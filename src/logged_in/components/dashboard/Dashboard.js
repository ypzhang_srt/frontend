import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { Typography, Box } from "@material-ui/core";
import SettingsArea from "./SettingsArea";
import UserDataArea from "./UserDataArea";
import ClusterMenu from "./ClusterMenu";
import ClusterChart from "./ClusterChart";
import AccountInformationArea from "./AccountInformationArea";

function Dashboard(props) {
  const {
    selectDashboard,
    clusters,
    curCluster,
    toggleAccountActivation,
    pushMessageToSnackbar,
    targets,
    setTargets,
    isAccountActivated,
  } = props;

  useEffect(selectDashboard, [selectDashboard]);
  // onChangeValue={this.handleChangeValue}
  const renderActivate = () => {
    if (isAccountActivated) {
      return
    }
    return (
      <div>
      <Box mt={4}>
        <Typography variant="subtitle1" gutterBottom>
          Your Account
        </Typography>
      </Box>
      <AccountInformationArea
        isAccountActivated={isAccountActivated}
        toggleAccountActivation={toggleAccountActivation}
        pushMessageToSnackbar={pushMessageToSnackbar}
      />
      </div>
      )
    }

    const renderSettings = () => {
      if (isAccountActivated) {
        return (<div>
        <Box mt={4}>
          <Typography variant="subtitle1" gutterBottom>
            Settings
          </Typography>
        </Box>
        <SettingsArea pushMessageToSnackbar={pushMessageToSnackbar} />
        <UserDataArea
          pushMessageToSnackbar={pushMessageToSnackbar}
          targets={targets}
          setTargets={setTargets}
        />
        </div>)
      }
    }

  return (
    <Fragment>
      <Box mt={4}>
        <Typography variant="subtitle1" gutterBottom>
          Available Clusters
        </Typography>
      </Box>
      <ClusterMenu clusters={clusters}
      /* onChangeDefaultCluster=this.handleChangeDefault*/
      >
      </ClusterMenu>

      <Box mt={4}>
        <Typography variant="subtitle1" gutterBottom>
          Cluster {curCluster === null? "": (curCluster.name)} Status:
        </Typography>
      </Box>
      <ClusterChart curCluster={curCluster} />

      { renderActivate() }
      { renderSettings() }
    </Fragment>
  );
}

Dashboard.propTypes = {
  toggleAccountActivation: PropTypes.func,
  pushMessageToSnackbar: PropTypes.func,
  targets: PropTypes.arrayOf(PropTypes.object).isRequired,
  setTargets: PropTypes.func.isRequired,
  // curCluster: PropTypes.object.isRequired,
  isAccountActivated: PropTypes.bool.isRequired,
  selectDashboard: PropTypes.func.isRequired,
};

export default Dashboard;
