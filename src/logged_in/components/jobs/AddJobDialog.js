import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import { Button, Box, withTheme } from "@material-ui/core";
import NewJobForm from "./NewJobForm";
// import StripeCardForm from "../subscription/stripe/StripeCardForm";
import FormDialog from "../../../shared/components/FormDialog";
// import ColoredButton from "../../../shared/components/ColoredButton";
// import HighlightedInformation from "../../../shared/components/HighlightedInformation";
import ButtonCircularProgress from "../../../shared/components/ButtonCircularProgress";
import getTokenHeaders from "../Token"
function getUser() {
  try {
    return localStorage.getItem('user');
  } catch (err) {
    return ""
  }
}

const AddJobDialog = withTheme(function (props) {
  const { open, onClose, onSuccess, curCluster, model } = props;

  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [gpu, setGpu] = useState(1);
  const [gpuError, setgpuError] = useState("");
  const [version, setVersion] = useState("")
  const [queue, setQueue] = useState("")

   const ongpuChange = gpu => {
    if (gpu < 0) {
      return;
    }
    if (gpuError) {
      setgpuError("");
    }
    setGpu(gpu);
  };

  const onVersionChange = event => {
    setVersion(event.target.value)
  };

  const onQueueChange = event => {
    setQueue(event.target.value)
  };

  const renderJobComponent = () => {
        return (
          <Fragment>
            <Box mb={2}>
              <NewJobForm
                curCluster={curCluster}
                setName={setName}
                name={name}
                gpu={gpu}
                version={version}
                queue={queue}
                gpuError={gpuError}
                onGpuChange={ongpuChange}
                onVersionChange={onVersionChange}
                onQueueChange={onQueueChange}
              />
            </Box>
          </Fragment>
        );
  };

  useEffect(() => {
    const version = (curCluster === null)? "" : curCluster["versions"][0]
    const queue = (curCluster === null)? "" : curCluster["queues"][0]
    setVersion(version)
    setQueue(queue)
  }, [setVersion, setQueue, curCluster])

  return (
    <FormDialog
      open={open}
      onClose={onClose}
      headline="Run Model in ECHELON"
      hideBackdrop={false}
      loading={loading}
      onFormSubmit={async event => {
        event.preventDefault();
        setLoading(true);
        const token = getTokenHeaders()
        const user = getUser()
        // TODO cluster name
        const jsonPayload =  {"cluster": curCluster.name,
        "user": user,
        "group": "", // use current group
        "job": {
          "model": model,
          "queue": queue,
          "ngpus": gpu,
          "version": version,
          "timeout_min": 30,  // remove the hardcoded number
        },
        }
        axios.post('/api/job', jsonPayload, {headers: token})
          .then(res => {
            console.log("submit job succeeded", res.data)
            setLoading(false);
            onSuccess();
          // setModels(res.data)
        })
        .catch(error => {
          console.log("submit job error", error)
          setTimeout(() => {
          }, 1500);
          setLoading(false);
          onClose();
          // onFailure();
        })
      }}
      content={
        <Box pb={2}>
          {renderJobComponent()}
        </Box>
      }
      actions={
        <Fragment>
          <Button
            fullWidth
            variant="contained"
            color="secondary"
            type="submit"
            size="large"
            disabled={loading}
          >
            Submit Job {loading && <ButtonCircularProgress />}
          </Button>
        </Fragment>
      }
    />
  );
});

AddJobDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  theme: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  // curCluster: PropTypes.object.isRequired,
  // model: PropTypes.object.isRequired
};

function Wrapper(props) {
  const { open, onClose, onSuccess, model, curCluster} = props;
  return (
    <AddJobDialog open={open} onClose={onClose} onSuccess={onSuccess} model={model} curCluster={curCluster}/>
  );
}


AddJobDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  // curCluster: PropTypes.object.isRequired,
  // model: PropTypes.object.isRequired
};

export default Wrapper;
