import React, { useState, useCallback, useEffect } from "react";
import PropTypes from "prop-types";
import axios from 'axios';
import { List, Divider, Paper, withStyles } from "@material-ui/core";
import JobTable from "./JobTable";
import JobHeader from "./JobHeader";
import getTokenHeaders from "../Token"
function getUser() {
    try {
      return localStorage.getItem('user');
    } catch (err) {
      return ""
    }
  }

const styles = {
  divider: {
    backgroundColor: "rgba(0, 0, 0, 0.26)"
  }
};

function Job(props) {
  const {
    classes,
    curCluster,
    pushMessageToSnackbar,
    selectJob
  } = props;

  const [jobs, setJobs] = useState([]);

  const fetchJobs = useCallback(() => {
    if (curCluster === null) {
        return Promise.resolve();
    }
    const user = getUser()
    const token = getTokenHeaders()
    const jsonPayload =  {"cluster": curCluster.name,
        "user": user,
    }
    console.log("submitting api jobs")

    axios.post('/api/jobs', jsonPayload, {headers: token })
      .then(res => {
          console.log("jobs returned ", res.data)
        setJobs(res.data)
      })
      .catch(error => {
        setJobs([])
        setTimeout(() => {
          pushMessageToSnackbar({
            text: "error getting jobs " + error,
          });
        }, 1500);
  })}, [setJobs, curCluster, pushMessageToSnackbar]);


  useEffect(() => {
    selectJob();
    fetchJobs();
  }, [
    selectJob,
    fetchJobs,
  ]);

  return (
    <Paper>
      <List disablePadding>
        <JobHeader classes={classes} />
        <Divider className={classes.divider} />
        <JobTable jobs={jobs}/>
      </List>
    </Paper>
  );
}

Job.propTypes = {
  classes: PropTypes.object.isRequired,
  // curCluster: PropTypes.object.isRequired,
  selectJob: PropTypes.func.isRequired,
  pushMessageToSnackbar: PropTypes.func.isRequired,
};

export default withStyles(styles)(Job);
