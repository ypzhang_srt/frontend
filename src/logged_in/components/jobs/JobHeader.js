import React, {useCallback } from "react";
import PropTypes from "prop-types";
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import { ListItemText, Toolbar, withStyles } from "@material-ui/core";

const styles = {
  toolbar: {
    justifyContent: "space-between"
  },
  divider: {
    backgroundColor: "rgba(0, 0, 0, 0.26)"
  }
};

function JobHeader(props) {
  const { classes } = props;
  const clickRefresh = useCallback(() => {
    console.log("in click refresh job")
  }, []);
  return (
    <Toolbar className={classes.toolbar}>
      <ListItemText primary="Jobs" secondary="Current Running Jobs" />
      <IconButton aria-label="output" onClick={clickRefresh} disabled={false} className={classes.margin}>
      <RefreshIcon fontSize="small" />
      </IconButton>
    </Toolbar>
  );
}

JobHeader.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(JobHeader);
