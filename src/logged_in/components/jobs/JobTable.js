import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
//import TableHead from "@material-ui/core/TableHead";
// import axios from "axios";

import {
  Button,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  withStyles
} from "@material-ui/core";
import EnhancedTableHead from "../../../shared/components/EnhancedTableHead";
// import IconButton from '@material-ui/core/IconButton';
// import Collapse from "@material-ui/core/Collapse";
//import Typography from "@material-ui/core/Typography";
// import ColorfulChip from "../../../shared/components/ColorfulChip";
// import unixToDateString from "../../../shared/functions/unixToDateString";
import HighlightedInformation from "../../../shared/components/HighlightedInformation";
// import currencyPrettyPrint from "../../../shared/functions/currencyPrettyPrint";
// import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
// import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
//import getTokenHeaders from "../Token"


const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

const styles = theme => ({
  tableWrapper: {
    overflowX: "auto",
    width: "100%"
  },
  blackBackground: {
    backgroundColor: theme.palette.primary.main
  },
  contentWrapper: {
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(2)
    },
    width: "100%"
  },
  dBlock: {
    display: "block !important"
  },
  dNone: {
    display: "none !important"
  },
  firstData: {
    paddingLeft: theme.spacing(3)
  },
  divider: {
    backgroundColor: "rgba(0, 0, 0, 0.26)"
  }
});


function Row(props) {
  const { job } = props;
  // const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();
  // const [jobs, setJobs] = React.useState([]);

  const handleClick = useCallback(
    (event) => {
      // openAddJobDialog(job);
    },
    []
  );

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell
          component="th"
          scope="row"
          className={classes.firstData}
        >
          {job.id}
        </TableCell>
        <TableCell component="th" scope="row">
          {job.job_name}
        </TableCell>
        <TableCell component="th" scope="row">
          {job.queue}
        </TableCell>
        <TableCell component="th" scope="row">
          {job.status}
        </TableCell>
        <TableCell component="th" scope="row">
          {job.time}
        </TableCell>

        <TableCell component="th" scope="row">
        <Button
          variant="contained"
          color="secondary"
          onClick={handleClick}
          disableElevation
          >
            Cancel
        </Button>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}


const rows = [
  {id: "id",
  numeric: false,
  label: "ID"},
  {
    id: "job_name",
    numeric: false,
    label: "Name"
  },
  {
    id: "queue",
    numeric: false,
    label: "Queue"
  },
  {
    id: "status",
    numeric: false,
    label: "Status"
  },
  {
    id: "time",
    numeric: false,
    label: "Time"
  },
  {
    id: "cancel",
    numeric: false,
    label: "Cancel"
  }
];

const rowsPerPage = 25;

function JobTable(props) {
  const { jobs, /* openAddJobDialog, theme,*/ classes } = props;
  const [page, setPage] = useState(0);


  const handleChangePage = useCallback(
    (_, page) => {
      setPage(page);
    },
    [setPage]
  );

  if (jobs.length > 0) {
    return (
      <div className={classes.tableWrapper}>
        <Table aria-labelledby="tableTitle">
          <EnhancedTableHead rowCount={jobs.length} rows={rows} />
          <TableBody>
            {jobs
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((job, index) => (
                  <Row key={index} job={job} />

              ))}
          </TableBody>
        </Table>

        <TablePagination
          component="div"
          count={jobs.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={handleChangePage}
          classes={{
            select: classes.dNone,
            selectIcon: classes.dNone,
            actions: jobs.length > 0 ? classes.dBlock : classes.dNone,
            caption: jobs.length > 0 ? classes.dBlock : classes.dNone
          }}
          labelRowsPerPage=""
        />
      </div>
    );
  }
  return (
    <div className={classes.contentWrapper}>
      <HighlightedInformation>
        No running jobs found.
      </HighlightedInformation>
    </div>
  );
}

JobTable.propTypes = {
  theme: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,

  jobs: PropTypes.arrayOf(PropTypes.object).isRequired,
  // openAddJobDialog: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(JobTable);
