import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";

function LazyLoadAddJobDialog(props) {
  const { open, onClose, onSuccess, onError, model, curCluster } = props;
  const [AddJobDialog, setAddJobDialog] = useState(null);
  const [hasFetchedAddJobDialog, setHasFetchedAddJobDialog] = useState(false);

  useEffect(() => {
    if (open && !hasFetchedAddJobDialog) {
      setHasFetchedAddJobDialog(true);
      import("./AddJobDialog").then(Component => {
        setAddJobDialog(() => Component.default);
      });
    }
  }, [open, hasFetchedAddJobDialog, setHasFetchedAddJobDialog, setAddJobDialog]);

  return (
    <Fragment>
      {AddJobDialog && (
        <AddJobDialog
          open={open}
          onClose={onClose}
          curCluster={curCluster}
          onSuccess={onSuccess}
          onError={onError}
          model={model}
        ></AddJobDialog>
      )}
    </Fragment>
  );

}

LazyLoadAddJobDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  // curCluster: PropTypes.object.isRequired,
  // model: PropTypes.object.isRequired
};

export default LazyLoadAddJobDialog;
