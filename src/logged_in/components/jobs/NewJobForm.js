import React from "react";
import PropTypes from "prop-types";
import { FormControl, MenuItem, FormHelperText, Select, TextField, Grid} from "@material-ui/core";
// import { Queue } from "@material-ui/icons";


function NewJobForm(props) {
  const {
    gpu,
    gpuError,
    version, 
    queue,
    onGpuChange,
    onVersionChange,
    onQueueChange,
    curCluster,
    // name,
    //     setName
  } = props;
  return (
    <Grid container spacing={6} justify="space-between">
      <Grid item xs={6}>
      <FormControl >
      <Select
        value={version}
        onChange={onVersionChange}
        MenuProps={{ disableScrollLock: true }}
      >
        {curCluster["versions"].map((version) => (
          <MenuItem value={version} key={version}>
            {version}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>version</FormHelperText>
      </FormControl>
      </Grid>
      <Grid item xs={6}>
      <FormControl >
      <Select
        value={queue}
        onChange={onQueueChange}
        MenuProps={{ disableScrollLock: true }}
      >
        {curCluster["queues"].map((queue) => (
          <MenuItem value={queue} key={queue}>
            {queue}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>queue</FormHelperText>
      </FormControl>
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          value={gpu}
          onChange={event => {
            onGpuChange(parseInt(event.target.value));
          }}
          InputProps={{
            inputProps: { 
                max: 4, min: 1 
            }}
          }
          error={gpuError ? true : false}
          helperText={gpuError}
          variant="outlined"
          fullWidth
          type="number"
          margin="normal"
          label="GPUs"
        />
      </Grid>
    </Grid>
  );
}

NewJobForm.propTypes = {
  gpu: PropTypes.number.isRequired,
  onGpuChange: PropTypes.func.isRequired,
  onVersionChange: PropTypes.func.isRequired,
  onQueueChange: PropTypes.func.isRequired,
  gpuError: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  setName: PropTypes.func.isRequired
};

export default NewJobForm;
