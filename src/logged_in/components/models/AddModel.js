import React, { Fragment, useState, useCallback } from "react";
import PropTypes from "prop-types";
import axios from 'axios';
import { Button, Box } from "@material-ui/core";
import ActionPaper from "../../../shared/components/ActionPaper";
import ButtonCircularProgress from "../../../shared/components/ButtonCircularProgress";
import AddModelOptions from "./AddModelOptions";
import getTokenHeaders from "../Token"

function AddModel(props) {
  const {
    pushMessageToSnackbar,
    Dropzone,
    EmojiTextArea,
    DateTimePicker,
    ImageCropper,
    onClose,
  } = props;

  const [files, setFiles] = useState([]);
  const [uploadAt, setUploadAt] = useState(new Date());
  const [loading, setLoading] = useState(false);
  const [cropperFile, setCropperFile] = useState(null);

  const acceptDrop = useCallback(
    (file) => {
      setFiles([file]);
    },
    [setFiles]
  );

  const [permission, setPermission] = useState("private");
  const [pathPrefix, setPathPrefix] = useState("./");

  const onPermissionChange = useCallback((permission) => {
    console.log("set permission is", permission)
    setPermission(permission);
  }, [setPermission]);

  const onPathChange = useCallback((pathPrefix) => {
    console.log("set path prefix is", pathPrefix)
    setPathPrefix(pathPrefix);
  }, [setPathPrefix]);
  
  const onStageSuccess = useCallback((file) => {
    console.log("setting loading to false", file)
    setFiles([file]);
    setLoading(false)
  }, [setLoading, setFiles]);

  const onDrop = useCallback(
    (acceptedFiles, rejectedFiles) => {
      if (acceptedFiles.length + rejectedFiles.length > 1) {
        pushMessageToSnackbar({
          isErrorMessage: true,
          text: "You cannot upload more than one file at once",
        });
      } else if (acceptedFiles.length === 0) {
        pushMessageToSnackbar({
          isErrorMessage: true,
          text: "The file you wanted to upload isn't a zip file",
        });
      } else if (acceptedFiles.length === 1) {
        const file = acceptedFiles[0];
        file.preview = URL.createObjectURL(file);
        file.key = new Date().getTime();
        setCropperFile(file);
      }
    },
    [pushMessageToSnackbar, setCropperFile]
  );

  const onCropperClose = useCallback(() => {
    setCropperFile(null);
  }, [setCropperFile]);

  const deleteItem = useCallback(() => {
    setCropperFile(null);
    setFiles([]);
  }, [setCropperFile, setFiles]);

  const onCrop = useCallback(
    (dataUrl) => {
      const file = { ...cropperFile };
      file.preview = dataUrl;
      acceptDrop(file);
      setCropperFile(null);
    },
    [acceptDrop, cropperFile, setCropperFile]
  );

  const handleUpload = useCallback(() => {
    setLoading(true);
    const token = getTokenHeaders()
    // TODO cluster name
    const jsonPayload =  {"cluster": files[0].cluster, "model": {"permission": permission, 
          "path": pathPrefix, 
          "file": "TODO",
          "upload": {
            "key": files[0].key
          },
    }}
    axios.post('/api/model/upload', jsonPayload, {headers: token})
    .then(res => {
      setTimeout(() => {
        pushMessageToSnackbar({
          text: "The model has been uploaded and being processed",
        });
        onClose();
      }, 1500);
    })
    .catch(error => {
      setTimeout(() => {
        pushMessageToSnackbar({
          text: "Error uploading the model",
        });
        onClose();
      }, 1500);
    })}, [permission, pathPrefix, files, onClose, pushMessageToSnackbar, setLoading]);
  

  return (
    <Fragment>
      <ActionPaper
        helpPadding
        maxWidth="md"
        content={
          <AddModelOptions
            EmojiTextArea={EmojiTextArea}
            Dropzone={Dropzone}
            files={files}
            onDrop={onDrop}
            deleteItem={deleteItem}
            DateTimePicker={DateTimePicker}
            uploadAt={uploadAt}
            onChangeUploadAt={setUploadAt}
            onCrop={onCrop}
            ImageCropper={ImageCropper}
            cropperFile={cropperFile}
            onStageSuccess={onStageSuccess}
            onPermissionChange={onPermissionChange}
            onPathChange={onPathChange}
            onCropperClose={onCropperClose}

          />
        }
        actions={
          <Fragment>
            <Box mr={1}>
              <Button onClick={onClose} disabled={loading}>
                Back
              </Button>
            </Box>
            <Button
              onClick={handleUpload}
              variant="contained"
              color="secondary"
              disabled={files.length === 0 || loading}
            >
              Upload {loading && <ButtonCircularProgress />}
            </Button>
          </Fragment>
        }
      />
    </Fragment>
  );
}

AddModel.propTypes = {
  pushMessageToSnackbar: PropTypes.func,
  onClose: PropTypes.func,
  Dropzone: PropTypes.elementType,
  EmojiTextArea: PropTypes.elementType,
  DateTimePicker: PropTypes.elementType,
  ImageCropper: PropTypes.elementType,
};

export default AddModel;
