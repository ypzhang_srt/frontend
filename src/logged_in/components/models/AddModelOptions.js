import React, { Fragment, useState, useCallback } from "react";
import PropTypes from "prop-types";
// import {useDropzone} from 'react-dropzone';
import {
  Typography,
  // IconButton,
  TextField,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  FormControl,
  Select,
  OutlinedInput,
  MenuItem,
  // Box,
  withStyles,
} from "@material-ui/core";
import S3Uploader from "./Upload";
// import CloseIcon from "@material-ui/icons/Close";
import Bordered from "../../../shared/components/Bordered";
import ImageCropperDialog from "../../../shared/components/ImageCropperDialog";

const styles = (theme) => ({
  floatButtonWrapper: {
    position: "absolute",
    top: theme.spacing(1),
    right: theme.spacing(1),
    zIndex: 1000,
  },
  inputRoot: {
    width: 190,
    "@media (max-width:  400px)": {
      width: 160,
    },
    "@media (max-width:  360px)": {
      width: 140,
    },
    "@media (max-width:  340px)": {
      width: 120,
    },
  },
  uploadIcon: {
    fontSize: 48,
    transition: theme.transitions.create(["color", "box-shadow", "border"], {
      duration: theme.transitions.duration.short,
      easing: theme.transitions.easing.easeInOut,
    }),
  },
  imgWrapper: { position: "relative" },
  img: {
    width: "100%",
    border: "1px solid rgba(0, 0, 0, 0.23)",
    borderRadius: theme.shape.borderRadius,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
  },
  uploadText: {
    transition: theme.transitions.create(["color", "box-shadow", "border"], {
      duration: theme.transitions.duration.short,
      easing: theme.transitions.easing.easeInOut,
    }),
  },
  numberInput: {
    width: 110,
  },
  numberInputInput: {
    padding: "9px 34px 9px 14.5px",
  },
  dNone: {
    display: "none",
  },
});

const inputOptions = ["private", "shared"];

function AddModelOptions(props) {
  const {
    //Dropzone,
    classes,
    onStageSuccess,
    onPermissionChange,
    onPathChange,
    // files,
    // deleteItem,
    // onDrop,
    // EmojiTextArea,
    ImageCropper,
    // DateTimePicker,
    cropperFile,
    onCrop,
    onCropperClose,
    // uploadAt,
    //onChangeUploadAt,
  } = props;
  const [permission, setPermission] = useState("private");
  // const [pathPrefix, setPathPrefix] = useState("./");
  // const [option2, setOption2] = useState("None");
  //const [option3, setOption3] = useState("None");
  //const [option4, setOption4] = useState("None");

  const handleChange = useCallback(
    (event) => {
      const { name, value } = event.target;
      switch (name) {
        case "permission":
          setPermission(value)
          onPermissionChange(value);
          break;
        // case "option2":
        //  setOption2(value);
        //  break;
        //case "option3":
        //  setOption3(value);
        //  break;
        // case "option4":
        //  setOption4(value);
        //  break;
        default:
          throw new Error("No branch selected in switch-statement.");
      }
    },
    [onPermissionChange]
  );

  const handlePath = useCallback((event) => {
    console.log("path is", event.target.value)
    onPathChange(event.target.value);
  },[onPathChange]);

  const inputs = 
    [
      {
        state: permission,
        label: "Permission",
        stateName: "permission",
      }
    ];

  return (
    <Fragment>
      {ImageCropper && (
        <ImageCropperDialog
          open={cropperFile ? true : false}
          ImageCropper={ImageCropper}
          src={cropperFile ? cropperFile.preview : ""}
          onCrop={onCrop}
          onClose={onCropperClose}
          aspectRatio={4 / 3}
        />
      )}
      <Typography paragraph variant="h6">
        Upload A Model
      </Typography>

        <S3Uploader permission={permission} onStageSuccess={onStageSuccess}/>
         <Typography paragraph variant="h6">
        Options
      </Typography>
      <List disablePadding>
        <Bordered disableVerticalPadding disableBorderRadius>
          <ListItem divider disableGutters className="listItemLeftPadding">
            <ListItemText>
              <Typography variant="body2">Model Path Prefix</Typography>
            </ListItemText>
            <ListItemSecondaryAction>
              <TextField id="outlined-basic" defaultValue="./"  
              onChange={handlePath}
              label="default to ./" variant="outlined" />
            </ListItemSecondaryAction>
          </ListItem>
          {inputs.map((element, index) => (
            <ListItem
              className="listItemLeftPadding"
              disableGutters
              divider={index !== inputs.length - 1}
              key={index}
            >
              <ListItemText>
                <Typography variant="body2">{element.label}</Typography>
              </ListItemText>
              <FormControl variant="outlined">
                <ListItemSecondaryAction>
                  <Select
                    value={element.state}
                    onChange={handleChange}
                    input={
                      <OutlinedInput
                        name={element.stateName}
                        labelWidth={0}
                        className={classes.numberInput}
                        classes={{ input: classes.numberInputInput }}
                      />
                    }
                    MenuProps={{ disableScrollLock: true }}
                  >
                    {inputOptions.map((innerElement) => (
                      <MenuItem value={innerElement} key={innerElement}>
                        {innerElement}
                      </MenuItem>
                    ))}
                  </Select>
                </ListItemSecondaryAction>
              </FormControl>
            </ListItem>
          ))}
        </Bordered>
      </List>
    </Fragment>
  );
}

AddModelOptions.propTypes = {
  DateTimePicker: PropTypes.elementType,
  Dropzone: PropTypes.elementType,
  ImageCropper: PropTypes.elementType,
  classes: PropTypes.object,
  cropperFile: PropTypes.object,
  onStageSuccess: PropTypes.func,
  onCrop: PropTypes.func,
  onCropperClose: PropTypes.func,
  files: PropTypes.arrayOf(PropTypes.object).isRequired,
  deleteItem: PropTypes.func,
  onDrop: PropTypes.func,
  value: PropTypes.string,
  characters: PropTypes.number,
  uploadAt: PropTypes.instanceOf(Date),
  onChangeUploadAt: PropTypes.func,
};

export default withStyles(styles, { withTheme: true })(AddModelOptions);
