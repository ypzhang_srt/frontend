import React, {useCallback} from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from "axios";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

import getTokenHeaders from "../Token"

const styles = {
  toolbar: {
    justifyContent: "space-between"
  }
};
function getUser() {
    try {
      return localStorage.getItem('user');
    } catch (err) {
      return ""
    }
  }

function GenPlotData(res_data) {
    if (res_data === null) {
        return []
    }
    var plot_data = []
    for (const point_id in res_data.data) {

        var item = {
          "t": parseFloat(res_data.time[point_id]),
        }
        for (const label_id in res_data.labels){
            item[res_data.labels[label_id]] = parseFloat(res_data.data[point_id][label_id])
        }
        // console.log("item is", item)
        plot_data.push(item)
    }
    // console.log("plot ddata is", plot_data)
    return plot_data
}

function ChartDialog(props) {
  const { open, onSetOpen, curCluster, job, model } = props;
  // const [open, setOpen] = React.useState(false);
  // const [scroll, setScroll] = React.useState('paper');
  const [content, setContent] = React.useState(null)

  const handleClose = useCallback(() => {
    onSetOpen(false)
  }, [onSetOpen]);

    // eslint-disable-next-line
    const fetchSeries = useCallback(() => {

    const token = getTokenHeaders()
    // TODO cluster name
    const jsonPayload =  {"cluster": curCluster.name,
        user: getUser(),
        "job": {
            "model": model ,
            "output_dir": job.output_dir,
        } }
        axios.post('/api/job/summary', jsonPayload, {headers: token})
        .then(res => {
            setContent(res.data)
        })
        .catch(error => {
        console.log("failed to get job output")
        setTimeout(() => {
        }, 1500);
        })
    }, [setContent, curCluster, job, model]);


  const scroll = "body" // "paper"
  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
      fetchSeries()
    }
  }, [open, fetchSeries]);


  const plot_data = GenPlotData(content)
  const renderLine = () => {
      if (content === null) {
        return
      }
    return (
      content.labels.map((label, index) => (
        <Line type="monotone" dataKey={label} key={index}/>
      ))
    )
 }

  return (
    <Dialog
    open={open}
    onClose={handleClose}
    scroll={scroll}
    fullWidth={true}
    maxWidth="lg"
    aria-labelledby="scroll-dialog-title"
    aria-describedby="scroll-dialog-description"
   >
       <DialogTitle id="scroll-dialog-title">{content === null? "PLOT": content.keyword}</DialogTitle>
       <DialogContent dividers={scroll === 'paper'}>
       <LineChart width={1200} height={800} data={plot_data}>
            <XAxis dataKey="t"/>
            <YAxis/>
            <Tooltip />
            <Legend />
            <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
            {renderLine()}
        </LineChart>
        </DialogContent>
   </Dialog>
  )
}

ChartDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    curCluster: PropTypes.object.isRequired,
    job: PropTypes.object.isRequired,
    model: PropTypes.object.isRequired,
    onSetOpen: PropTypes.func.isRequired,
};

export default withStyles(styles)(ChartDialog);
