import React, { useState, useCallback, Fragment } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import { Button, Box, withTheme, ListItemText, ListItemIcon } from "@material-ui/core";
import Checkbox from '@material-ui/core/Checkbox';
// import FormGroup from '@material-ui/core/FormGroup';
// import StripeCardForm from "../subscription/stripe/StripeCardForm";
import FormDialog from "../../../shared/components/FormDialog";
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import ColoredButton from "../../../shared/components/ColoredButton";
import HighlightedInformation from "../../../shared/components/HighlightedInformation";
import ButtonCircularProgress from "../../../shared/components/ButtonCircularProgress";
import getTokenHeaders from "../Token"
function getUser() {
  try {
    return localStorage.getItem('user');
  } catch (err) {
    return ""
  }
}

const DeleteModelDialog = withTheme(function (props) {
  const { open, onClose, onSuccess, curCluster, model, onSetModels } = props;

  const [loading, setLoading] = useState(false);
  const [checkRecursive, setCheckRecursive] = useState(false);

  const onCheckChange = useCallback((event) => {
    console.log("checked", event.target.checked)
    //setState({ ...state, [event.target.name]: event.target.checked });
    setCheckRecursive(event.target.checked)
  },[setCheckRecursive]);

  const renderModelComponent = (model) => {
        return (
          <Fragment>
            <HighlightedInformation>
              {model["file"]} @  {model["path"]}
            </HighlightedInformation>
            <Box display="flex" alignItems="center">
              <Box mr={2}>
                <ListItemText
                  primary="remove the belonging directory"
                  //secondary={isAccountActivated ? "Activated" : "Not activated"}
                  className="mr-2"
                />
              </Box>
              <ListItemIcon>
                
                  <Checkbox
                  checked={checkRecursive}
                  onChange={onCheckChange}
                  label="remove the belonging directory?"
                  />
              </ListItemIcon>
            </Box>
          </Fragment>
        );
  };

  return (
    <FormDialog
      open={open}
      onClose={onClose}
      headline="Delete Model"
      hideBackdrop={false}
      loading={loading}
      onFormSubmit={async event => {
        event.preventDefault();
        setLoading(true);
        const token = getTokenHeaders()
        const user = getUser()
        model["remove_dir"] = checkRecursive
        const jsonPayload =  {
          "action": "delete",
          "cluster": curCluster.name,
        "user": user,
        "model": model}

        axios.post('/api/model', jsonPayload, {headers: token})
          .then(res => {
            console.log("submit delete model")
            setLoading(false);
            onSetModels(res.data)
            onSuccess();
            
        })
        .catch(error => {
          console.log("submit delete model error", error)
          setTimeout(() => {
          }, 1500);
          setLoading(false);
          onClose();
          // onFailure();
        })
      }}
      content={
        <Box pb={2}>
          {renderModelComponent(model)}
        </Box>
      }
      actions={
        <Fragment>
          <Button
            fullWidth
            variant="contained"
            color="secondary"
            type="submit"
            size="large"
            disabled={loading}
          >
            Delete Model {loading && <ButtonCircularProgress />}
          </Button>
        </Fragment>
      }
    />
  );
});

DeleteModelDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  theme: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onSetModels: PropTypes.func.isRequired,
  // curCluster: PropTypes.object.isRequired,
  model: PropTypes.object.isRequired
};

function Wrapper(props) {
  const { open, onClose, onSuccess, model, curCluster, onSetModels} = props;
  return (
    <DeleteModelDialog open={open} onClose={onClose} onSuccess={onSuccess} model={model} curCluster={curCluster} onSetModels={onSetModels} />
  );
}


DeleteModelDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  curCluster: PropTypes.string.isRequired,
  onSetModels: PropTypes.func.isRequired,
  model: PropTypes.object.isRequired
};

export default Wrapper;
