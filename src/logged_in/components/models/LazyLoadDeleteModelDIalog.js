import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";

function LazyLoadDeleteModelDialog(props) {
  const { open, onClose, onSuccess, onError, model, curCluster, onSetModels } = props;
  const [DeleteModelDialog, setDeleteModelDialog] = useState(null);
  const [hasDeleteModelDialog, setHasDeleteModelDialog] = useState(false);

  useEffect(() => {
    if (open && !hasDeleteModelDialog) {
      setHasDeleteModelDialog(true);
      import("./DeleteModelDialog").then(Component => {
        setDeleteModelDialog(() => Component.default);
      });
    }
  }, [open, hasDeleteModelDialog, setHasDeleteModelDialog, setDeleteModelDialog]);

  return (
    <Fragment>
      {DeleteModelDialog && (
        <DeleteModelDialog
          open={open}
          onClose={onClose}
          curCluster={curCluster}
          onSuccess={onSuccess}
          onError={onError}
          model={model}
          onSetModels={onSetModels}
        ></DeleteModelDialog>
      )}
    </Fragment>
  );

}

LazyLoadDeleteModelDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  // curCluster: PropTypes.object.isRequired,
  onSetModels: PropTypes.func.isRequired,
  // model: PropTypes.object.isRequired
};

export default LazyLoadDeleteModelDialog;
