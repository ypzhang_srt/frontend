import React, { useEffect, useCallback, useState } from "react";
import PropTypes from "prop-types";
import axios from 'axios';
import { List, Divider, Paper, withStyles } from "@material-ui/core";
import ModelTable from "./ModelTable";
import ModelInfo from "./ModelInfo";
import AddModel from "./AddModel";
import getTokenHeaders from "../Token"

const styles = {
  divider: {
    backgroundColor: "rgba(0, 0, 0, 0.26)"
  }
};

function Model(props) {
  const {
    classes,
    selectModel,
    EmojiTextArea,
    ImageCropper,
    Dropzone,
    curCluster,
    DateTimePicker,
    pushMessageToSnackbar,
    openAddJobDialog,
    openDeleteModelDialog,
    models,
    onSetModels,
  } = props;
  // const [models, setModels] = useState([]);
  const [isAddPostPaperOpen, setIsAddPostPaperOpen] = useState(false);

  const openAddPostModal = useCallback(() => {
    setIsAddPostPaperOpen(true);
  }, [setIsAddPostPaperOpen]);

  const closeAddPostModal = useCallback(() => {
    setIsAddPostPaperOpen(false);
  }, [setIsAddPostPaperOpen]);

  // eslint-disable-next-line
  const fetchModelInfo = useCallback(() => {
    if (curCluster === null) {
      return
    }
    const token = getTokenHeaders()
    // TODO cluster name
    const jsonPayload =  {"cluster": curCluster.name, "action": "list"}
    axios.post('/api/model', jsonPayload, {headers: token})
      .then(res => {
        onSetModels(res.data)
      })
      .catch(error => {
        onSetModels([])
        setTimeout(() => {
        }, 1500);
      })
  }, [curCluster, onSetModels]);

  useEffect(() => {
    selectModel();
    fetchModelInfo();
  }, [
    selectModel,
    fetchModelInfo,
  ]);


  if (isAddPostPaperOpen) {
    return <AddModel
      onClose={closeAddPostModal}
      EmojiTextArea={EmojiTextArea}
      ImageCropper={ImageCropper}
      Dropzone={Dropzone}
      DateTimePicker={DateTimePicker}
      pushMessageToSnackbar={pushMessageToSnackbar}
    />
  }

  return (
    <Paper>
      <List disablePadding>
        <ModelInfo classes={classes} openAddPostModal={openAddPostModal}/>
        <Divider className={classes.divider} />
        <ModelTable models={models} curCluster={curCluster}
            openAddJobDialog={openAddJobDialog}
            openDeleteModelDialog={openDeleteModelDialog}
            onSetModels={onSetModels} />
      </List>
    </Paper>
  );
}

Model.propTypes = {
  classes: PropTypes.object.isRequired,
  selectModel: PropTypes.func.isRequired,
  openAddJobDialog: PropTypes.func.isRequired,
  openDeleteModelDialog: PropTypes.func.isRequired,
  EmojiTextArea: PropTypes.elementType,
  ImageCropper: PropTypes.elementType,
  Dropzone: PropTypes.elementType,
  onSetModels: PropTypes.func.isRequired,
  DateTimePicker: PropTypes.elementType,
  // curCluster: PropTypes.object.isRequired,
  // models: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default withStyles(styles)(Model);