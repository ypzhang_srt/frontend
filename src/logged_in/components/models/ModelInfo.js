import React, {useCallback } from "react";
import PropTypes from "prop-types";
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import { ListItemText, Button, Toolbar, withStyles } from "@material-ui/core";

const styles = {
  toolbar: {
    justifyContent: "space-between"
  },
  divider: {
    backgroundColor: "rgba(0, 0, 0, 0.26)"
  }
};

function ModelInfo(props) {
  const { classes, openAddPostModal } = props;
  const clickRefresh = useCallback(() => {
    console.log("in logs")
  }, []);
  return (
    <Toolbar className={classes.toolbar}>
      <ListItemText primary="Models" secondary="List of Models" />
      <IconButton aria-label="output" onClick={clickRefresh} disabled={true} className={classes.margin}>
      <RefreshIcon fontSize="small" />
      </IconButton>
      <Button
        variant="contained"
        color="secondary"
        onClick={openAddPostModal}
        disableElevation
      >
        Upload New Model
      </Button>
    </Toolbar>
  );
}

ModelInfo.propTypes = {
  classes: PropTypes.object.isRequired,
  openAddPostModal: PropTypes.func.isRequired
};

export default withStyles(styles)(ModelInfo);
