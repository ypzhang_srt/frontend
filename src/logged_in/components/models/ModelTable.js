import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
//import TableHead from "@material-ui/core/TableHead";
import axios from "axios";

import {
  Button,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  withStyles
} from "@material-ui/core";
import EnhancedTableHead from "../../../shared/components/EnhancedTableHead";
import IconButton from '@material-ui/core/IconButton';
import Collapse from "@material-ui/core/Collapse";
import DeleteIcon from '@material-ui/icons/Delete';
import DescriptionIcon from '@material-ui/icons/Description';
import ListIcon from '@material-ui/icons/List';
import TimelineIcon from '@material-ui/icons/Timeline';
import GetAppIcon from '@material-ui/icons/GetApp';
//import Typography from "@material-ui/core/Typography";
// import ColorfulChip from "../../../shared/components/ColorfulChip";
// import unixToDateString from "../../../shared/functions/unixToDateString";
import HighlightedInformation from "../../../shared/components/HighlightedInformation";
// import currencyPrettyPrint from "../../../shared/functions/currencyPrettyPrint";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import getTokenHeaders from "../Token"
import PRTDialog from "./PRTDialog"
import ChartDialog from "./ChartDialog"

function getUser() {
  try {
    return localStorage.getItem('user');
  } catch (err) {
    return ""
  }
}

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

const styles = theme => ({
  tableWrapper: {
    overflowX: "auto",
    width: "100%"
  },
  blackBackground: {
    backgroundColor: theme.palette.primary.main
  },
  contentWrapper: {
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(2)
    },
    width: "100%"
  },
  dBlock: {
    display: "block !important"
  },
  dNone: {
    display: "none !important"
  },
  firstData: {
    paddingLeft: theme.spacing(3)
  }
});

const job_rows = [
  {
    id: "id",
    numeric: false,
    label: "ID"
  },
  {
    id: "version",
    numeric: false,
    label: "VERSION"
  },
  {
    id: "status",
    numeric: false,
    label: "Status"
  },
  {
    id: "elapsed",
    numeric: false,
    label: "Elapsed"
  },
  {
    id: "view",
    numeric: false,
    label: "View"
  }
]

function JobRow(props) {
  const { job, model, curCluster } = props;
  const classes = useRowStyles();
  // TODO add spaces between the icons
  const [PRTOpen, setPRTOpen] = React.useState(false)
  const [ChartOpen, setChartOpen] = React.useState(false)
  const clickDescription = useCallback(() => {
    setPRTOpen(true)
  }, [setPRTOpen]);

  const clickDownload = useCallback(() => {
    console.log("in download")
  }, []);


  const clickLogs = useCallback(() => {
    console.log("in logs")
  }, []);

  const clickChart = useCallback(() => {
    setChartOpen(true)
  }, [setChartOpen]);

  const onSetPRTOpen = useCallback((open) => {
    setPRTOpen(open)
  }, [setPRTOpen]);

  const onSetChartOpen = useCallback((open) => {
    setChartOpen(open)
  }, [setChartOpen]);

  console.log("job is", job)
  const enable_view_buttons = (job.last_status === "finished" || job.last_status === "running" || job.last_status === "completed")
  const enable_download_button = (job.last_status === "finished" || job.last_status === "completed")

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell
          component="th"
          scope="row"
          className={classes.firstData}
        >
          {job.id}
        </TableCell>
        <TableCell
          component="th"
          scope="row"
          className={classes.firstData}
        >
          {job.echelon_version}
        </TableCell>

        <TableCell component="th" scope="row">
          {job.last_status}
        </TableCell>
        <TableCell component="th" scope="row">
          {job.elapsed === null? "" : job.elapsed}
        </TableCell>
        <TableCell component="th" scope="row">
        <IconButton aria-label="output" onClick={clickDescription} disabled={!enable_view_buttons} className={classes.margin}>
        <DescriptionIcon fontSize="small" />
        </IconButton>
        <PRTDialog open={PRTOpen} onSetOpen={onSetPRTOpen} curCluster={curCluster} job={job} model={model}>
        </PRTDialog>

        <IconButton aria-label="chart" onClick={clickChart} disabled={!enable_view_buttons} className={classes.margin}>
        <TimelineIcon fontSize="small" />
        </IconButton>
        <ChartDialog open={ChartOpen} onSetOpen={onSetChartOpen} curCluster={curCluster} job={job} model={model}>
        </ChartDialog>

        <IconButton aria-label="logs" onClick={clickLogs} disabled={!enable_view_buttons} className={classes.margin}>
        <ListIcon fontSize="small" />
        </IconButton>

        <IconButton aria-label="download" onClick={clickDownload} disabled={!enable_download_button} className={classes.margin}>
        <GetAppIcon fontSize="small" />
        </IconButton>


        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}




function Row(props) {
  const { model, openAddJobDialog, openDeleteModelDialog, curCluster } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();
  const [jobs, setJobs] = React.useState([]);

  const handleClick = useCallback(
    (event) => {
      openAddJobDialog(model);
    },
    [openAddJobDialog, model]
  );


  const handleDeleteClick = useCallback(
    (event) => {
      openDeleteModelDialog(model);
    },
    [openDeleteModelDialog, model]
  );

  const handleOpenAModel = useCallback(() => {
    if (!open) {
      const token = getTokenHeaders()
      const user = getUser()
      const jsonPayload =  {"cluster": curCluster.name,
          "user": user,
          "model": model}
      axios.post("/api/model/job", jsonPayload, {headers: token})
        .then(res => {
          console.log("model jos is", res.data)
          setJobs(res.data)
          // setLoading(false);
          // onSuccess();
          // onSetModels(res.data)
        // setModels(res.data)
      })
      .catch(error => {
        console.log("submit job error", error)
        setTimeout(() => {
        }, 1500);
        setJobs([])
        // setLoading(false);
        // onClose();
        // onFailure();
      })
    }
    setOpen(!open)
  }, [model, open, setOpen, setJobs, curCluster]);

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={handleOpenAModel}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell
          component="th"
          scope="row"
          className={classes.firstData}
        >
          {model.file}
        </TableCell>
        <TableCell component="th" scope="row">
          {model.permission}
        </TableCell>
        <TableCell component="th" scope="row">
          {model.path}
        </TableCell>
        <TableCell component="th" scope="row">
        <Button
          variant="contained"
          color="secondary"
          onClick={handleClick}
          disableElevation
          >
            Run
        </Button>
        </TableCell>
        <TableCell component="th" scope="row">
        <IconButton aria-label="delete" onClick={handleDeleteClick} className={classes.margin}>
          <DeleteIcon fontSize="small" />
        </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>

              <Table aria-labelledby="tableTitle">
          <EnhancedTableHead rowCount={jobs.length} rows={job_rows} />
          <TableBody>
            {jobs
              .map((job, index) => (
                  <JobRow key={index} job={job} curCluster={curCluster} model={model}/>

              ))}
          </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}


const rows = [
  {id: "jobs",
  numeric: false,
  label: "Jobs"},
  {
    id: "file",
    numeric: false,
    label: "File"
  },
  {
    id: "permission",
    numeric: false,
    label: "Permission"
  },
  {
    id: "path",
    numeric: false,
    label: "Path"
  },
  {
    id: "run",
    numeric: false,
    label: "Run"
  },
  {
    id: "edit",
    numeric: false,
    label: "Edit"
  }
];

const rowsPerPage = 25;

function ModelTable(props) {
  const { models, openAddJobDialog, openDeleteModelDialog, curCluster, /*theme,*/ classes, onSetModels } = props;
  const [page, setPage] = useState(0);


  const handleChangePage = useCallback(
    (_, page) => {
      setPage(page);
    },
    [setPage]
  );


  if (models.length > 0) {
    return (
      <div className={classes.tableWrapper}>
        <Table aria-labelledby="tableTitle">
          <EnhancedTableHead rowCount={models.length} rows={rows} />
          <TableBody>
            {models
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((model, index) => (
                  <Row key={index} model={model} curCluster={curCluster} openAddJobDialog={openAddJobDialog}
                    openDeleteModelDialog={openDeleteModelDialog} onSetModels={onSetModels} />

              ))}
          </TableBody>
        </Table>

        <TablePagination
          component="div"
          count={models.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={handleChangePage}
          classes={{
            select: classes.dNone,
            selectIcon: classes.dNone,
            actions: models.length > 0 ? classes.dBlock : classes.dNone,
            caption: models.length > 0 ? classes.dBlock : classes.dNone
          }}
          labelRowsPerPage=""
        />
      </div>
    );
  }
  return (
    <div className={classes.contentWrapper}>
      <HighlightedInformation>
        No models found.
      </HighlightedInformation>
    </div>
  );
}

ModelTable.propTypes = {
  theme: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  // curCluster: PropTypes.object.isRequired,
  models: PropTypes.arrayOf(PropTypes.object).isRequired,
  openAddJobDialog: PropTypes.func.isRequired,
  onSetModels: PropTypes.func.isRequired,
  openDeleteModelDialog: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(ModelTable);
