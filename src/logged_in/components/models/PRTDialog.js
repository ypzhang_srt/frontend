import React, {useCallback} from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from "axios";
import getTokenHeaders from "../Token"

const styles = {
  toolbar: {
    justifyContent: "space-between"
  }
};
function getUser() {
    try {
      return localStorage.getItem('user');
    } catch (err) {
      return ""
    }
  }

function PRTDialog(props) {
  const { open, onSetOpen, curCluster, job, model } = props;
  // const [open, setOpen] = React.useState(false);
  // const [scroll, setScroll] = React.useState('paper');
  const [content, setContent] = React.useState(null)

  const handleClose = useCallback(() => {
    onSetOpen(false)
  }, [onSetOpen]);

    // eslint-disable-next-line
    const fetchPRT = useCallback(() => {

    const token = getTokenHeaders()
    // TODO cluster name
    const jsonPayload =  {"cluster": curCluster.name,
        user: getUser(),
        "job": {
            "model": model ,
            "output_dir": job.output_dir,
        } }
    axios.post('/api/job/output', jsonPayload, {headers: token})
        .then(res => {
        setContent(res.data)
        })
        .catch(error => {
        console.log("failed to get job output")
        setTimeout(() => {
        }, 1500);
        })
    }, [setContent, curCluster, job, model]);


  const scroll = "body" // "paper"
  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
    fetchPRT()
  }, [open, fetchPRT]);

  const renderContent = (lines) => {
      return (
        lines.map((line, index) => (
            <DialogContentText
            style={{ whiteSpace: 'pre' }}
            key={index}
            id="content-line"
            ref={descriptionElementRef}
            display="block"
            tabIndex={-1}
          >
            {line}
          </DialogContentText>
        ))
      )
  }
  if (content !== null) {
    console.log(content)
  }
  return (
    <Dialog
    open={open}
    onClose={handleClose}
    scroll={scroll}
    fullWidth={true}
    maxWidth="lg"
    aria-labelledby="scroll-dialog-title"
    aria-describedby="scroll-dialog-description"
   >
       <DialogTitle id="scroll-dialog-title">PRT</DialogTitle>
       <DialogContent dividers={scroll === 'paper'}>
       {content === null? "" : renderContent(content.head) }
       {content === null? "" : renderContent(content.tail) }
        </DialogContent>
   </Dialog>
  )
}

PRTDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    curCluster: PropTypes.object.isRequired,
    job: PropTypes.object.isRequired,
    model: PropTypes.object.isRequired,
    onSetOpen: PropTypes.func.isRequired,
};

export default withStyles(styles)(PRTDialog);
