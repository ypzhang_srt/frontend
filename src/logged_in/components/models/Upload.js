import React, { useState} from "react";
import Dropzone from 'react-dropzone-uploader'
import axios from 'axios';
import getTokenHeaders from "../Token"
import PropTypes from "prop-types";
import {
    withStyles,
} from "@material-ui/core";

  
const styles = (theme) => ({
    uploadIcon: {
      fontSize: 48,
      transition: theme.transitions.create(["color", "box-shadow", "border"], {
        duration: theme.transitions.duration.short,
        easing: theme.transitions.easing.easeInOut,
      }),
    },
 
    uploadText: {
      transition: theme.transitions.create(["color", "box-shadow", "border"], {
        duration: theme.transitions.duration.short,
        easing: theme.transitions.easing.easeInOut,
      }),
    },
 
  });
  
  /**
import 'react-dropzone-uploader/dist/styles.css'

**/

function S3Uploader(props) {
    const {
      //Dropzone,
  //    classes,
      // files,
      // deleteItem,
      // onDrop,
      onStageSuccess,
      permission,
      pathPrefix
      // EmojiTextArea,
    } = props;

    // specify upload params and url for your files
    // const getUploadParams = ({ meta }) => { return { url: 'https://httpbin.org/post' } }
    const [model, setModel] = useState({});
    // The associated S3 bucket needs to allow CORS permissions. 
    // Add them in the aws console
    const getUploadParams = async ({ file, meta: { name } }) => {
        const token = getTokenHeaders()
        // TODO cluster name
        const jsonPayload =  {"cluster": "hydra", "model": {"permission": {permission}, "path": {pathPrefix}, "file": "TODO"}}
        try {
            const resp = await axios.post('/api/model/upload', jsonPayload, {headers: token})
            const fields = resp.data.model.fields
            const uploadUrl = resp.data.model.url
            // const fileUrl = resp.data.model.url + resp.data.key
            // return { fields, meta: { fileUrl }, url: uploadUrl }
            const model = {
              "key": resp.data.model.fields.key,
              "cluster": "hydra",
            }
            setModel(model)
            return { fields, url: uploadUrl }
        } catch (err) {
            // TODO
        }
      }

    /* const getUploadParams = async ({ file, meta: { name } }) => {
        const token = getTokenHeaders()
            // TODO cluster name
        const jsonPayload =  {"cluster": "hydra", "model": {"permission": "private"}}
        try {
            console.log("file is ", file)
            const resp = await axios.post('/api/model/upload', jsonPayload, {headers: token})
            console.log("resp is", resp)
            const uploadUrl = resp.data.model
            // const fileUrl = resp.data.model.url + resp.data.key
            console.log("uploadUrl is", uploadUrl)

            return { body: file, url: uploadUrl }
        } catch (err) {
        }
      }
      */

    // called every time a file's `status` changes
    const handleChangeStatus = ({ meta, file }, status) => { 
      if (meta.status === "done") {
        onStageSuccess(model)
      }
      console.log(status, meta, file) 
    }
    
    // receives array of files that are done uploading when submit button is clicked
    /*
    const handleSubmit = (files, allFiles) => {
      console.log(files.map(f => f.meta))
      allFiles.forEach(f => f.remove())
    }
    */
  
    return (
      <Dropzone
        getUploadParams={getUploadParams}
        onChangeStatus={handleChangeStatus}
        // onSubmit={handleSubmit}
        maxFiles={1}
        accept="application/zip, application/x-zip-compressed"
      />
    )
  }


S3Uploader.propTypes = {
    onStageSuccess: PropTypes.func,
    permission: PropTypes.string,
    pathPrefix: PropTypes.string
  };

  export default withStyles(styles, { withTheme: true })(S3Uploader);
