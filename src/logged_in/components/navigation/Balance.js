import React, {useState, useEffect, useCallback} from "react";
import PropTypes from "prop-types";
import axios from 'axios';
import { OutlinedInput, withStyles } from "@material-ui/core";
import hourPrettyPrint from "../../../shared/functions/hourPrettyPrint";
import getTokenHeaders from "../Token"

const styles = {
  input: { padding: "0px 9px", cursor: "pointer" },
  outlinedInput: {
    width: 90,
    height: 40,
    cursor: "pointer"
  },
  wrapper: {
    display: "flex",
    alignItems: "center"
  }
};

function Balance(props) {
  const { classes, openAddGPUHourDialog } = props;

  const [balance, setBalance] = useState(0);
  const fetchBalance = useCallback(() => {
    const token = getTokenHeaders()

    if (token !== null && token !== "0" ) {
      axios.get('/api/balance', {headers: token,
      })
        .then(res => {
          console.log("balance is ", res.data)
          setBalance(res.data["balance"])
        })

        .catch(error => {
          setTimeout(() => {
          }, 1500);
        })
      }
  }, [setBalance]);


  useEffect(() => {
    fetchBalance();

    return () => {

    }
  }, [
    fetchBalance,
  ]);

  return (
    <div className={classes.wrapper}>
      <OutlinedInput
        value={balance === null ? "" : hourPrettyPrint(balance)}
        className={classes.outlinedInput}
        classes={{ input: classes.input }}
        readOnly
        labelWidth={0}
        onClick={openAddGPUHourDialog}
      />
    </div>
  );
}

Balance.propTypes = {
  classes: PropTypes.object.isRequired,
  openAddGPUHourDialog: PropTypes.func.isRequired
};

export default withStyles(styles)(Balance);
