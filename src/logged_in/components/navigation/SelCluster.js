import React from "react";
import PropTypes from "prop-types";
import { FormControl, FormHelperText, Select, withStyles, MenuItem } from "@material-ui/core";

const styles = (theme) => ({
  input: { padding: "0px 9px", cursor: "pointer" },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  outlinedInput: {
    width: 90,
    height: 40,
    cursor: "pointer"
  },
  wrapper: {
    display: "flex",
    alignItems: "center"
  }
});

function SelCluster(props) {
  const { curCluster, clusters, classes, onSelectCluster } = props;
  const handleSelect = (event) => {
    onSelectCluster(event.target.value)
    // setCurCluster(event.target.value);
  };
  const value = (curCluster === null)? "" : curCluster.name
  return (
    <div className={classes.wrapper}>
       <FormControl className={classes.formControl}>
      <Select
        value={value}
        onChange={handleSelect}
        MenuProps={{ disableScrollLock: true }}
      >
        {clusters.map((cluster) => (
          <MenuItem value={cluster.name} key={cluster.name}>
            {cluster.name}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>Select the cluster</FormHelperText>
      </FormControl>
    </div>
  );
}

SelCluster.propTypes = {
  clusters: PropTypes.arrayOf(PropTypes.object),
  // curCluster: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  onSelectCluster: PropTypes.func.isRequired
};

export default withStyles(styles)(SelCluster);
