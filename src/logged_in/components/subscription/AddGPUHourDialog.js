import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
import { loadStripe } from "@stripe/stripe-js";
import {
  Elements,

} from "@stripe/react-stripe-js";
import { Button, withTheme } from "@material-ui/core";
import GPUHourForm from "./stripe/GPUHourForm";
import FormDialog from "../../../shared/components/FormDialog";
// import ColoredButton from "../../../shared/components/ColoredButton";
// import HighlightedInformation from "../../../shared/components/HighlightedInformation";
import ButtonCircularProgress from "../../../shared/components/ButtonCircularProgress";
import { CardMedia } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 200,
      paddingTop: 200, //'56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },

  }));


// process.env.SRT_STRIPE_PUBLISHABLE_KEY
// const stripePromise = loadStripe(process.env.SRT_STRIPE_PUBLISHABLE_KEY)
const stripePromise = loadStripe("pk_live_51IhwgSLBiyM2KNwGimH1ivkBOFxyRrsjs9Mt9ukj19FEhowmqC5jxxUPrqSMKLIJL0PKRNokvbp11wdmJ8BlTlph004o5lfGcN");

// const paymentOptions = ["Credit Card"] // , "SEPA Direct Debit"];

const AddGPUHourDialog = withTheme(function (props) {
  const classes = useStyles();
  const { open, onClose } = props;
  const [loading, setLoading] = useState(false);
  const [hour, setHour] = useState(5);

  const onHourChange = hour => {
    if (hour <= 0) {
      return;
    }
    setHour(hour);
  };

  const onFormSubmit = async(event) => {
    event.preventDefault();
    setLoading(true);
    const stripe = await stripePromise;
    const { error } = await stripe.redirectToCheckout({
      lineItems: [{ price: 'price_1Ij2rFLBiyM2KNwGhpgP4GoK', quantity: hour}],
      mode: "payment",
      clientReferenceId: localStorage.getItem('user'),
      // successUrl: `${window.location.origin}/success?session_id={CHECKOUT_SESSION_ID}`,
      successUrl: `${window.location.origin}/c/dashboard`,
      // cancelUrl: `${window.location.origin}/canceled`,
      cancelUrl: `${window.location.origin}/c/dashboard`,

    });

    setLoading(false);
    if (error) {
        console.log("error", error)
    }else {
        console.log("good")
    }
  };

  const renderPaymentComponent = () => {
    return (
        <Fragment>
                  <CardMedia
        className={classes.media}
        image={`${process.env.PUBLIC_URL}/images/logged_in/Echelon-Logo-FNL.jpg`}
        title="GPU hour"
      />
        <GPUHourForm
          onHourChange={onHourChange}
          hour={hour}
        />
        </Fragment>
    );
  };

  return (
    <FormDialog
      open={open}
      onClose={onClose}
      headline="Recharge GPU Batteries"
      hideBackdrop={false}
      loading={loading}
      onFormSubmit={onFormSubmit}
      content={
          renderPaymentComponent()
      }
      actions={
        <Fragment>
          <Button
            fullWidth
            variant="contained"
            color="secondary"
            type="submit"
            size="large"
          >
            Pay with Stripe { <ButtonCircularProgress />}
          </Button>
        </Fragment>
      }
    />
  );
});

AddGPUHourDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  // theme: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
};

function Wrapper(props) {
  const { open, onClose, onSuccess } = props;
  return (
    <Elements stripe={stripePromise}>
      {open && (
        <AddGPUHourDialog open={open} onClose={onClose} onSuccess={onSuccess} />
      )}
    </Elements>
  );
}


AddGPUHourDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
};

export default Wrapper;
