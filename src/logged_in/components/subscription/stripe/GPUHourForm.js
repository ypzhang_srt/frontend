import React from "react";
import PropTypes from "prop-types";
// import { Grid } from "@material-ui/core";
// import { CardElement } from "@stripe/react-stripe-js";
// import StripeTextField from "./StripeTextField";
import HourSlider from './HourSlider'



function GPUHourForm(props) {
  const { hour, onHourChange } = props;
  return (
      <HourSlider
        onHourChange={onHourChange}
        hour={hour}
      />
  );
}

GPUHourForm.propTypes = {
    onHourChange: PropTypes.func.isRequired,
    hour: PropTypes.number.isRequired
};

export default GPUHourForm;
