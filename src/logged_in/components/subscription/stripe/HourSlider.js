import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';
import TimerIcon from '@material-ui/icons/Timer';
import PropTypes from "prop-types";
const useStyles = makeStyles({
  root: {
    width: 250,
  },
  input: {
    width: 42,
  },
});

function HourSlider(props) {
  const classes = useStyles();
  const { hour, onHourChange } = props;

  const handleSliderChange = (event, newValue) => {
    onHourChange(newValue);
  };

  const handleInputChange = (event) => {
    onHourChange(event.target.value === '' ? '' : Number(event.target.value));
  };

  const handleBlur = () => {
    if (hour < 0) {
        onHourChange(0);
    } else if (hour > 100) {
        onHourChange(100);
    }
  };

  return (

    <Grid container spacing={1} justify="space-between">
        <Grid item  xs={1}>
          <TimerIcon />
        </Grid>
        <Grid item xs={6}>
          <Slider
            value={typeof hour === 'number' ? hour : 0}
            onChange={handleSliderChange}
            aria-labelledby="input-slider"
            min={1}
            max={100}
            step={1}
          />
        </Grid>
        <Grid item xs={1}>
          <Input
            className={classes.input}
            value={hour}
            margin="dense"
            onChange={handleInputChange}
            onBlur={handleBlur}
            inputProps={{
              step: 1,
              min: 1,
              max: 100,
              type: 'number',
              'aria-labelledby': 'input-slider',
            }}
          />
        </Grid>
        <Grid item xs={1}>
            <Typography id="input-slider" gutterBottom>
              Hrs
            </Typography>
        </Grid>
    </Grid>
  );
}

HourSlider.propTypes = {
    onHourChange: PropTypes.func.isRequired,
    hour: PropTypes.number.isRequired
};

export default HourSlider;