import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
//import TableHead from "@material-ui/core/TableHead";
// import axios from "axios";

import hourPrettyPrint from "../../../shared/functions/hourPrettyPrint";
import unixToDateString from "../../../shared/functions/unixToDateString";
import {
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  withStyles
} from "@material-ui/core";
import EnhancedTableHead from "../../../shared/components/EnhancedTableHead";
// import IconButton from '@material-ui/core/IconButton';
// import Collapse from "@material-ui/core/Collapse";
//import Typography from "@material-ui/core/Typography";
import ColorfulChip from "../../../shared/components/ColorfulChip";
// import unixToDateString from "../../../shared/functions/unixToDateString";
import HighlightedInformation from "../../../shared/components/HighlightedInformation";
// import currencyPrettyPrint from "../../../shared/functions/currencyPrettyPrint";
// import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
// import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
//import getTokenHeaders from "../Token"
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

const styles = theme => ({
  tableWrapper: {
    overflowX: "auto",
    width: "100%"
  },
  blackBackground: {
    backgroundColor: theme.palette.primary.main
  },
  contentWrapper: {
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(2)
    },
    width: "100%"
  },
  dBlock: {
    display: "block !important"
  },
  dNone: {
    display: "none !important"
  },
  firstData: {
    paddingLeft: theme.spacing(3)
  }
});


function Row(props) {
  const { transaction, theme } = props;
  // const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();
  // const [transactions, settransactions] = React.useState([]);
  const color = transaction.flag === "+"?  green[800] : red[800]

  const renderJobID = (job_id) => {
      console.log("job id", job_id)
      if (job_id === -1) {
        console.log("return chip")
          return (
            <ColorfulChip
                label="NA"
                color={red[800]}
            />
          )
      } else {
        console.log("return job id")
          return job_id
      }
  }


  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell
          component="th"
          scope="row"
          className={classes.firstData}
        >
          {unixToDateString(transaction.ts)}
        </TableCell>
        <TableCell component="th" scope="row">
            <ColorfulChip
                label={hourPrettyPrint(transaction.amount)}
                color={color}
            />
        </TableCell>
        <TableCell component="th" scope="row">
            <ColorfulChip
                label={hourPrettyPrint(transaction.amount_after)}
                color={theme.palette.primary.light}
            />
        </TableCell>
        <TableCell component="th" scope="row">
          {renderJobID(transaction.job_id)}
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}


const rows = [
  {
    id: "when",
    numeric: false,
    label: "When"
  },
  {
    id: "amount",
    numeric: false,
    label: "Amount"
  },
  {
    id: "amount_after",
    numeric: false,
    label: "After"
  },
  {
    id: "job",
    numeric: false,
    label: "Job"
  }
];

const rowsPerPage = 25;

function TransTable(props) {
  const { transactions, theme, /* openAddtransactionDialog, theme,*/ classes } = props;
  const [page, setPage] = useState(0);


  const handleChangePage = useCallback(
    (_, page) => {
      setPage(page);
    },
    [setPage]
  );

  if (transactions.length > 0) {
    return (
      <div className={classes.tableWrapper}>
        <Table aria-labelledby="tableTitle">
          <EnhancedTableHead rowCount={transactions.length} rows={rows} />
          <TableBody>
            {transactions
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((transaction, index) => (
                  <Row theme={theme} key={index} transaction={transaction} />

              ))}
          </TableBody>
        </Table>

        <TablePagination
          component="div"
          count={transactions.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={handleChangePage}
          classes={{
            select: classes.dNone,
            selectIcon: classes.dNone,
            actions: transactions.length > 0 ? classes.dBlock : classes.dNone,
            caption: transactions.length > 0 ? classes.dBlock : classes.dNone
          }}
          labelRowsPerPage=""
        />
      </div>
    );
  }
  return (
    <div className={classes.contentWrapper}>
      <HighlightedInformation>
        No running transactions found.
      </HighlightedInformation>
    </div>
  );
}

TransTable.propTypes = {
  theme: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,

  transactions: PropTypes.arrayOf(PropTypes.object).isRequired,
  // openAddtransactionDialog: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(TransTable);
