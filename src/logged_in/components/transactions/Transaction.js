import React, { useState, useCallback, useEffect } from "react";
import PropTypes from "prop-types";
import axios from 'axios';
import { List, Paper, withStyles } from "@material-ui/core";
import TransTable from "./TransTable";
import getTokenHeaders from "../Token"
function getUser() {
    try {
      return localStorage.getItem('user');
    } catch (err) {
      return ""
    }
  }

const styles = {
  divider: {
    backgroundColor: "rgba(0, 0, 0, 0.26)"
  }
};

function Transaction(props) {
  const {
    // classes,
    curCluster,
    selectTransaction,
    pushMessageToSnackbar,
  } = props;

  const [transactions, setTransactions] = useState([]);

  const fetchTransactions = useCallback(() => {
    if (curCluster === null) {
        return Promise.resolve();
    }
    const user = getUser()
    const token = getTokenHeaders()
    const jsonPayload =  {"group": "",
        "user": user,
    }
    console.log("submitting transactions transactions")

    axios.post('/api/transactions', jsonPayload, {headers: token })
      .then(res => {
        console.log("transactions returned ", res.data)
        setTransactions(res.data)
      })
      .catch(error => {
        setTransactions([])
        setTimeout(() => {
          pushMessageToSnackbar({
            text: "error getting transactions " + error,
          });
        }, 1500);
  })}, [setTransactions, curCluster, pushMessageToSnackbar]);


  useEffect(() => {
    selectTransaction()
    fetchTransactions();
  }, [
    selectTransaction,
    fetchTransactions,
  ]);

  return (
    <Paper>
      <List disablePadding>
        <TransTable transactions={transactions}/>
      </List>
    </Paper>
  );
}

Transaction.propTypes = {
  // classes: PropTypes.object.isRequired,
  // curCluster: PropTypes.object.isRequired,
  selectTransaction: PropTypes.func.isRequired,
  pushMessageToSnackbar: PropTypes.func.isRequired,
};

export default withStyles(styles)(Transaction);
