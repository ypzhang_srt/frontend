import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

export default function TaskListGroup() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    gilad: true,
    jason: false,
    antoine: false,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const { gilad, jason, antoine } = state;
  const error = [gilad, jason, antoine].filter((v) => v).length !== 2;

  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Completed</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="gilad" />}
            label="Fake Login"
          />
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="gilad" />}
            label="List Clusters"
          />
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="jason" />}
            label="List Models"
          />
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="jason" />}
            label="Run A Job"
          />
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="antoine" />}
            label="List Jobs For A Model"
          />
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="antoine" />}
            label="Upload model to S3 bucket"
          />
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="antoine" />}
            label="Upload A Model"
          />
            <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="gilad" />}
            label="View Running Jobs"
          />

          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="gilad" />}
            label="Delete a model"
          />
          <FormControlLabel
            control={<Checkbox checked={true} onChange={handleChange} name="antoine" />}
            label="Registration (database)"
          />
        </FormGroup>
      </FormControl>
      <FormControl required error={error} component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">TODO</FormLabel>
        <FormGroup>

          <FormControlLabel
            control={<Checkbox checked={false} onChange={handleChange} name="jason" />}
            label="View Cluster Detail"
          />
          <FormControlLabel
            control={<Checkbox checked={false} onChange={handleChange} name="antoine" />}
            label="Verify user email"
          />
          <FormControlLabel
            control={<Checkbox checked={false} onChange={handleChange} name="antoine" />}
            label="Billing Databbase"
          />
        </FormGroup>
        <FormHelperText>Move to the left once done</FormHelperText>
      </FormControl>
    </div>
  );
}