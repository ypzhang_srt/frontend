
function hourPrettyPrint(hours) {
    const hour = hours / 100;
    return hour + " hours"
  }

export default hourPrettyPrint;